%let deb=1997;
%let fin=2014;
%let nom=t11;
%let nom_rep=Tableau_11 Caractristiques DEFM A;

libname nostra "F:\Sources\Tensions\PSM 2015";
%let annee=14;

%let chemin=d:\documents-utilisateurs\charline.babet\Donnees\PSM\PSM 2015\Sorties\&nom_rep;
libname libout "&chemin";

%macro dossier(chemin_rep);
	options NOXWAIT NOXSYNC ;
		%let rc=%sysfunc(filename(fileref, &chemin_rep));
		 %if %sysfunc(fexist(&fileref))  %then %do ; 
		 	%end;
			%else %do;
			Data _null_;
				X mkdir "&chemin_rep"; /*Cration du repertoire  l'aide de la macro-variable*/
			run;
		%end;
%mend;
%dossier(&chemin)
libname libout "&chemin";

data donnees_nostra;
	set nostra.Deoe_f9nt&annee.t1 nostra.Deoe_f9nt&annee.t2 nostra.Deoe_f9nt&annee.t3 nostra.Deoe_f9nt&annee.t4 ;
	attrib fap_3 format=$3.;
	fap_3=substr(fap,1,3);
	if reg<"10" or reg>"95" then delete; /*on ne garde que la France mtropolitaine*/
run;

proc sort data=donnees_nostra;by fap_3;run; 
proc summary data=donnees_nostra noprint;
	var f_femm_a f_defm_a f_12clda f_24tlda;
	class fap_3;
	output out=res(drop=_type_ _freq_) sum=f_femm_a f_defm_a f_12clda f_24tlda;
run;

data libout.res;
	set res;
	part_femmes_a=100*f_femm_a/f_defm_a;
	part_defm_1an_ou_plus=100*(f_12clda+f_24tlda)/f_defm_a;
	if fap_3="" then fap_3="ENS";
run;

%macro prep_fichier_t11(chemin,nom_tab,nom_graph);
	%let chemin_psm=d:\documents-utilisateurs\charline.babet\Donnees\PSM\PSM 2015\Modles;
	options missing="";
	options NOXWAIT NOXSYNC ;
	x copy "&chemin_psm.\Modle &nom_graph..xls" "&chemin.\&nom_graph..xls";
	options NOXWAIT NOXSYNC ; *** Open Excel ***; 
	x "start excel"; 
	data _null_ ; 
		x = SLEEP(4) ; 
	run ; 
	filename cmds DDE 'excel|system' ;
	data _null_ ;
		file cmds ;
		put '[FILE-OPEN("'"&chemin.\&nom_graph..xls"'")]' ;
	run ;
	%let plage1=L1C1:L1C40;
	%let plage2=L2C1:L1000C40;
	filename t dde "EXCEL|&chemin.\[&nom_graph..xls]eff!&plage1" notab;
	data _null_;
		file t lrecl=500;
		set &nom_tab;
		sp="09"X;
		put "Fap3" sp  "f_femm_a" sp
		"f_12clda" sp "f_24tlda" sp "f_defm_a"
		;		
	run;
	 filename t dde "EXCEL|&chemin.\[&nom_graph..xls]eff!&plage2" notab;
	data _null_;
		file t lrecl=500;
		set &nom_tab;
		sp="09"X;
		put fap_3 $3. sp 
		f_femm_a commax6.2 sp f_12clda commax6.2 sp
		f_24tlda commax6.2 sp f_defm_a commax6.2 
		;		
	run;
	filename t dde "EXCEL|&chemin.\[&nom_graph..xls]part!&plage1" notab;
	data _null_;
		file t lrecl=500;
		set &nom_tab;
		sp="09"X;
		put "Fap3" sp  "part_femmes_a" sp
		"part_defm_1an_ou_plus"
		;		
	run;
	 filename t dde "EXCEL|&chemin.\[&nom_graph..xls]part!&plage2" notab;
	data _null_;
		file t lrecl=500;
		set &nom_tab;
		sp="09"X;
		put fap_3 $3. sp 
		part_femmes_a commax6.2 sp part_defm_1an_ou_plus commax6.2
		;		
	run;
	options missing=".";
	/*sauvegarde du fichier Excel*/
	FILENAME cmds DDE 'Excel|system';
	DATA _NULL_;
		FILE cmds;
		PUT '[save()]';
	RUN;
	/*fermeture du fichier*/
	data _null_ ;
		file cmds ;
		put "[FILE-CLOSE()]" ;
	run ;
	/*fermer excel*/
	data _null_ ;
		file cmds ;
		put "[QUIT()]" ;
	run ;

%mend;
%prep_fichier_t11(&chemin,libout.res,&nom_rep)





