/*********** METHODE utilis�e = M1bis des tests ************/
/* M�thode qui applique aux s�ries une correction identique dans le temps (translation)
apr�s avoir corrig� les ruptures 2003, 1993, 1990 et 1994. */
/* On fait tourner une premi�re fois le programme en traitant T2A et T2B (pour avoir leurs
s�ries corrig�es sur 1994-2002), puis une seconde fois en les regroupant dans T2Z (pour avoir 
sa s�rie corrig�e sur 1982-1993). */
/***********************************************************/

/* V�rification que les s�ries sont bien cal�es. */
proc summary data=calage1982; 
class FAP_3 cs_cal sexe_ag3_cal;
var poids_cal;
output out=test_1982 (drop=_type_ _freq_) sum=_1982;
run;
proc summary data=calage2011; 
var poids_cal;
output out=test_2011 (drop=_type_ _freq_) sum=_2011;
run;

/*** Calcul des coefficients de correction. */
proc import replace file="C:\Users\guillaume.toure\Desktop\DAMETH_TOURE\PSM\preparation_donnees\Series longues 2021\Programmes et sorties SAS 2021\3_Coefficients de correction.xls"
dbms="xls"
out=series_insee;
run;

data coefs;
set series_insee;
	coef_2003=_2003_brut/_2002;
	coef_1993=_1993/_1992;
	coef_1990=_1990/_1989;
keep cs_sexe_ag3_cal coef_2003 coef_1993 coef_1990;
run;


%macro series_coefs;

%do annee=1982 %to 2014;
proc sort data=calage&annee.; by fap_3 cs_sexe_ag3_cal; run;
proc summary data=calage&annee. nway; 
class FAP_3 cs_sexe_ag3_cal;
var poids_cal;
output out=serie_&annee.(drop=_type_ _freq_) sum=_&annee.;
run;
%end;

data series_fap_csa;
merge %do annee=1982 %to 2014; serie_&annee. %end;;
by FAP_3 cs_sexe_ag3_cal;
%do annee=1982 %to 2014; 
if _&annee.=. then _&annee.=0;
%end;;
run;

proc summary data=series_fap_csa;
by FAP_3 cs_sexe_ag3_cal;
var %do annee=1982 %to 2014; _&annee. %end;;
output out=series_fap_csa (drop=_type_ _freq_) sum=;
run;
proc sort data=series_fap_csa; by fap_3; run;
proc summary data=series_fap_csa;
by FAP_3;
var %do annee=1982 %to 2014; _&annee. %end;;
output out=series_fap (drop=_type_ _freq_) sum=;
run;
proc sort data=series_fap_csa; by cs_sexe_ag3_cal; run;
proc summary data=series_fap_csa;
by cs_sexe_ag3_cal;
var %do annee=1982 %to 2014; _&annee. %end;;
output out=series_csa (drop=_type_ _freq_) sum=;
run; /* En fait, comme les s�ries sont cal�es, series_csa=series_insee. */

/* Pour corriger la s�rie T2Z sur toute la p�riode, on regroupe T2A et T2B dans T2Z 
sur 1982-2014 dans les tables ..._temp. */

%do annee=1982 %to 2014;

data calage&annee._temp;
set calage&annee.;
if fap_3 in ('T2A','T2B') then fap_temp='T2Z';
else fap_temp=fap_3;
run;

proc sort data=calage&annee._temp; by fap_temp cs_sexe_ag3_cal; run;
proc summary data=calage&annee._temp nway; 
class fap_temp cs_sexe_ag3_cal;
var poids_cal;
output out=serie_&annee._temp(drop=_type_ _freq_) sum=_&annee.;
run;

%end;

data series_fap_csa_temp;
merge %do annee=1982 %to 2014; serie_&annee._temp %end;;
by fap_temp cs_sexe_ag3_cal;
%do annee=1982 %to 2014; 
if _&annee.=. then _&annee.=0;
%end;;
run;

proc summary data=series_fap_csa_temp;
by fap_temp cs_sexe_ag3_cal;
var %do annee=1982 %to 2014; _&annee. %end;;
output out=series_fap_csa_temp (drop=_type_ _freq_) sum=;
run;
proc sort data=series_fap_csa_temp; by fap_temp; run;
proc summary data=series_fap_csa_temp;
by fap_temp;
var %do annee=1982 %to 2014; _&annee. %end;;
output out=series_fap_temp (drop=_type_ _freq_) sum=;
run;

%mend;

%series_coefs;

/*** Correction des s�ries longues par FAP. */

/** 1�re fois : en corrigeant T2A et T2B */

%macro correction_FAP_1;

/* Correction de l'�volution 2002-2003. */

proc sort data=serie_2003; by fap_3 cs_sexe_ag3_cal; run;
proc sort data=serie_2002; by fap_3 cs_sexe_ag3_cal; run;
data correction_2003;
merge serie_2002 serie_2003;
by FAP_3 cs_sexe_ag3_cal;
run;

proc sort data=correction_2003; by cs_sexe_ag3_cal; run;
proc sort data=coefs; by cs_sexe_ag3_cal; run;
data correction_2003; 
merge correction_2003 coefs;
by cs_sexe_ag3_cal;
run;

proc sort data=correction_2003; by fap_3 cs_sexe_ag3_cal; run;
data correction_2003;
set correction_2003;

if _2002=. then _2002=0;
if _2003=. then _2003=0;

alpha_2003=_2003/coef_2003-_2002;

run;

proc sort data=correction_2003; by fap_3 cs_sexe_ag3_cal; run;
proc summary data=correction_2003 nway;
class FAP_3 cs_sexe_ag3_cal;
var alpha_2003;
output out=correction_2003a (drop=_type_ _freq_) sum=;
run;

/* Correction de l'�volution 1993-1994 (pour T6Z). */ 

proc sort data=serie_1994; by fap_3 cs_sexe_ag3_cal; run;
proc sort data=serie_1993; by fap_3 cs_sexe_ag3_cal; run;
proc sort data=correction_2003a; by fap_3 cs_sexe_ag3_cal; run;

data correction_1994;
merge serie_1993 serie_1994 correction_2003a ;
by FAP_3 cs_sexe_ag3_cal;
run;

proc sort data=correction_1994; by cs_sexe_ag3_cal; run;
proc sort data=coefs; by cs_sexe_ag3_cal; run;
data correction_1994; 
merge correction_1994 coefs;
by cs_sexe_ag3_cal;
run;

proc sort data=correction_1994; by fap_3 cs_sexe_ag3_cal; run;

data correction_1994;
set correction_1994;

if _1993=. then _1993=0;
if _1994=. then _1994=0;
if alpha_2003=. then alpha_2003=0;

if FAP_3="T6Z" and cs_sexe_ag3_cal in (7,8,9,10,11,12) then alpha_1994=57/6 /*54/6*/;
else if FAP_3="R3Z" and cs_sexe_ag3_cal in (7,8,9,10,11,12) then alpha_1994=-40/6 /*-38/6*/;
else if FAP_3="L6Z" and cs_sexe_ag3_cal in (7,8,9,10,11,12) then alpha_1994=-17/6 /*-16/6*/;
else alpha_1994=0; 

run;

proc sort data=correction_1994; by fap_3 cs_sexe_ag3_cal; run;
proc summary data=correction_1994 nway;
class FAP_3 cs_sexe_ag3_cal;
var alpha_2003 alpha_1994;
output out=correction_1994a (drop=_type_ _freq_) sum=;
run;

/* Correction de l'�volution 1992-1993. */ 

proc sort data=serie_1993; by fap_3 cs_sexe_ag3_cal; run;
proc sort data=serie_1992; by fap_3 cs_sexe_ag3_cal; run;
proc sort data=correction_1994a; by fap_3 cs_sexe_ag3_cal; run;

data correction_1993;
merge serie_1992 serie_1993 correction_1994a;
by FAP_3 cs_sexe_ag3_cal;
run;

proc sort data=correction_1993; by cs_sexe_ag3_cal; run;
proc sort data=coefs; by cs_sexe_ag3_cal; run;
data correction_1993; 
merge correction_1993 coefs;
by cs_sexe_ag3_cal;
run;

proc sort data=correction_1993; by fap_3 cs_sexe_ag3_cal; run;

data correction_1993;
set correction_1993;

if _1992=. then _1992=0;
if _1993=. then _1993=0;
if alpha_2003=. then alpha_2003=0;
if alpha_1994=. then alpha_1994=0;

temp_1992=_1992+alpha_2003+alpha_1994;
temp_1993=_1993+alpha_2003+alpha_1994;

alpha_1993=temp_1993/coef_1993-temp_1992; 

run;

proc sort data=correction_1993; by fap_3 cs_sexe_ag3_cal; run;
proc summary data=correction_1993 nway;
class FAP_3 cs_sexe_ag3_cal;
var alpha_1993 alpha_1994 alpha_2003;
output out=correction_1993a (drop=_type_ _freq_) sum=;
run; 

/* Correction de l'�volution 1989-1990. */

proc sort data=serie_1990; by fap_3 cs_sexe_ag3_cal; run;
proc sort data=serie_1989; by fap_3 cs_sexe_ag3_cal; run;
proc sort data=correction_1993a; by fap_3 cs_sexe_ag3_cal; run;

data correction_1990;
merge serie_1990 serie_1989 correction_1993a;
by FAP_3 cs_sexe_ag3_cal;
run;

proc sort data=correction_1990; by cs_sexe_ag3_cal; run;
proc sort data=coefs; by cs_sexe_ag3_cal; run;
data correction_1990; 
merge correction_1990 coefs;
by cs_sexe_ag3_cal;
run;

proc sort data=correction_1990; by fap_3 cs_sexe_ag3_cal; run;

data correction_1990;
set correction_1990;

if _1989=. then _1989=0;
if _1990=. then _1990=0;

if alpha_2003=. then alpha_2003=0;
if alpha_1994=. then alpha_1994=0;
if alpha_1993=. then alpha_1993=0;

temp_1989=_1989+alpha_2003+alpha_1994+alpha_1993;
temp_1990=_1990+alpha_2003+alpha_1994+alpha_1993;

alpha_1990=temp_1990/coef_1990-temp_1989; 

run;

proc sort data=correction_1990; by fap_3 cs_sexe_ag3_cal; run;
proc summary data=correction_1990 nway;
class FAP_3 cs_sexe_ag3_cal;
var  alpha_2003 alpha_1994 alpha_1993 alpha_1990;
output out=correction_1990a (drop=_type_ _freq_) sum=;
run;

proc sort data=series_fap_csa; by fap_3 cs_sexe_ag3_cal; run;
proc sort data=correction_1990a; by fap_3 cs_sexe_ag3_cal; run;

data series_FAP_corrigees;
merge series_fap_csa correction_1990a;
by FAP_3 cs_sexe_ag3_cal;
if alpha_2003=. then alpha_2003=0;
if alpha_1994=. then alpha_1994=0;
if alpha_1993=. then alpha_1993=0;
if alpha_1990=. then alpha_1990=0;

%do annee=1982 %to 1989;
emploi_c_&annee.=_&annee.+alpha_1993+alpha_1994+alpha_2003+alpha_1990;
%end;
%do annee=1990 %to 1992;
emploi_c_&annee.=_&annee.+alpha_1993+alpha_1994+alpha_2003;
%end;
emploi_c_1993=_1993+alpha_1994+alpha_2003;
%do annee=1994 %to 2002;
emploi_c_&annee.=_&annee.+alpha_2003;
%end;
%do annee=2003 %to 2014;
emploi_c_&annee.=_&annee.;
%end;

run;

proc sort data=series_FAP_corrigees; by fap_3; run;
proc summary data=series_FAP_corrigees;
by FAP_3;
var emploi_c_1982 - emploi_c_2014 alpha_2003 alpha_1993 alpha_1994 alpha_1990;
output out=series_FAP_corrigees_1 sum=;
run;

%mend;

%correction_FAP_1;

/** 2�me fois : en ne corrigeant que T2Z */

%macro correction_FAP_2;

/* Correction de l'�volution 2002-2003. */

proc sort data=serie_2003_temp; by fap_temp cs_sexe_ag3_cal; run;
proc sort data=serie_2002_temp; by fap_temp cs_sexe_ag3_cal; run;
data correction_2003;
merge serie_2002_temp serie_2003_temp;
by fap_temp cs_sexe_ag3_cal;
run;

proc sort data=correction_2003; by cs_sexe_ag3_cal; run;
proc sort data=coefs; by cs_sexe_ag3_cal; run;
data correction_2003; 
merge correction_2003 coefs;
by cs_sexe_ag3_cal;
run;

proc sort data=correction_2003; by fap_temp cs_sexe_ag3_cal; run;
data correction_2003;
set correction_2003;

if _2002=. then _2002=0;
if _2003=. then _2003=0;

alpha_2003=_2003/coef_2003-_2002;

run;

proc sort data=correction_2003; by fap_temp cs_sexe_ag3_cal; run;
proc summary data=correction_2003 nway;
class fap_temp cs_sexe_ag3_cal;
var alpha_2003;
output out=correction_2003a (drop=_type_ _freq_) sum=;
run;

/* Correction de l'�volution 1993-1994 (pour T6Z). */ 

proc sort data=serie_1994_temp; by fap_temp cs_sexe_ag3_cal; run;
proc sort data=serie_1993_temp; by fap_temp cs_sexe_ag3_cal; run;
proc sort data=correction_2003a; by fap_temp cs_sexe_ag3_cal; run;

data correction_1994;
merge serie_1993_temp serie_1994_temp correction_2003a ;
by fap_temp cs_sexe_ag3_cal;
run;

proc sort data=correction_1994; by cs_sexe_ag3_cal; run;
proc sort data=coefs; by cs_sexe_ag3_cal; run;
data correction_1994; 
merge correction_1994 coefs;
by cs_sexe_ag3_cal;
run;

proc sort data=correction_1994; by fap_temp cs_sexe_ag3_cal; run;

data correction_1994;
set correction_1994;

if _1993=. then _1993=0;
if _1994=. then _1994=0;
if alpha_2003=. then alpha_2003=0;

if fap_temp="T6Z" and cs_sexe_ag3_cal in (7,8,9,10,11,12) then alpha_1994=57/6 /*54/6*/;
else if fap_temp="R3Z" and cs_sexe_ag3_cal in (7,8,9,10,11,12) then alpha_1994=-40/6 /*-38/6*/;
else if fap_temp="L6Z" and cs_sexe_ag3_cal in (7,8,9,10,11,12) then alpha_1994=-17/6 /*-16/6*/;
else alpha_1994=0; 

run;

proc sort data=correction_1994; by fap_temp cs_sexe_ag3_cal; run;
proc summary data=correction_1994 nway;
class fap_temp cs_sexe_ag3_cal;
var alpha_2003 alpha_1994;
output out=correction_1994a (drop=_type_ _freq_) sum=;
run;

/* Correction de l'�volution 1992-1993. */ 

proc sort data=serie_1993_temp; by fap_temp cs_sexe_ag3_cal; run;
proc sort data=serie_1992_temp; by fap_temp cs_sexe_ag3_cal; run;
proc sort data=correction_1994a; by fap_temp cs_sexe_ag3_cal; run;

data correction_1993;
merge serie_1992_temp serie_1993_temp correction_1994a;
by fap_temp cs_sexe_ag3_cal;
run;

proc sort data=correction_1993; by cs_sexe_ag3_cal; run;
proc sort data=coefs; by cs_sexe_ag3_cal; run;
data correction_1993; 
merge correction_1993 coefs;
by cs_sexe_ag3_cal;
run;

proc sort data=correction_1993; by fap_temp cs_sexe_ag3_cal; run;

data correction_1993;
set correction_1993;

if _1992=. then _1992=0;
if _1993=. then _1993=0;
if alpha_2003=. then alpha_2003=0;
if alpha_1994=. then alpha_1994=0;

temp_1992=_1992+alpha_2003+alpha_1994;
temp_1993=_1993+alpha_2003+alpha_1994;

alpha_1993=temp_1993/coef_1993-temp_1992; 

run;

proc sort data=correction_1993; by fap_temp cs_sexe_ag3_cal; run;
proc summary data=correction_1993 nway;
class fap_temp cs_sexe_ag3_cal;
var alpha_1993 alpha_1994 alpha_2003;
output out=correction_1993a (drop=_type_ _freq_) sum=;
run; 

/* Correction de l'�volution 1989-1990. */

proc sort data=serie_1990_temp; by fap_temp cs_sexe_ag3_cal; run;
proc sort data=serie_1989_temp; by fap_temp cs_sexe_ag3_cal; run;
proc sort data=correction_1993a; by fap_temp cs_sexe_ag3_cal; run;

data correction_1990;
merge serie_1990_temp serie_1989_temp correction_1993a;
by fap_temp cs_sexe_ag3_cal;
run;

proc sort data=correction_1990; by cs_sexe_ag3_cal; run;
proc sort data=coefs; by cs_sexe_ag3_cal; run;
data correction_1990; 
merge correction_1990 coefs;
by cs_sexe_ag3_cal;
run;

proc sort data=correction_1990; by fap_temp cs_sexe_ag3_cal; run;

data correction_1990;
set correction_1990;

if _1989=. then _1989=0;
if _1990=. then _1990=0;

if alpha_2003=. then alpha_2003=0;
if alpha_1994=. then alpha_1994=0;
if alpha_1993=. then alpha_1993=0;

temp_1989=_1989+alpha_2003+alpha_1994+alpha_1993;
temp_1990=_1990+alpha_2003+alpha_1994+alpha_1993; 

alpha_1990=temp_1990/coef_1990-temp_1989; 

run;

proc sort data=correction_1990; by fap_temp cs_sexe_ag3_cal; run;
proc summary data=correction_1990 nway;
class fap_temp cs_sexe_ag3_cal;
var  alpha_2003 alpha_1994 alpha_1993 alpha_1990;
output out=correction_1990a (drop=_type_ _freq_) sum=;
run;

proc sort data=series_fap_csa_temp; by fap_temp cs_sexe_ag3_cal; run;
proc sort data=correction_1990a; by fap_temp cs_sexe_ag3_cal; run;

data series_FAP_corrigees;
merge series_fap_csa_temp correction_1990a;
by fap_temp cs_sexe_ag3_cal;
if alpha_2003=. then alpha_2003=0;
if alpha_1994=. then alpha_1994=0;
if alpha_1993=. then alpha_1993=0;
if alpha_1990=. then alpha_1990=0;

%do annee=1982 %to 1989;
	emploi_c_&annee.=_&annee.+alpha_1993+alpha_1994+alpha_2003+alpha_1990;
%end;
%do annee=1990 %to 1992;
	emploi_c_&annee.=_&annee.+alpha_1993+alpha_1994+alpha_2003;
%end;
	emploi_c_1993=_1993+alpha_1994+alpha_2003;
%do annee=1994 %to 2002;
	emploi_c_&annee.=_&annee.+alpha_2003;
%end;
%do annee=2003 %to 2014;
	emploi_c_&annee.=_&annee.;
%end; 

run;

proc sort data=series_FAP_corrigees; by fap_temp; run;
proc summary data=series_FAP_corrigees;
by fap_temp;
var emploi_c_1982 - emploi_c_2014 alpha_2003 alpha_1993 alpha_1994 alpha_1990;
output out=series_FAP_corrigees_2 sum=;
run;

%mend;

%correction_FAP_2;


/*** V�rifications. */

/* La somme doit faire z�ro.*/
proc means data=series_FAP_corrigees_1 sum;
var alpha_2003 alpha_1993 alpha_1994 alpha_1990;
run;
proc means data=series_FAP_corrigees_2 sum;
var alpha_2003 alpha_1993 alpha_1994 alpha_1990;
run;

/* On doit retrouver les totaux des s�ries Insee. */
proc means data=series_FAP_corrigees_1 sum;
var emploi_c_1982 - emploi_c_2014;
run;
proc means data=series_FAP_corrigees_2 sum;
var emploi_c_1982 - emploi_c_2014;
run;

/*** S�ries par FAP. */
proc sort data=series_FAP_corrigees_1; by fap_3; run;
data series_FAP_corrigees_1; 
set series_FAP_corrigees_1; 
keep fap_3 emploi_c_1994--emploi_c_2014;
if fap_3='T2Z' then delete;
run;
proc sort data=series_FAP_corrigees_2; by fap_temp; run;
data series_FAP_corrigees_2; 
set series_FAP_corrigees_2; 
fap_3=fap_temp; 
keep fap_3 emploi_c_1982--emploi_c_1993;
run;

data series_FAP_corrigees;
merge series_FAP_corrigees_2 series_FAP_corrigees_1;
by fap_3;
run;

proc export data=series_FAP_corrigees
		 	outfile="C:\Users\guillaume.toure\Desktop\DAMETH_TOURE\PSM\preparation_donnees\Series longues 2021\Methodo\X_Series a chaque etape\Sorties SAS\3_Series corrigees des ruptures2.xls" 
			DBMS=tab REPLACE; 
run;

proc export data=series_FAP_corrigees
		 	outfile="C:\Users\guillaume.toure\Desktop\DAMETH_TOURE\PSM\preparation_donnees\Series longues 2021\Methodo\X_Series a chaque etape\Sorties SAS\3_Correction_FAP_series par FAP2.xls" 
			DBMS=tab REPLACE; 
run;

