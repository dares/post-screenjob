data chemin ;
	set chemin;
	call symput(macrovar, valeur);
run;

libname map "&lib_map.";

%include "&lib_formats.";

%let nom=c1;
%let nom_rep=Carte_1 Part r�gion;

%let chemin=&lib_sortie.\&nom_rep;

%let chemin_input=&lib_input.;

/*libname rp "C:\Users\christophe.michel\Documents\Sources\RP\RP 2016\Exploitation Compl�mentaire Individus";*/

libname rpmet "&lib_rpmet.";
libname rp "W:\DONNEES_SOURCES\RP\2018\Exploitation compl�mentaire\Exploitation_Compl�mentaires_Individus";

%let liste_region=01 02 03 04 11 21 22 23 24 25 26 31 41 42 43 52 53 54 72 73 74 82 83 91 93 94;

%macro dossier(chemin_rep);
	options NOXWAIT NOXSYNC ;
		%let rc=%sysfunc(filename(fileref, &chemin_rep));
		 %if %sysfunc(fexist(&fileref))  %then %do ; 
		 	%end;
			%else %do;
			Data _null_;
				X mkdir "&chemin_rep"; /*Cration du repertoire  l'aide de la macro-variable*/
			run;
		%end;
%mend;
%dossier(&chemin)
libname libout "&chemin";

/*%macro rp(liste_reg);
	%let i=1;
	%do %while(%length(%scan(&liste_reg,&i))>0);
		%let reg&i=%scan(&liste_reg,&i);
		%let i=%eval(&i+1);
	%end;
	%let nbreg=%eval(&i-1);
	%do i=1 %to &nbreg;
		%let bib=rp;
		data rp&i;
			set &bib..r&&reg&i(where=(tact="11" and AGEREVQ>="015"));
			P=lowcase(prof);
			FAP=put(P,$FAP9PCx.);
			FAP_3=substr(FAP,1,3);
		run;
		proc summary data=rp&i missing nway;
			class region_trav FAP_3;
			var ipondi;
			output out=b&i. sum=emp_&i.;
		run;
	%end;
	data rp;
		merge %do i=1 %to &nbreg; b&i. %end;;
		by region_trav fap_3;
	run;

	data rp;
		set rp;
		%do i=1 %to &nbreg;  if emp_&i=. then emp_&i=0; %end;
		emploi=emp_1 %do i=2 %to &nbreg;  + emp_&i %end;;
	run;

%mend;
%rp(&liste_region)*/

/*com
dom
met
spm*/

proc contents data=rp_com;
run;


/*
REG16_TRAV;REG16_TRAV : Lieu de travail (R�gion - d�coupage 2016);01;Guadeloupe (01);45175;127073.95096
REG16_TRAV;REG16_TRAV : Lieu de travail (R�gion - d�coupage 2016);02;Martinique (02);44882;129701.37791
REG16_TRAV;REG16_TRAV : Lieu de travail (R�gion - d�coupage 2016);03;Guyane (03);24401;68858.252436
REG16_TRAV;REG16_TRAV : Lieu de travail (R�gion - d�coupage 2016);04;La R�union (04);100824;261055.16817
REG16_TRAV;REG16_TRAV : Lieu de travail (R�gion - d�coupage 2016);06;Mayotte (06);134;438.44583154
REG16_TRAV;REG16_TRAV : Lieu de travail (R�gion - d�coupage 2016);11;�le-de-France (11);2013677;5731789.7977
REG16_TRAV;REG16_TRAV : Lieu de travail (R�gion - d�coupage 2016);24;Centre-Val de Loire (24);256679;975261.49956
REG16_TRAV;REG16_TRAV : Lieu de travail (R�gion - d�coupage 2016);27;Bourgogne-Franche-Comt� (27);265026;1070873.9446
REG16_TRAV;REG16_TRAV : Lieu de travail (R�gion - d�coupage 2016);28;Normandie (28);323050;1269766.7204
REG16_TRAV;REG16_TRAV : Lieu de travail (R�gion - d�coupage 2016);32;Hauts-de-France (32);575062;2110388.2123
REG16_TRAV;REG16_TRAV : Lieu de travail (R�gion - d�coupage 2016);44;Grand Est (44);539873;2060505.8075
REG16_TRAV;REG16_TRAV : Lieu de travail (R�gion - d�coupage 2016);52;Pays de la Loire (52);414373;1530534.0983
REG16_TRAV;REG16_TRAV : Lieu de travail (R�gion - d�coupage 2016);53;Bretagne (53);339169;1308794.1058
REG16_TRAV;REG16_TRAV : Lieu de travail (R�gion - d�coupage 2016);75;Nouvelle-Aquitaine (75);607356;2326889.6378
REG16_TRAV;REG16_TRAV : Lieu de travail (R�gion - d�coupage 2016);76;Occitanie (76);598945;2212380.6357
REG16_TRAV;REG16_TRAV : Lieu de travail (R�gion - d�coupage 2016);84;Auvergne-Rh�ne-Alpes (84);870818;3201472.0187
REG16_TRAV;REG16_TRAV : Lieu de travail (R�gion - d�coupage 2016);93;Provence-Alpes-C�te d'Azur (93);632090;1901030.4604
REG16_TRAV;REG16_TRAV : Lieu de travail (R�gion - d�coupage 2016);94;Corse (94);100813;131745.71369
REG16_TRAV;REG16_TRAV : Lieu de travail (R�gion - d�coupage 2016);99;�tranger (sans objet) (99);110797;424685.22939
REG16_TRAV;REG16_TRAV : Lieu de travail (R�gion - d�coupage 2016);YY;Sans objet (YY);11658589;39710477.515
REG16_TRAV;REG16_TRAV : Lieu de travail (R�gion - d�coupage 2016);ZZ;Collectivit�d'outre-mer (ZZ);8826;21113.468746
*/




data rp_com;
set rp.com(where=(tact="11" and AGEREVQ>="015"));
P=lowcase(prof);
FAP=put(P,$FAP9PCx.);
FAP_3=substr(FAP,1,3);
run;
proc summary data=rp_com missing nway;
class reg16_trav FAP_3;
var ipondi;
output out=b_com sum=emp_com;
run;
data rp_dom;
set rp.dom(where=(tact="11" and AGEREVQ>="015"));
P=lowcase(prof);
FAP=put(P,$FAP9PCx.);
FAP_3=substr(FAP,1,3);
run;
proc summary data=rp_dom missing nway;
class reg16_trav FAP_3;
var ipondi;
output out=b_dom sum=emp_dom;
run;
data rp_met;
set rpmet.met(where=(tact="11" and AGEREVQ>="015"));
P=lowcase(prof);
FAP=put(P,$FAP9PCx.);
FAP_3=substr(FAP,1,3);
run;
proc summary data=rp_met missing nway;
class reg16_trav FAP_3;
var ipondi;
output out=b_met sum=emp_met;
run;
data rp_spm;
set rp.spm(where=(tact="11" and AGEREVQ>="015"));
P=lowcase(prof);
FAP=put(P,$FAP9PCx.);
FAP_3=substr(FAP,1,3);
run;
proc summary data=rp_spm missing nway;
class reg16_trav FAP_3;
var ipondi;
output out=b_spm sum=emp_spm;
run;





data rpmet.rp;
merge b_com b_met b_dom b_spm;
by reg16_trav fap_3;
run;

data rpmet.rp;
set rpmet.rp;
if emp_com=. then emp_com=0; 
if emp_dom=. then emp_dom=0;
if emp_met=. then emp_met=0;
if emp_spm=. then emp_spm=0;
emploi=emp_com + emp_dom + emp_met + emp_spm;
run;






/* Nouvelles rgions. */
proc freq data=rpmet.rp; tables reg16_trav; run;
data rp; 
set rpmet.rp;
	/*ajout dom */
	if reg16_trav in ("01") then code_reg="01";
	if reg16_trav in ("02") then code_reg="02";
	if reg16_trav in ("03") then code_reg="03";
	if reg16_trav in ("04") then code_reg="04";
	if reg16_trav in ("06") then code_reg="06";
    /**/
	if reg16_trav in ("11") then code_reg="11";
	if reg16_trav in ("24") then code_reg="24";
	if reg16_trav in ("27","26","43") then code_reg="27";
	if reg16_trav in ("28","23","25") then code_reg="28";
	if reg16_trav in ("32","31","22") then code_reg="32";
	if reg16_trav in ("44","41","42","21") then code_reg="44";
	if reg16_trav in ("52") then code_reg="52";
	if reg16_trav in ("53") then code_reg="53";
	if reg16_trav in ("75","72","54","74") then code_reg="75";
	if reg16_trav in ("76","73","91") then code_reg="76";
	if reg16_trav in ("84","82","83") then code_reg="84";
	if reg16_trav in ("93") then code_reg="93";
	if reg16_trav in ("94") then code_reg="94"; 

keep reg16_trav code_reg fap_3 emploi;
run;

data libout.rp;
	set rp;
	keep code_reg fap_3 emploi;
run;

proc summary data=libout.rp missing nway;
	where code_reg ne "";
	class code_reg FAP_3;
	var emploi;
	output out=a sum=emp_fap_reg;
run;
proc summary data=libout.rp missing nway;
	where code_reg ne "";
	class code_reg;
	var emploi;
	output out=b sum=emp_reg;
run;

proc sort data=a; by code_reg fap_3; run;
proc sort data=b; by code_reg; run;
data input;
merge a b;
by code_reg;
	if emp_fap_reg=. then emp_fap_reg=0;
	variable = 100 * emp_fap_reg / emp_reg;
drop _type_ _freq_;
run;

/* 	proc summary data=input missing nway;
	class code_reg;
	var variable;
	output out=test sum=pourcent;
	run;                                    */

proc sort data=input; by fap_3; run;

proc format;                                   
   value cl_pct  0-0.1 = '0,0 - 0,1' 
   				 0.1 - 0.25 ='0,1 - 0,25'
   				 0.25 - 0.5 ='0,25 - 0,5'
				 0.5 - 0.75= '0,5 - 0,75'   
				 0.75 - 1= '0,75 - 1,0'   
                 1 - 1.5= '1,0 - 1,5' 
                 1.5 - 2= '1,5 - 2,0' 
                 2 - 2.5= '2,0 - 2,5'
                 2.5 - 3= '2,5 - 3,0' 
				 3 - 3.5= '3,0 - 3,5' 
				 3.5 - 4= '3,5 - 4,0' 
				 4 - 4.5= '4,0 - 4,5' 
 				 4.5 - 5= '4,5 - 5,0' 
	             5 - 7.5 ='5,0 - 7,5'
				 7.5 - 10='7,5 - 10';

run;



data libout.input;
set input;
length nom_reg  $32;
length GID  $10;
var_classe=put(variable,cl_pct.);
if code_reg="01" then do; GID="Dom____"; nom_reg="Guadeloupe";end;
if code_reg="02" then do; GID="Dom____"; nom_reg="Martinique";end;
if code_reg="03" then do; GID="Dom____"; nom_reg="Guyane";end;
if code_reg="04" then do; GID="Dom____"; nom_reg="La R�union";end;
if code_reg="06" then do; GID="Dom____"; nom_reg="Mayotte";end;
if code_reg="11" then do; GID="FRA.8_1"; nom_reg="�le-de-France";end;
if code_reg="24" then do; GID="FRA.4_1"; nom_reg="Centre-Val de Loire";end;
if code_reg="27" then do; GID="FRA.2_1"; nom_reg="Bourgogne-Franche-Comt�";end;
if code_reg="28" then do; GID="FRA.9_1"; nom_reg="Normandie";end;
if code_reg="32" then do; GID="FRA.7_1"; nom_reg="Hauts-de-France";end;
if code_reg="44" then do; GID="FRA.6_1"; nom_reg="Grand Est";end;
if code_reg="52" then do; GID="FRA.12_1"; nom_reg="Pays de la Loire";end;
if code_reg="53" then do; GID="FRA.3_1"; nom_reg="Bretagne";end;
if code_reg="75" then do; GID="FRA.10_1"; nom_reg="Nouvelle-Aquitaine";end;
if code_reg="76" then do; GID="FRA.11_1"; nom_reg="Occitanie";end;
if code_reg="84" then do; GID="FRA.1_1"; nom_reg="Auvergne-Rh�ne-Alpes";end;
if code_reg="93" then do; GID="FRA.13_1"; nom_reg="Provence-Alpes-C�te d'Azur";end;
if code_reg="94" then do; GID="FRA.5_1"; nom_reg="Corse";end;
run;

ods html file="C:\Users\guillaume.toure\Desktop\DAMETH_TOURE\PSM\preparation_donnees\PSM 2021\Sorties\Carte_1 Part r�gion\input_carte1.xls";
proc print data=libout.input noobs;
run; 
ods html close;

proc sort data=libout.input out = input; by variable; run;

%macro carte_1(fap);

goptions reset=pattern  ftext='Arial' ftitle='Arial/bold' device=png gsfname=out htext=0.9
xmax=4in ymax=3in ;
filename out "&chemin_input.\&fap..png" ;

title;

pattern1 value=solid color=CXCCFFFF;
pattern2 value=solid color=CX33CCCC;
pattern3 value=solid color=CX009999;
pattern4 value=solid color=CX006666 /*CX008080*/;
pattern5 value=solid color=CX006600;
pattern6 value=solid color=CX339933;
pattern7 value=solid color=CX99CC00;
pattern8 value=solid color=CXFFCC00;
/* pattern9 value=solid color=;
pattern10 value=solid color=; */

ods _all_ close;
ods listing;

PROC GMAP DATA =input MAP = map.france_metro_newreg ;
	where fap_3="&fap.";
	ID code_reg ;
	CHORO var_classe / discrete coutline=black /*HTML=infobulle*/;
	label var_classe='En %' ;
RUN ; 
QUIT ;

ods listing close;

%mend;

%carte_1(A0Z);
%carte_1(A1Z);
%carte_1(A2Z);
%carte_1(A3Z);
%carte_1(B0Z);
%carte_1(B1Z);
%carte_1(B2Z);
%carte_1(B3Z);
%carte_1(B4Z);
%carte_1(B5Z);
%carte_1(B6Z);
%carte_1(B7Z);
%carte_1(C0Z);
%carte_1(C1Z);
%carte_1(C2Z);
%carte_1(D0Z);
%carte_1(D1Z);
%carte_1(D2Z);
%carte_1(D3Z);
%carte_1(D4Z);
%carte_1(D6Z);
%carte_1(E0Z);
%carte_1(E1Z);
%carte_1(E2Z);
%carte_1(F0Z);
%carte_1(F1Z);
%carte_1(F2Z);
%carte_1(F3Z);
%carte_1(F4Z);
%carte_1(F5Z);
%carte_1(G0A);
%carte_1(G0B);
%carte_1(G1Z);
%carte_1(H0Z);
%carte_1(J0Z);
%carte_1(J1Z);
%carte_1(J3Z);
%carte_1(J4Z);
%carte_1(J5Z);
%carte_1(J6Z);
%carte_1(K0Z);
%carte_1(L0Z);
%carte_1(L1Z);
%carte_1(L2Z);
%carte_1(L3Z);
%carte_1(L4Z);
%carte_1(L5Z);
%carte_1(L6Z);
%carte_1(M0Z);
%carte_1(M1Z);
%carte_1(M2Z);
%carte_1(N0Z);
%carte_1(P0Z);
%carte_1(P1Z);
%carte_1(P2Z);
%carte_1(P3Z);
%carte_1(P4Z);
%carte_1(Q0Z);
%carte_1(Q1Z);
%carte_1(Q2Z);
%carte_1(R0Z);
%carte_1(R1Z);
%carte_1(R2Z);
%carte_1(R3Z);
%carte_1(R4Z);
%carte_1(S0Z);
%carte_1(S1Z);
%carte_1(S2Z);
%carte_1(S3Z);
%carte_1(T0Z);
%carte_1(T1Z);
%carte_1(T2A);
%carte_1(T2B);
%carte_1(T3Z);
%carte_1(T4Z);
%carte_1(T6Z);
%carte_1(U0Z);
%carte_1(U1Z);
%carte_1(V0Z);
%carte_1(V1Z);
%carte_1(V2Z);
%carte_1(V3Z);
%carte_1(V4Z);
%carte_1(V5Z);
%carte_1(W0Z);
%carte_1(W1Z);
%carte_1(X0Z);
