%include "d:\documents-utilisateurs\charline.babet\Donnees\PSM\PSM 2015\Programmes\macros portraits.sas";

%let nom=g17;
%let nom_rep=Graphique_17 Entres PE;

%let chemin=d:\documents-utilisateurs\charline.babet\Donnees\PSM\PSM 2015\Sorties\&nom_rep;
libname libout "&chemin";

/* Demander les donnes sur les tensions  Yannick Croguennec. */
libname nostra "F:\Sources\Tensions\PSM 2015";
%let annee=14;

%macro dossier(chemin_rep);
	options NOXWAIT NOXSYNC ;
		%let rc=%sysfunc(filename(fileref, &chemin_rep));
		 %if %sysfunc(fexist(&fileref))  %then %do ; 
		 	%end;
			%else %do;
			Data _null_;
				X mkdir "&chemin_rep"; /*Cration du repertoire  l'aide de la macro-variable*/
			run;
		%end;
%mend;
%dossier(&chemin)
libname libout "&chemin";

data donnees_nostra;
	set nostra.Deoe_f9nt&annee.t1 nostra.Deoe_f9nt&annee.t2 nostra.Deoe_f9nt&annee.t3 nostra.Deoe_f9nt&annee.t4 ;
	attrib fap_3 format=$3.;
	fap_3=substr(fap,1,3);
	if reg<"10" or reg>"95" then delete; /*on ne garde que la France mtropolitaine*/
run;
proc sort data=donnees_nostra;by fap_3;run; 
proc summary data=donnees_nostra noprint;
	var e_dee_t e_inlic e_indem e_infin e_in1er e_inrep e_inxxx;
	class fap_3;
	output out=res(drop=_type_ _freq_) sum=e_dee_t e_inlic e_indem e_infin e_in1er e_inrep e_inxxx;
run;

data libout.res;
	set res;
	part_e_inlic=100*e_inlic/e_dee_t;
	part_e_indem=100*e_indem/e_dee_t;
	part_e_infin=100*e_infin/e_dee_t;
	part_e_in1er=100*e_in1er/e_dee_t;
	part_e_inrep=100*e_inrep/e_dee_t;
	part_e_inxxx=100*e_inxxx/e_dee_t;
	tot=part_e_inlic+part_e_indem+part_e_infin+part_e_in1er+part_e_inrep+part_e_inxxx;
run;

%macro prep_fichier_g17(chemin,nom_tab,nom_graph);
	%let chemin_psm=d:\documents-utilisateurs\charline.babet\Donnees\PSM\PSM 2015\Modles;
	options missing="";
	options NOXWAIT NOXSYNC ;
	x copy "&chemin_psm.\Modle &nom_graph..xls" "&chemin.\&nom_graph..xls";
	options NOXWAIT NOXSYNC ; *** Open Excel ***; 
	x "start excel"; 
	data _null_ ; 
		x = SLEEP(4) ; 
	run ; 
	filename cmds DDE 'excel|system' ;
	data _null_ ;
		file cmds ;
		put '[FILE-OPEN("'"&chemin.\&nom_graph..xls"'")]' ;
	run ;
	%let plage1=L1C1:L1C40;
	%let plage2=L2C1:L1000C40;
	filename t dde "EXCEL|&chemin.\[&nom_graph..xls]eff!&plage1" notab;
	data _null_;
		file t lrecl=500;
		set &nom_tab;
		sp="09"X;
		put "Fap3" sp  "e_inlic" sp
		"e_indem" sp "e_infin" sp "e_in1er" sp "e_inrep"  sp "e_inxxx" sp "e_dee_t"
		;		
	run;
	 filename t dde "EXCEL|&chemin.\[&nom_graph..xls]eff!&plage2" notab;
	data _null_;
		file t lrecl=500;
		set &nom_tab;
		sp="09"X;
		put fap_3 $3. sp 
		e_inlic commax6.2 sp e_indem commax6.2 sp
		e_infin commax6.2 sp e_in1er commax6.2 sp e_inrep commax6.2 sp
		e_inxxx commax6.2 sp e_dee_t commax6.2
		;		
	run;
	filename t dde "EXCEL|&chemin.\[&nom_graph..xls]part!&plage1" notab;
	data _null_;
		file t lrecl=500;
		set &nom_tab;
		sp="09"X;
		put "Fap3" sp  "part_e_inlic" sp
		"part_e_indem" sp "part_e_infin" sp "part_e_in1er" sp "part_e_inrep"  sp "part_e_inxxx" sp "Total"
		;		
	run;
	 filename t dde "EXCEL|&chemin.\[&nom_graph..xls]part!&plage2" notab;
	data _null_;
		file t lrecl=500;
		set &nom_tab;
		sp="09"X;
		put fap_3 $3. sp 
		part_e_inlic commax6.2 sp part_e_indem commax6.2 sp
		part_e_infin commax6.2 sp part_e_in1er commax6.2 sp part_e_inrep commax6.2 sp
		part_e_inxxx commax6.2 sp tot commax6.2
		;		
	run;
	options missing=".";
	/*sauvegarde du fichier Excel*/
	FILENAME cmds DDE 'Excel|system';
	DATA _NULL_;
		FILE cmds;
		PUT '[save()]';
	RUN;
	/*fermeture du fichier*/
	data _null_ ;
		file cmds ;
		put "[FILE-CLOSE()]" ;
	run ;
	/*fermer excel*/
	data _null_ ;
		file cmds ;
		put "[QUIT()]" ;
	run ;

%mend;
%prep_fichier_g17(&chemin,libout.res,&nom_rep)
