%include "d:\documents-utilisateurs\charline.babet\Donnees\PSM\PSM 2015\Programmes\macros portraits.sas";

%let deb=1997;
%let fin=2014;
%let nom=g16;
%let nom_rep=Graphique_16 Tensions;

%let chemin=d:\documents-utilisateurs\charline.babet\Donnees\PSM\PSM 2015\Sorties\&nom_rep;
libname libout "&chemin";

/* Demander les donnes sur les tensions  Yannick Croguennec. */
libname nostra "F:\Sources\Tensions\PSM 2015";

%macro dossier(chemin_rep);
	options NOXWAIT NOXSYNC ;
		%let rc=%sysfunc(filename(fileref, &chemin_rep));
		 %if %sysfunc(fexist(&fileref))  %then %do ; 
		 	%end;
			%else %do;
			Data _null_;
				X mkdir "&chemin_rep"; /*Cration du repertoire  l'aide de la macro-variable*/
			run;
		%end;
%mend;
%dossier(&chemin)
libname libout "&chemin";

/* proc freq data=nostra.f3fm_elu; tables DATETRIM; run; */

data f3;
	set nostra.f3fm_elu;
	keep fap tension oee_ga dee123_ga tension_ga datetrim annee mois;
	attrib annee format=4. mois format=2.;
	annee=substr(datetrim,1,4);
	mois=substr(datetrim,5,2);
	if annee<=&fin and mois<=12;
	oee_ga=(oee+lag(oee)+lag2(oee)+lag3(oee))/4;
	dee123_ga=(dee123+lag(dee123)+lag2(dee123)+lag3(dee123))/4;
	tension_ga=oee_ga/dee123_ga;
run;
data f0;
	set nostra.f0fm_elu;
	keep fap tension oee_ga dee123_ga tension_ga datetrim annee mois;
	attrib annee format=4. mois format=2.;
	annee=substr(datetrim,1,4);
	mois=substr(datetrim,5,2);
	if annee<=&fin and mois<=12;
	oee_ga=(oee+lag(oee)+lag2(oee)+lag3(oee))/4;
	dee123_ga=(dee123+lag(dee123)+lag2(dee123)+lag3(dee123))/4;
	tension_ga=oee_ga/dee123_ga;
	fap="ENS";
run;
data tensions;
	set f0 f3;
	if annee>=1998;
	attrib code format=$9.;
	code=strip(fap)||strip(datetrim);
run;
proc sort data=tensions;by fap;run;

data libout.tensions;set tensions;run;


%macro prep_fichier_tensions(an_deb,an_fin,chemin,nom_tab,nom_graph,onglet);
	%let chemin_psm=d:\documents-utilisateurs\charline.babet\Donnees\PSM\PSM 2015\Modles;
	options missing="";
	options NOXWAIT NOXSYNC ;
	x copy "&chemin_psm.\Modle &nom_graph..xls" "&chemin.\&nom_graph..xls";
	options NOXWAIT NOXSYNC ; *** Open Excel ***; 
	x "start excel"; 
	data _null_ ; 
		x = SLEEP(4) ; 
	run ; 
	filename cmds DDE 'excel|system' ;
	data _null_ ;
		file cmds ;
		put '[FILE-OPEN("'"&chemin.\&nom_graph..xls"'")]' ;
	run ;
	%let plage1=L1C1:L1C5;
	%let plage2=L2C1:L10000C5;
	filename t dde "EXCEL|&chemin.\[&nom_graph..xls]&onglet!&plage1" notab;
	data _null_;
		file t lrecl=500;
		set &nom_tab;
		sp="09"X;
		put "Code" sp "Date" sp "Fap" sp "Tension";
	run;
	 filename t dde "EXCEL|&chemin.\[&nom_graph..xls]&onglet!&plage2" notab;
	data _null_;
		file t lrecl=500;
		set &nom_tab;
		sp="09"X;
		put code $9. sp datetrim $6. sp fap $3. sp tension_ga commax4.3;
		;		
	run;
	options missing=".";
	/*sauvegarde du fichier Excel*/
	FILENAME cmds DDE 'Excel|system';
	DATA _NULL_;
		FILE cmds;
		PUT '[save()]';
	RUN;
	/*fermeture du fichier*/
	data _null_ ;
		file cmds ;
		put "[FILE-CLOSE()]" ;
	run ;
	/*fermer excel*/
	data _null_ ;
		file cmds ;
		put "[QUIT()]" ;
	run ;

%mend;
%prep_fichier_tensions(&deb,&fin,&chemin,tensions,&nom_rep, tensions);

