%include "C:\Users\guillaume.toure\Desktop\DAMETH_TOURE\PSM\preparation_donnees\PSM 2021\Programmes\macros portraits fap225.sas";

%let deb=2003;
%let fin=2020;
%let nom=t3;
%let nom_rep=Tableau_3bis Sexe fap225;

libname eec "C:\Users\guillaume.toure\Desktop\DAMETH_TOURE\PSM\preparation_donnees\Series longues 2021\Bases SAS\sÚries_longues";

%let chemin=C:\Users\guillaume.toure\Desktop\DAMETH_TOURE\PSM\preparation_donnees\PSM 2021\Sorties\&nom_rep;
libname libout "&chemin";

%macro prep_sexe(lib);

%do an=&deb %to &fin;
	data sl_dmq_&an;
		set &lib..sl_dmq_&an. ;
		%if &an<1990 %then %do;
			keep  idt_aire im_loc noi fap_3 fap_5 fap_1 poids_final age_enq var seuil
			jj ag_num naim mm rep_age_enq sexe_cal rep_age_enq_3;
		%end;
		%if &an>=1990 and &an<2003 %then %do;
			keep  aire imloc noi fap_3 fap_5 fap_1 poids_final age_enq var seuil
			jj ag_num naim mm rep_age_enq sexe_cal rep_age_enq_3;
		%end;
		%if &an>=2003 %then %do;
			keep  ident noi fap_3 fap_5 fap_1 poids_final annee age age_enq
			rep_age_enq sexe_cal rep_age_enq_3;
		%end;

		%if  &an.<1990 or &an.>2002 %then %do;
			age_enq=age;
		%end;
		%else %do;
			var=ranuni(-1);
			seuil=JJ/31;
			ag_num=input(ag,3.);
			age_enq=ag-(NAIM>MM)-(NAIM=MM)*(var>seuil);
		%end;
		select ;
		when (age_enq<25) rep_age_enq='15';
		when (age_enq<30) rep_age_enq='25';
		when (age_enq<35) rep_age_enq='30';
		when (age_enq<40) rep_age_enq='35';
		when (age_enq<45) rep_age_enq='40';
		when (age_enq<50) rep_age_enq='45';
		when (age_enq<55) rep_age_enq='50';
		when (age_enq<60) rep_age_enq='55';
		when (age_enq>=60) rep_age_enq='60';
		otherwise rep_age_enq='XX';
		end;

		select ;
		when (age_enq<30) rep_age_enq_3='3-';
		when (age_enq<50) rep_age_enq_3='5-';
		when (age_enq>=50) rep_age_enq_3='5+';
		otherwise rep_age_enq_3='XX';
		end;
		run;

	run;
%end;
%mend;

%prep_sexe(eec);

%calculs_fap225(chemin_res=&chemin,nom=&nom,an_deb=&deb,an_fin=&fin,nom_table_prep=sl_dmq,
	lib_donnees=work,lib_sortie=libout,var=sexe_cal,var_ventil=rep_age_enq_3,
	var_pond=poids_final,seuil_diffusion=100);

%prep_fichier_fap225(an_deb=&deb,an_fin=&fin,chemin=&chemin,nom_tab=libout.res&nom._&deb._&fin,
	nom_tab_diff=libout.resdiff&nom._&deb._&fin,nom_graph=&nom_rep,var_ventil=rep_age_enq_3,
	var=sexe_cal);

%prep_graph_taille_ech_fap225(an_deb=&deb,an_fin=&fin,chemin=&chemin,
	nom_tab=libout.taille_ech&nom._&deb._&fin,nom_graph=Graphique taille ech ag,
	titre="Taille de l'Úchantillon par classe d'ge (Tableau 3)", 
	var_ventil=rep_age_enq_3)

