%include "pgm_macro_portrait.";
libname eec "&donnees_eec.";

libname map "&lib_map.";
%include "&lib_formats.";

%let nom=c3;
%let nom_rep=Carte_3 DEFM ABC;

libname nostra "&donnees_nostra.";
%let annee=14;

%let chemin=&lib_sortie.\&nom_rep;
libname libout "&chemin";
%let chemin_input=&lib_sortie.\&nom_rep;

%macro dossier(chemin_rep);
	options NOXWAIT NOXSYNC ;
		%let rc=%sysfunc(filename(fileref, &chemin_rep));
		 %if %sysfunc(fexist(&fileref))  %then %do ; 
		 	%end;
			%else %do;
			Data _null_;
				X mkdir "&chemin_rep"; /*Cration du repertoire  l'aide de la macro-variable*/
			run;
		%end;
%mend;
%dossier(&chemin)
libname libout "&chemin";

data donnees_nostra;
	set nostra.Deoe_f9nt&annee.t1 nostra.Deoe_f9nt&annee.t2 nostra.Deoe_f9nt&annee.t3 nostra.Deoe_f9nt&annee.t4 ;
	attrib fap_3 format=$3.;
	fap_3=substr(fap,1,3);
	if reg<"10" or reg>"95" then delete; /*on ne garde que la France mtropolitaine*/

	/* Nouvelles rgions  partir de 2016. */
	if reg in ("11") then code_reg="11";
	if reg in ("24") then code_reg="24";
	if reg in ("26","43") then code_reg="27";
	if reg in ("23","25") then code_reg="28";
	if reg in ("31","22") then code_reg="32";
	if reg in ("41","42","21") then code_reg="44";
	if reg in ("52") then code_reg="52";
	if reg in ("53") then code_reg="53";
	if reg in ("72","54","74") then code_reg="75";
	if reg in ("73","91") then code_reg="76";
	if reg in ("82","83") then code_reg="84";
	if reg in ("93") then code_reg="93";
	if reg in ("94") then code_reg="94";

run;

proc summary data=donnees_nostra nway;
class code_reg fap_3;
var defm_abc ;
output out=a sum=emp_fap;
run;

proc summary data=donnees_nostra nway;
class code_reg;
var defm_abc ;
output out=b sum=emp_region;
run;

data input;
merge a b;
by code_reg;
if emp_fap=. then emp_fap=0;
variable=100*emp_fap/emp_region;
drop _type_ _freq_;
run;

proc sort data=input;
by fap_3;
run;

proc format;                                   
   value cl_pct  0-0.1 = '0,0 - 0,1' 
   				 0.1 - 0.25 ='0,1 - 0,25'
   				 0.25 - 0.5 ='0,25 - 0,5'
				 0.5 - 0.75= '0,5 - 0,75'   
				 0.75 - 1= '0,75 - 1,0'   
                 1 - 1.5= '1,0 - 1,5' 
                 1.5 - 2= '1,5 - 2,0' 
                 2 - 2.5= '2,0 - 2,5'
                 2.5 - 3= '2,5 - 3,0' 
				 3 - 3.5= '3,0 - 3,5' 
				 3.5 - 4= '3,5 - 4,0' 
				 4 - 4.5= '4,0 - 4,5' 
 				 4.5 - 5= '4,5 - 5,0' 
	             5 - 7.5 ='5,0 - 7,5'
				 7.5 - 10='7,5 - 10';

run;


data input;
set input;
var_classe=put(variable,cl_pct.);
run;

%macro carte_3(fap);
goptions reset=pattern  ftext='Arial' ftitle='Arial' device=png gsfname=out htext=0.9
xmax=3.2in ymax=2.3in ;

filename out "&chemin_input\&fap..png" ;
title;

pattern1 value=solid color=CXCCFFFF;
pattern2 value=solid color=CX33CCCC;
pattern3 value=solid color=CX009999;
pattern4 value=solid color=CX006666 /*CX008080*/;
pattern5 value=solid color=CX006600;
pattern6 value=solid color=CX339933;
pattern7 value=solid color=CX99CC00;
pattern8 value=solid color=CXFFCC00;
/* pattern9 value=solid color=;
pattern10 value=solid color=; */

ods _all_ close;
ods listing;

PROC GMAP DATA =input MAP = map.france_metro_newreg ;
	where fap_3="&fap.";
ID code_reg ;
	CHORO var_classe / discrete coutline=black /*HTML=infobulle*/;
	label var_classe='En %' ;
RUN ; 
QUIT ;

ods listing close;

%mend;

%carte_3(A0Z);
%carte_3(A1Z);
%carte_3(A2Z);
%carte_3(A3Z);
%carte_3(B0Z);
%carte_3(B1Z);
%carte_3(B2Z);
%carte_3(B3Z);
%carte_3(B4Z);
%carte_3(B5Z);
%carte_3(B6Z);
%carte_3(B7Z);
%carte_3(C0Z);
%carte_3(C1Z);
%carte_3(C2Z);
%carte_3(D0Z);
%carte_3(D1Z);
%carte_3(D2Z);
%carte_3(D3Z);
%carte_3(D4Z);
%carte_3(D6Z);
%carte_3(E0Z);
%carte_3(E1Z);
%carte_3(E2Z);
%carte_3(F0Z);
%carte_3(F1Z);
%carte_3(F2Z);
%carte_3(F3Z);
%carte_3(F4Z);
%carte_3(F5Z);
%carte_3(G0A);
%carte_3(G0B);
%carte_3(G1Z);
%carte_3(H0Z);
%carte_3(J0Z);
%carte_3(J1Z);
%carte_3(J3Z);
%carte_3(J4Z);
%carte_3(J5Z);
%carte_3(J6Z);
%carte_3(K0Z);
%carte_3(L0Z);
%carte_3(L1Z);
%carte_3(L2Z);
%carte_3(L3Z);
%carte_3(L4Z);
%carte_3(L5Z);
%carte_3(L6Z);
%carte_3(M0Z);
%carte_3(M1Z);
%carte_3(M2Z);
%carte_3(N0Z);
%carte_3(P0Z);
%carte_3(P1Z);
%carte_3(P2Z);
%carte_3(P3Z);
%carte_3(P4Z);
%carte_3(Q0Z);
%carte_3(Q1Z);
%carte_3(Q2Z);
%carte_3(R0Z);
%carte_3(R1Z);
%carte_3(R2Z);
%carte_3(R3Z);
%carte_3(R4Z);
%carte_3(S0Z);
%carte_3(S1Z);
%carte_3(S2Z);
%carte_3(S3Z);
%carte_3(T0Z);
%carte_3(T1Z);
%carte_3(T2A);
%carte_3(T2B);
%carte_3(T3Z);
%carte_3(T4Z);
%carte_3(T6Z);
%carte_3(U0Z);
%carte_3(U1Z);
%carte_3(V0Z);
%carte_3(V1Z);
%carte_3(V2Z);
%carte_3(V3Z);
%carte_3(V4Z);
%carte_3(V5Z);
%carte_3(W0Z);
%carte_3(W1Z);
%carte_3(X0Z);



data libout.input;
set input;
length nom_reg  $32;
length GID  $10;
var_classe=put(variable,cl_pct.);
if code_reg="01" then do; GID="Dom____"; nom_reg="Guadeloupe";end;
if code_reg="02" then do; GID="Dom____"; nom_reg="Martinique";end;
if code_reg="03" then do; GID="Dom____"; nom_reg="Guyane";end;
if code_reg="04" then do; GID="Dom____"; nom_reg="La R�union";end;
if code_reg="06" then do; GID="Dom____"; nom_reg="Mayotte";end;
if code_reg="11" then do; GID="FRA.8_1"; nom_reg="�le-de-France";end;
if code_reg="24" then do; GID="FRA.4_1"; nom_reg="Centre-Val de Loire";end;
if code_reg="27" then do; GID="FRA.2_1"; nom_reg="Bourgogne-Franche-Comt�";end;
if code_reg="28" then do; GID="FRA.9_1"; nom_reg="Normandie";end;
if code_reg="32" then do; GID="FRA.7_1"; nom_reg="Hauts-de-France";end;
if code_reg="44" then do; GID="FRA.6_1"; nom_reg="Grand Est";end;
if code_reg="52" then do; GID="FRA.12_1"; nom_reg="Pays de la Loire";end;
if code_reg="53" then do; GID="FRA.3_1"; nom_reg="Bretagne";end;
if code_reg="75" then do; GID="FRA.10_1"; nom_reg="Nouvelle-Aquitaine";end;
if code_reg="76" then do; GID="FRA.11_1"; nom_reg="Occitanie";end;
if code_reg="84" then do; GID="FRA.1_1"; nom_reg="Auvergne-Rh�ne-Alpes";end;
if code_reg="93" then do; GID="FRA.13_1"; nom_reg="Provence-Alpes-C�te d'Azur";end;
if code_reg="94" then do; GID="FRA.5_1"; nom_reg="Corse";end;
run;

ods html file="&chemin_input.\input_carte3.xls";
proc print data=libout.input noobs;
run; 
ods html close;
