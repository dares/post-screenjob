
proc import replace file="C:\Users\guillaume.toure\Desktop\DAMETH_TOURE\PSM\preparation_donnees\Series longues 2021\Programmes et sorties SAS 2021\4_Marges.xls"
dbms="xls"
out=marges;
run;

/* Cas particulier de 2002. */
data calage2002;
set calage2002;

	if cs_sexe_ag3_cal=1 then csa1=1; else csa1=0;
	if cs_sexe_ag3_cal=2 then csa2=1; else csa2=0;
	if cs_sexe_ag3_cal=3 then csa3=1; else csa3=0;
	if cs_sexe_ag3_cal=4 then csa4=1; else csa4=0;
	if cs_sexe_ag3_cal=5 then csa5=1; else csa5=0;
	if cs_sexe_ag3_cal=6 then csa6=1; else csa6=0;
	if cs_sexe_ag3_cal=7 then csa7=1; else csa7=0;
	if cs_sexe_ag3_cal=8 then csa8=1; else csa8=0;
	if cs_sexe_ag3_cal=9 then csa9=1; else csa9=0;
	if cs_sexe_ag3_cal=10 then csa10=1; else csa10=0;
	if cs_sexe_ag3_cal=11 then csa11=1; else csa11=0;
	if cs_sexe_ag3_cal=12 then csa12=1; else csa12=0;
	if cs_sexe_ag3_cal=13 then csa13=1; else csa13=0;
	if cs_sexe_ag3_cal=14 then csa14=1; else csa14=0;
	if cs_sexe_ag3_cal=15 then csa15=1; else csa15=0;
	if cs_sexe_ag3_cal=16 then csa16=1; else csa16=0;
	if cs_sexe_ag3_cal=17 then csa17=1; else csa17=0;
	if cs_sexe_ag3_cal=18 then csa18=1; else csa18=0;
	if cs_sexe_ag3_cal=19 then csa19=1; else csa19=0;
	if cs_sexe_ag3_cal=20 then csa20=1; else csa20=0;
	if cs_sexe_ag3_cal=21 then csa21=1; else csa21=0;
	if cs_sexe_ag3_cal=22 then csa22=1; else csa22=0;
	if cs_sexe_ag3_cal=23 then csa23=1; else csa23=0;
	if cs_sexe_ag3_cal=24 then csa24=1; else csa24=0;
	if cs_sexe_ag3_cal=25 then csa25=1; else csa25=0;
	if cs_sexe_ag3_cal=26 then csa26=1; else csa26=0;
	if cs_sexe_ag3_cal=27 then csa27=1; else csa27=0;
	if cs_sexe_ag3_cal=28 then csa28=1; else csa28=0;
	if cs_sexe_ag3_cal=29 then csa29=1; else csa29=0;
	if cs_sexe_ag3_cal=30 then csa30=1; else csa30=0;
	if cs_sexe_ag3_cal=31 then csa31=1; else csa31=0;
	if cs_sexe_ag3_cal=32 then csa32=1; else csa32=0;
	if cs_sexe_ag3_cal=33 then csa33=1; else csa33=0;
	if cs_sexe_ag3_cal=34 then csa34=1; else csa34=0;
	if cs_sexe_ag3_cal=35 then csa35=1; else csa35=0;
	if cs_sexe_ag3_cal=36 then csa36=1; else csa36=0;
	if cs_sexe_ag3_cal=37 then csa37=1; else csa37=0;
	if cs_sexe_ag3_cal=38 then csa38=1; else csa38=0;
	if cs_sexe_ag3_cal=39 then csa39=1; else csa39=0;
	if cs_sexe_ag3_cal=40 then csa40=1; else csa40=0;
	if cs_sexe_ag3_cal=41 then csa41=1; else csa41=0;
	if cs_sexe_ag3_cal=42 then csa42=1; else csa42=0;
	if cs_sexe_ag3_cal=43 then csa43=1; else csa43=0;
	if cs_sexe_ag3_cal=44 then csa44=1; else csa44=0;
	if cs_sexe_ag3_cal=45 then csa45=1; else csa45=0;
	if cs_sexe_ag3_cal=46 then csa46=1; else csa46=0;
	if cs_sexe_ag3_cal=47 then csa47=1; else csa47=0;
	if cs_sexe_ag3_cal=48 then csa48=1; else csa48=0;
	if cs_sexe_ag3_cal=49 then csa49=1; else csa49=0;

run;

/*** Calage final.*/
%macro calage_final;

%do annee=1982 %to 2002;

	data marges&annee.;
	set marges(where=(ANNEE="&annee."));
	drop ANNEE;
	run;

	%calmar (
	data = calage&annee., /* table en entr�e */
	contpoi=NON, /*  ne pas afficher le contenu de la table des poids */
	poids = poids_cal, /* variable de pond�ration initiale */
	ident = ID, /* identifiant des observations */
	datamar = marges&annee., /* table des marges */
	pct = NON, /* les marges ne sont pas donn�es en % */
	m = 2, /* m�thode : 1 = lin�aire, 2 = raking ratio, 3 = logit, 4 = lin�aire tronqu�e */
	up=, /*  borne inf�rieure des rapports de poids (si M=3 ou 4) */ 
    lo=, /*  borne sup�rieure des rapports de poids (si M=3 ou 4) */
	editpoi=,/*  �dition de tous les poids finaux */
	datapoi = a&annee._final, /* table contenant la pond�ration finale */
	poidsfin = poids_final, /*  variable de pond�ration finale */
	stat = oui, /* �diter des statistiques (moyenne, �cart-type, quantiles, valeurs extr�mes...) et des graphiques relatifs aux distributions des variables "rapport de poids" et "pond�ration finale" */
	obseli = oui, /* sortie d'une table __OBSELI contenant les observations �limin�es, les variables du calage, les variables de pond�ration et la variable &IDENT */
	maxiter = 15, /* nombre maximum d'it�rations au cours de l'algorithme de Newton */
	seuil = 0.0001 /* seuil e pour le test d'arr�t de l'algorithme de Newton */
	);

	proc sort data=calage&annee.; by ID; run;
	proc sort data=a&annee._final; by ID; run;
	data calage_2_&annee.;
	merge calage&annee.  a&annee._final;
	by ID;
	run;
	/*  calage&annee. = table EE avec les poids cal�s
		a&annee._final = table contenant uniquement les id et les poids finaux 
		calage_2_&annee. = table EE avec les poids finaux */

%end;

%do annee=2003 %to 2020;
	data calage_2_&annee.;
	set calage&annee.;
	run;
%end;

%mend;

%calage_final;

/*** Sortie des s�ries par FAP, CS et DP. */
%macro series;

%do annee=1982 %to 2014;
	proc summary data=calage_2_&annee. nway missing;
	class fap_1 fap_3 cs;
	var poids_final;
	output out=serie&annee.(drop=_FREQ_ _TYPE_) sum=_&annee.;
	run;
	/* serie&annee. = tables contenant les effectifs par fap*cs de chaque ann�e */
%end;

data serie;
merge %do annee=1982 %to 2014; serie&annee. %end;;
by fap_1 FAP_3 cs;
run;
/* serie = table contenant les s�ries pas FAP*CS */

proc summary data=serie nway missing;
class FAP_3;
var _1982-_2014;
output out=series_fap(drop=_FREQ_ _TYPE_) sum=;
run;
/* series_fap = table contenant les s�ries pas FAP */

proc summary data=serie nway missing;
class cs;
var _1982-_2014;
output out=series_cs(drop=_FREQ_ _TYPE_) sum=;
run;
/* series_cs = table contenant les s�ries pas CS */

proc summary data=serie nway missing;
class FAP_1;
var _1982-_2014;
output out=series_dp(drop=_FREQ_ _TYPE_) sum=;
run;
/* series_dp = table contenant les s�ries pas DP */

%mend;

%series;

proc export data=series_fap 
		 	outfile="C:\Users\guillaume.toure\Desktop\DAMETH_TOURE\PSM\preparation_donnees\Series longues 2021\Methodo\X_Series a chaque etape\Sorties SAS\4_Calage_series par FAP.xls" 
			DBMS=tab REPLACE; 
run;
proc export data=series_cs 
		 	outfile="C:\Users\guillaume.toure\Desktop\DAMETH_TOURE\PSM\preparation_donnees\Series longues 2021\Methodo\X_Series a chaque etape\Sorties SAS\4_Calage_series par CS.xls" 
			DBMS=tab REPLACE; 
run;
proc export data=series_dp 
		 	outfile="C:\Users\guillaume.toure\Desktop\DAMETH_TOURE\PSM\preparation_donnees\Series longues 2021\Methodo\X_Series a chaque etape\Sorties SAS\4_Calage_series par DP.xls" 
			DBMS=tab REPLACE; 
run;
