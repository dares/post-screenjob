%include "d:\documents-utilisateurs\charline.babet\Donnees\PSM\PSM 2015\Programmes\macros portraits.sas";

%let deb=1997;
%let fin=2014;
%let nom=g14;
%let nom_rep=Graphique_14 Age DE;

libname nostra "F:\Sources\Tensions\PSM 2015";
/* Chemin o sont stockes les rsultats trimestriels. On peut les constituer  l'aide
du programme PGM_FAP2012T1_DA.sas sous Donnes/XFAP2009/Programme(sas_creation) (dans ce cas
il faut au pralable rcuprer les donnes sous W ou auprs du dpartement March du Travail),
ou demander  Yannick Croguennec. */
%let annee=14;

%let chemin=d:\documents-utilisateurs\charline.babet\Donnees\PSM\PSM 2015\Sorties\&nom_rep;
libname libout "&chemin";

%macro dossier(chemin_rep);
	options NOXWAIT NOXSYNC ;
		%let rc=%sysfunc(filename(fileref, &chemin_rep));
		 %if %sysfunc(fexist(&fileref))  %then %do ; 
		 	%end;
			%else %do;
			Data _null_;
				X mkdir "&chemin_rep"; /*Cration du repertoire  l'aide de la macro-variable*/
			run;
		%end;
%mend;
%dossier(&chemin)
libname libout "&chemin";

data donnees_nostra;
	set nostra.Deoe_f9nt&annee.t1 nostra.Deoe_f9nt&annee.t2 nostra.Deoe_f9nt&annee.t3 nostra.Deoe_f9nt&annee.t4 ;
	attrib fap_3 format=$3.;
	fap_3=substr(fap,1,3);
	if reg<"10" or reg>"95" then delete; /*on ne garde que la France mtropolitaine*/
run;
proc sort data=donnees_nostra;by fap_3;run; 
proc summary data=donnees_nostra noprint;
	var F_DEFM_A F_AGEM1A F_AGEM2A F_AGEM3A F_AGEM4A F_AGEM5A;
	class fap_3;
	output out=res sum=F_DEFM_A F_AGEM1A F_AGEM2A F_AGEM3A F_AGEM4A F_AGEM5A;
run;
data libout.res;
	set res;
	if fap_3="" then fap_3="ENS";
	F_DEFM_A=F_DEFM_A/4;
	F_AGEM1A=F_AGEM1A/4;
	F_AGEM2A=F_AGEM2A/4;
	F_AGEM3A=F_AGEM3A/4;
	F_AGEM4A=F_AGEM4A/4;
	F_AGEM5A=F_AGEM5A/4;
	part_agem1 = 100*F_AGEM1A/F_DEFM_A;
	part_agem2 = 100*F_AGEM2A/F_DEFM_A;
	part_agem3 = 100*F_AGEM3A/F_DEFM_A;
	part_agem4 = 100*F_AGEM4A/F_DEFM_A;
	part_agem5 = 100*F_AGEM5A/F_DEFM_A;
	part_tot=100*(F_AGEM1A+F_AGEM2A+F_AGEM3A+F_AGEM4A+F_AGEM5A)/F_DEFM_A;
	drop _type_ _freq_;
run;


%macro prep_fichier_g14(chemin,nom_tab,nom_graph);
	%let chemin_psm=d:\documents-utilisateurs\charline.babet\Donnees\PSM\PSM 2015\Modles;
	options missing="";
	options NOXWAIT NOXSYNC ;
	x copy "&chemin_psm.\Modle &nom_graph..xls" "&chemin.\&nom_graph..xls";
	options NOXWAIT NOXSYNC ; *** Open Excel ***; 
	x "start excel"; 
	data _null_ ; 
		x = SLEEP(4) ; 
	run ; 
	filename cmds DDE 'excel|system' ;
	data _null_ ;
		file cmds ;
		put '[FILE-OPEN("'"&chemin.\&nom_graph..xls"'")]' ;
	run ;
	%let plage1=L1C1:L1C40;
	%let plage2=L2C1:L1000C40;
	filename t dde "EXCEL|&chemin.\[&nom_graph..xls]eff!&plage1" notab;
	data _null_;
		file t lrecl=500;
		set &nom_tab;
		sp="09"X;
		put "Fap3" sp "F_DEFM_A" sp "F_AGEM1A" sp
		"F_AGEM2A" sp "F_AGEM3A" sp "F_AGEM4A" sp "F_AGEM5A"
		;		
	run;
	 filename t dde "EXCEL|&chemin.\[&nom_graph..xls]eff!&plage2" notab;
	data _null_;
		file t lrecl=500;
		set &nom_tab;
		sp="09"X;
		put fap_3 $3. sp 
		f_defm_a commax6.2 sp f_agem1a commax6.2 sp
		f_agem2a commax6.2 sp f_agem3a commax6.2 sp f_agem4a commax6.2 sp
		f_agem5a commax6.2
		;		
	run;
	filename t dde "EXCEL|&chemin.\[&nom_graph..xls]part!&plage1" notab;
	data _null_;
		file t lrecl=500;
		set &nom_tab;
		sp="09"X;
		put "Fap3" sp "Part_agem1" sp
		"Part_agem2" sp "Part_agem3" sp "Part_agem4" sp "Part_agem5" sp "Part_tot"
		;		
	run;
	 filename t dde "EXCEL|&chemin.\[&nom_graph..xls]part!&plage2" notab;
	data _null_;
		file t lrecl=500;
		set &nom_tab;
		sp="09"X;
		put fap_3 $3. sp 
		part_agem1 commax6.2 sp part_agem2 commax6.2 sp
		part_agem3 commax6.2 sp part_agem4 commax6.2 sp part_agem5 commax6.2 sp
		part_tot commax6.2
		;		
	run;
	options missing=".";
	/*sauvegarde du fichier Excel*/
	FILENAME cmds DDE 'Excel|system';
	DATA _NULL_;
		FILE cmds;
		PUT '[save()]';
	RUN;
	/*fermeture du fichier*/
	data _null_ ;
		file cmds ;
		put "[FILE-CLOSE()]" ;
	run ;
	/*fermer excel*/
	data _null_ ;
		file cmds ;
		put "[QUIT()]" ;
	run ;

%mend;
%prep_fichier_g14(&chemin,libout.res,&nom_rep)
