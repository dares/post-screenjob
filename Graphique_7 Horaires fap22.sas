%include "C:\Users\guillaume.toure\Desktop\DAMETH_TOURE\PSM\preparation_donnees\PSM 2021\Programmes\macros portraits fap22.sas";

%let deb=2003;
%let fin=2020 /*2014*/;
%let nom=g7;
%let nom_rep=Graphique_7 Horaires fap22;

libname eec "C:\Users\guillaume.toure\Desktop\DAMETH_TOURE\PSM\preparation_donnees\Series longues 2021\Bases SAS\s�ries_longues";

%let chemin=C:\Users\guillaume.toure\Desktop\DAMETH_TOURE\PSM\preparation_donnees\PSM 2021\Sorties\&nom_rep;
libname libout "&chemin";

%macro prep_horaires(lib);

%do an=&deb %to &fin;
	data sl_dmq_&an;
	set &lib..sl_dmq_&an. ;

		%if &an<=2012 %then %do;
		if nuitc in ('1','2') then rep_nuit="1";
		if nuitc="3" then rep_nuit="0";
		if nuitc="" then rep_nuit="";
		if samedc in ('1','2') then rep_sam="1";
		if samedc="3" then rep_sam="0";
		if samedc="" then rep_sam="";
		if dimanc in ('1','2') then rep_dim="1";
		if dimanc="3" then rep_dim="0";
		if dimanc="" then rep_dim="";
		%end;
		%if &an.>2012 %then %do;
		if nuitc in ('1','2') then rep_nuit="1";
		if nuitc="3" then rep_nuit="0";
		if nuitc="" then rep_nuit="";
		if samedic in ('1','2') then rep_sam="1";
		if samedic="3" then rep_sam="0";
		if samedic="" then rep_sam="";
		if dimanc in ('1','2') then rep_dim="1";
		if dimanc="3" then rep_dim="0";
		if dimanc="" then rep_dim="";
		%end;

			/* Questions poses uniquement en 1e et dernire interrogation  partir de 2013. */
			%if %eval(&an.)<=2012 %then %do;
				poidsh=poids_final; 
			%end;
			%if %eval(&an.)=2013 %then %do;
				/*poidsh=(extriDF15/4)/1000*/ poidsh=(extriDF/4)/1000;
			%end;
			%if %eval(&an.)=2014 %then %do;
				/*poidsh=(extriDF15/4)/1000*/ poidsh=(extriDF/4)/1000;
			%end;
			%if %eval(&an.)>2014 %then %do; 
				/*poidsh=(extriDF15/4)/1000;*/ poidsh=(extriDF/4)/1000;
			%end;

	run;
%end;
%mend;

%prep_horaires(eec);

/*pour ce graphique, on utilise plusieurs variables -> on lance plusieurs fois la macro
calculs et on renomme les tables en sortie. Si les tables existent dj, il faut au pralable
les supprimer (sinon message d'erreur)*/
%macro renomm(suff);
	PROC DATASETS LIBRARY = libout ;
	 CHANGE resg7_&deb._&fin=resg7&suff._&deb._&fin ; 
	 CHANGE resNRg7_&deb._&fin=resNRg7&suff._&deb._&fin ; 
	 CHANGE resdiffg7_&deb._&fin=resdiffg7&suff._&deb._&fin ;
	 CHANGE Poidsg7_&deb._&fin=Poidsg7&suff._&deb._&fin ;
	 CHANGE Taille_echg7_&deb._&fin=Taille_echg7&suff._&deb._&fin ;
	  QUIT;   RUN  ;
	data libout.resg7&suff._&deb._&fin;
		set libout.resg7&suff._&deb._&fin;
		rename %do an=%eval(&deb+1) %to %eval(&fin-1); eff%eval(&an-1)_%eval(&an+1)=eff&suff%eval(&an-1)_%eval(&an+1)
		part%eval(&an-1)_%eval(&an+1)=part&suff%eval(&an-1)_%eval(&an+1) %end;;
	run;
	data libout.resdiffg7&suff._&deb._&fin;
		set libout.resdiffg7&suff._&deb._&fin;
		rename %do an=%eval(&deb+1) %to %eval(&fin-1); eff%eval(&an-1)_%eval(&an+1)=eff&suff%eval(&an-1)_%eval(&an+1)
		part%eval(&an-1)_%eval(&an+1)=part&suff%eval(&an-1)_%eval(&an+1) %end;;
	run;
%mend;

%calculs_fap22(chemin_res=&chemin,nom=&nom,an_deb=&deb,an_fin=&fin,nom_table_prep=sl_dmq,
	lib_donnees=work,lib_sortie=libout,var=rep_nuit,var_ventil=,var_pond=poidsh,
	seuil_diffusion=100);
%renomm(n);

%calculs_fap22(chemin_res=&chemin,nom=&nom,an_deb=&deb,an_fin=&fin,nom_table_prep=sl_dmq,
	lib_donnees=work,lib_sortie=libout,var=rep_sam,var_ventil=,var_pond=poidsh,
	seuil_diffusion=100);
%renomm(s);

%calculs_fap22(chemin_res=&chemin,nom=&nom,an_deb=&deb,an_fin=&fin,nom_table_prep=sl_dmq,
	lib_donnees=work,lib_sortie=libout,var=rep_dim,var_ventil=,var_pond=poidsh,
	seuil_diffusion=100);
%renomm(d);

/*%calculs_fap22(chemin_res=&chemin,nom=&nom,an_deb=&deb,an_fin=&fin,nom_table_prep=sl_dmq,
	lib_donnees=work,lib_sortie=libout,var=rep_atyp,var_ventil=,var_pond=poidsh,
	seuil_diffusion=100);
%renomm(a);*/

data libout.resg7_&deb._&fin;
	merge libout.resg7n_&deb._&fin libout.resg7s_&deb._&fin libout.resg7d_&deb._&fin /*libout.resg7a_&deb._&fin*/;
	by fap_1;
run;
data libout.resdiffg7_&deb._&fin;
	merge libout.resdiffg7n_&deb._&fin libout.resdiffg7s_&deb._&fin libout.resdiffg7d_&deb._&fin /*libout.resdiffg7a_&deb._&fin*/;
	by fap_1;
run;


/* Comme on a plusieurs sries de donn�es, on utilise une version un peu modifie de la
macro prep_fichier. */
%macro prep_fichier_horaires(an_deb=,an_fin=,chemin=,nom_tab=,nom_tab_diff=,
	nom_graph=, liste_var=, liste_suff=);
	%let i=1;
	%do %while(%length(%scan(&liste_var,&i))>0);
		%let var&i=%scan(&liste_var,&i);
		%let i=%eval(&i+1);
	%end;
	%let nbvar=%eval(&i-1);
	%let i=1;
	%do %while(%length(%scan(&liste_suff,&i))>0);
		%let suff&i=%scan(&liste_suff,&i);
		%let i=%eval(&i+1);
	%end;
	%let nbsuff=%eval(&i-1);

	%let chemin_psm=C:\Users\guillaume.toure\Desktop\DAMETH_TOURE\PSM\preparation_donnees\PSM 2021\Mod�les;
	options missing="";
	options NOXWAIT NOXSYNC ;
	x copy "&chemin_psm.\Mod�le &nom_graph..xls" "&chemin.\&nom_graph..xls";
	options NOXWAIT NOXSYNC ; *** Open Excel ***; 
	x "start excel"; 
	data _null_ ; 
		x = SLEEP(4) ; 
	run ; 
	filename cmds DDE 'excel|system' ;
	data _null_ ;
		file cmds ;
		put '[FILE-OPEN("'"&chemin.\&nom_graph..xls"'")]' ;
	run ;
	%let plage1=L1C1:L1C40;
	%let plage2=L2C1:L1000C40;
	%do i=1 %to &nbvar;
		filename t dde "EXCEL|&chemin.\[&nom_graph..xls]eff&&suff&i.._3ans!&plage1" notab;
		data _null_;
			file t lrecl=500;
			set &nom_tab;
			sp="09"X;
			put "Code" sp "Fap" sp %if "&&var&i" ne "" %then %do; "&&var&i" sp %end;
			"%eval(&an_deb)-%eval(&an_deb+2)"  %do an=%eval(&an_deb+2) %to %eval(&an_fin-1); sp "%eval(&an-1)-%eval(&an+1)"  %end;
			;		
		run;
		 filename t dde "EXCEL|&chemin.\[&nom_graph..xls]eff&&suff&i.._3ans!&plage2" notab;
		data _null_;
			file t lrecl=500;
			set &nom_tab;
			sp="09"X;
			put code $5. sp fap_1 $1. sp %if "&&var&i" ne "" %then %do; &&var&i $6. sp %end;
			eff&&suff&i%eval(&an_deb)_%eval(&an_deb+2) commax6.2 %do an=%eval(&an_deb+2) %to %eval(&an_fin-1); sp eff&&suff&i%eval(&an-1)_%eval(&an+1) commax6.2 %end;
			;		
		run;
		%if &&var&i ne %then %do;
			filename t dde "EXCEL|&chemin.\[&nom_graph..xls]part&&suff&i.._3ans!&plage1" notab;
			data _null_;
				file t lrecl=500;
				set &nom_tab;
				sp="09"X;
				put "Code" sp "Fap" sp %if "&&var&i" ne "" %then %do; "&&var&i" sp %end;
				"%eval(&an_deb)-%eval(&an_deb+2)"  %do an=%eval(&an_deb+2) %to %eval(&an_fin-1); sp "%eval(&an-1)-%eval(&an+1)"  %end;
				;		
			run;
			 filename t dde "EXCEL|&chemin.\[&nom_graph..xls]part&&suff&i.._3ans!&plage2" notab;
			data _null_;
				file t lrecl=500;
				set &nom_tab;
				sp="09"X;
				put code $5. sp fap_1 $1. sp %if "&&var&i" ne "" %then %do; &&var&i $6. sp %end;
				part&&suff&i%eval(&an_deb)_%eval(&an_deb+2) commax6.2 %do an=%eval(&an_deb+2) %to %eval(&an_fin-1); sp part&&suff&i%eval(&an-1)_%eval(&an+1) commax6.2 %end;
				;		
			run;
			filename t dde "EXCEL|&chemin.\[&nom_graph..xls]part&&suff&i.._3ans_diff!&plage1" notab;
			data _null_;
				file t lrecl=500;
				set &nom_tab_diff;
				sp="09"X;
				put "Code" sp "Fap" sp %if "&&var&i" ne "" %then %do; "&&var&i" sp %end;
				"%eval(&an_deb)-%eval(&an_deb+2)"  %do an=%eval(&an_deb+2) %to %eval(&an_fin-1); sp "%eval(&an-1)-%eval(&an+1)"  %end;
				;		
			run;
			 filename t dde "EXCEL|&chemin.\[&nom_graph..xls]part&&suff&i.._3ans_diff!&plage2" notab;
			data _null_;
				file t lrecl=500;
				set &nom_tab_diff;
				sp="09"X;
				put code $5. sp fap_1 $1. sp %if "&&var&i" ne "" %then %do; &&var&i $6. sp %end;
				part&&suff&i%eval(&an_deb)_%eval(&an_deb+2) commax6.2 %do an=%eval(&an_deb+2) %to %eval(&an_fin-1); sp part&&suff&i%eval(&an-1)_%eval(&an+1) commax6.2 %end;
				;		
			run;
		%end;
	%end;
	options missing=".";
	/*sauvegarde du fichier Excel*/
	FILENAME cmds DDE 'Excel|system';
	DATA _NULL_;
		FILE cmds;
		PUT '[save()]';
	RUN;
	/*fermeture du fichier*/
	data _null_ ;
		file cmds ;
		put "[FILE-CLOSE()]" ;
	run ;
	/*fermer excel*/
	data _null_ ;
		file cmds ;
		put "[QUIT()]" ;
	run ;

%mend;
%prep_fichier_horaires(an_deb=&deb,an_fin=&fin,chemin=&chemin,
	nom_tab=libout.res&nom._&deb._&fin,nom_tab_diff=libout.resdiff&nom._&deb._&fin,
	nom_graph=&nom_rep,liste_var=rep_nuit rep_sam rep_dim rep_atyp,liste_suff=n s d a);

