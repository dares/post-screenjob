%include "C:\Users\guillaume.toure\Desktop\DAMETH_TOURE\PSM\preparation_donnees\PSM 2021\Programmes\macros portraits fap22.sas";

%let deb=2003;
%let fin=2020;
%let nom=t7;
%let nom_rep=Tableau_7 Salaire m�dian fap22;

libname eec "C:\Users\guillaume.toure\Desktop\DAMETH_TOURE\PSM\preparation_donnees\Series longues 2021\Bases SAS\s�ries_longues";

%let chemin=C:\Users\guillaume.toure\Desktop\DAMETH_TOURE\PSM\preparation_donnees\PSM 2021\Sorties\&nom_rep;
libname libout "&chemin";

%macro prep_sal(lib);

%do an=&deb %to &fin;
	data sl_dmq_&an;
		set &lib..sl_dmq_&an. ;
		keep fap_3 fap_1 salredtr rep_sal app_stag statut ttrref aidref poidsDF extri extriDF
		/*extriDF15*/ tppred stat2 noi ident salaire;
		if salredtr in ('A','B','C') then rep_sal='1250';
		if salredtr in ('D') then rep_sal='1500';
		if salredtr in ('E') then rep_sal='2000';
		if salredtr in ('F','G') then rep_sal='3000';
		if salredtr in ('H','I','J') then rep_sal='3+++';

		app_stag=(statut='22' or ttrref='3' or aidref='2');

			%if %eval(&an.)<2003 %then %do;
				poidsDF=extri; 
			%end;
			%else %do; 
				%if %eval(&an.)<2012 %then %do; 
					poidsDF=extriDF/4;
				%end;
				%if %eval(&an.)=2012 %then %do; 
					poidsDF=extriDF/4;/*poidsDF=extriDF15/4;*/
				%end;
				%if %eval(&an.)=2013 %then %do; 
					poidsDF=extriDF/4;/*poidsDF=extriDF15/4;*/
				%end;
				%if %eval(&an.)=2014 %then %do; 
					poidsDF=extriDF/4;/*poidsDF=extriDF15/4;*/
				%end;
				%if %eval(&an.)>2014 %then %do;  
					poidsDF=extriDF/4;
				%end;
			%end;
			poidsDF=poidsDF/1000;

		%if %eval(&an.)>=2013 %then %do;
			salaire=input(salred,7.);
		%end;
		%else %do; 
			salaire=salred;
		%end;

		if tppred='1' and stat2='2' and app_stag=0;
		run;
%end;
%mend;

%prep_sal(eec);

%macro sal_median(an_deb,an_fin,lib_donnees,nom_table_prep,lib_sortie,var,var_pond);
/* donnes agrges sur 3 ans */
	   %do an=%eval(&an_deb+1) %to %eval(&an_fin-1);
		data combin%eval(&an-1)_%eval(&an+1);
			set &lib_donnees..&nom_table_prep._%eval(&an-1)  
				&lib_donnees..&nom_table_prep._&an 
				&lib_donnees..&nom_table_prep._%eval(&an+1);
			%if (&an ne 1993 and &an ne 1994) %then %do;
				&var_pond=&var_pond/3;
			%end;
		run;
		proc sort data=combin%eval(&an-1)_%eval(&an+1); by fap_1; run;
		proc univariate data=combin%eval(&an-1)_%eval(&an+1) noprint;
			var &var;
			by fap_1;
			weight &var_pond;
			output out=res_%eval(&an-1)_%eval(&an+1) q1=q1_%eval(&an-1)_%eval(&an+1) median=mediane_%eval(&an-1)_%eval(&an+1) q3=q3_%eval(&an-1)_%eval(&an+1);
		run;
		/*Ensemble*/
		proc univariate data=combin%eval(&an-1)_%eval(&an+1) noprint;
			var &var;
			weight &var_pond;
			output out=res_ens_%eval(&an-1)_%eval(&an+1) q1=q1_%eval(&an-1)_%eval(&an+1) median=mediane_%eval(&an-1)_%eval(&an+1) q3=q3_%eval(&an-1)_%eval(&an+1);
		run;

		data res_ens_%eval(&an-1)_%eval(&an+1);
			set res_ens_%eval(&an-1)_%eval(&an+1);
			fap_1="Y";
		run;

		data res%eval(&an-1)_%eval(&an+1);
			set res_%eval(&an-1)_%eval(&an+1) res_ens_%eval(&an-1)_%eval(&an+1);
		run;
		proc sort data=res%eval(&an-1)_%eval(&an+1); by fap_1; run;

	%end; 
	data &lib_sortie..salmedian_&nom._&an_deb._&an_fin;
		merge %do an=%eval(&an_deb+1) %to %eval(&an_fin-1); res%eval(&an-1)_%eval(&an+1) %end;;
		by fap_1;
	run;
%mend;

%sal_median(&deb.,&fin.,work,sl_dmq,libout,salaire,poidsDF);


%macro prep_fichier_salmedian(an_deb,an_fin,chemin,nom_tab,nom_graph);
	%let chemin_psm=C:\Users\christophe.michel\Documents\PSM\PSM 2020\Mod�les;
	options missing="";
	options NOXWAIT NOXSYNC ;
	x copy "&chemin_psm.\Mod�le &nom_graph..xls" "&chemin.\&nom_graph..xls";
	options NOXWAIT NOXSYNC ; *** Open Excel ***; 
	x "start excel"; 
	data _null_ ; 
		x = SLEEP(4) ; 
	run ; 
	filename cmds DDE 'excel|system' ;
	data _null_ ;
		file cmds ;
		put '[FILE-OPEN("'"&chemin.\&nom_graph..xls"'")]' ;
	run ;
	%let plage1=L1C1:L1C60;
	%let plage2=L2C1:L1000C60;
	filename t dde "EXCEL|&chemin.\[&nom_graph..xls]q1!&plage1" notab;
	data _null_;
		file t lrecl=500;
		set &nom_tab;
		sp="09"X;
		put "Fap" sp "%eval(&an_deb)-%eval(&an_deb+2)"  %do an=%eval(&an_deb+2) %to %eval(&an_fin-1); sp "%eval(&an-1)-%eval(&an+1)"  %end;
		;		
	run;
	 filename t dde "EXCEL|&chemin.\[&nom_graph..xls]q1!&plage2" notab;
	data _null_;
		file t lrecl=500;
		set &nom_tab;
		sp="09"X;
		put Fap_1 $1. sp q1_%eval(&an_deb)_%eval(&an_deb+2) commax6.2 %do an=%eval(&an_deb+2) %to %eval(&an_fin-1); sp q1_%eval(&an-1)_%eval(&an+1) commax6.2  %end;
		;		
	run;
	filename t dde "EXCEL|&chemin.\[&nom_graph..xls]q3!&plage1" notab;
	data _null_;
		file t lrecl=500;
		set &nom_tab;
		sp="09"X;
		put "Fap" sp "%eval(&an_deb)-%eval(&an_deb+2)"  %do an=%eval(&an_deb+2) %to %eval(&an_fin-1); sp "%eval(&an-1)-%eval(&an+1)"  %end;
		;		
	run;
	 filename t dde "EXCEL|&chemin.\[&nom_graph..xls]q3!&plage2" notab;
	data _null_;
		file t lrecl=500;
		set &nom_tab;
		sp="09"X;
		put Fap_1 $1. sp q3_%eval(&an_deb)_%eval(&an_deb+2) commax6.2 %do an=%eval(&an_deb+2) %to %eval(&an_fin-1); sp q3_%eval(&an-1)_%eval(&an+1) commax6.2  %end;
		;		
	run;
	filename t dde "EXCEL|&chemin.\[&nom_graph..xls]mediane!&plage1" notab;
	data _null_;
		file t lrecl=500;
		set &nom_tab;
		sp="09"X;
		put "Fap" sp "%eval(&an_deb)-%eval(&an_deb+2)"  %do an=%eval(&an_deb+2) %to %eval(&an_fin-1); sp "%eval(&an-1)-%eval(&an+1)"  %end;
		;		
	run;
	 filename t dde "EXCEL|&chemin.\[&nom_graph..xls]mediane!&plage2" notab;
	data _null_;
		file t lrecl=500;
		set &nom_tab;
		sp="09"X;
		put Fap_1 $1. sp mediane_%eval(&an_deb)_%eval(&an_deb+2) commax6.2 %do an=%eval(&an_deb+2) %to %eval(&an_fin-1); sp mediane_%eval(&an-1)_%eval(&an+1) commax6.2  %end;
		;		
	run;
	options missing=".";
	/*sauvegarde du fichier Excel*/
	FILENAME cmds DDE 'Excel|system';
	DATA _NULL_;
		FILE cmds;
		PUT '[save()]';
	RUN;
	/*fermeture du fichier*/
	data _null_ ;
		file cmds ;
		put "[FILE-CLOSE()]" ;
	run ;
	/*fermer excel*/
	data _null_ ;
		file cmds ;
		put "[QUIT()]" ;
	run ;

%mend;
%let nom_graph=Tableau_7 Salaire m�dian fap22;
%prep_fichier_salmedian(&deb,&fin,&chemin,libout.salmedian_&nom._&deb._&fin,&nom_graph);





