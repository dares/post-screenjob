%include "C:\Users\guillaume.toure\Desktop\DAMETH_TOURE\PSM\preparation_donnees\PSM 2021\Programmes\macros portraits fap225.sas";

%let deb=2003;
%let fin=2020/*2014*/;
%let nom=g8;
%let nom_rep=Graphique_8 Salaires fap225;

libname eec "C:\Users\guillaume.toure\Desktop\DAMETH_TOURE\PSM\preparation_donnees\Series longues 2021\Bases SAS\s�ries_longues";

%let chemin=C:\Users\guillaume.toure\Desktop\DAMETH_TOURE\PSM\preparation_donnees\PSM 2021\Sorties\&nom_rep;
libname libout "&chemin";

%macro prep_sal(lib);

%do an=&deb %to &fin;
	data sl_dmq_&an;
		set &lib..sl_dmq_&an. ;
		keep fap_3 fap_5 salredtr rep_sal app_stag statut ttrref aidref poidsDF extri extriDF
		extriDF15 tppred stat2 noi ident;

		if salredtr in ('A','B','C') then rep_sal='1250';
		if salredtr in ('D') then rep_sal='1500';
		if salredtr in ('E') then rep_sal='2000';
		if salredtr in ('F','G') then rep_sal='3000';
		if salredtr in ('H','I','J') then rep_sal='3+++';

		app_stag=(statut='22' or ttrref='3' or aidref='2');

			%if %eval(&an.)<2003 %then %do;
				poidsDF=extri; 
			%end;
			%else %do; 
				%if %eval(&an.)<2012 %then %do; 
					poidsDF=extriDF/4;
				%end;
				%if %eval(&an.)=2012 %then %do; 
					poidsDF=extriDF/4; /*poidsDF=extriDF15/4;*/
				%end;
				%if %eval(&an.)=2013 %then %do; 
					poidsDF=extriDF/4; /*poidsDF=extriDF15/4;*/
				%end;
				%if %eval(&an.)=2014 %then %do; 
					poidsDF=extriDF/4;/*poidsDF=extriDF15/4;*/
				%end;
				%if %eval(&an.)>2014 %then %do;  
					poidsDF=extriDF/4;
				%end;
			%end;
			poidsDF=poidsDF/1000;

		if tppred='1' and stat2='2' and app_stag=0;
		run;
%end;
%mend;

%prep_sal(eec);

%calculs_fap225(chemin_res=&chemin,nom=&nom,an_deb=&deb,an_fin=&fin,nom_table_prep=sl_dmq,
	lib_donnees=work,lib_sortie=libout,var=rep_sal,var_ventil=,var_pond=poidsDF,
	seuil_diffusion=100);

%prep_fichier_fap225(an_deb=&deb,an_fin=&fin,chemin=&chemin,nom_tab=libout.res&nom._&deb._&fin,
	nom_tab_diff=libout.resdiff&nom._&deb._&fin,nom_graph=&nom_rep,var=rep_sal,
	var_ventil=);

%prep_graph_taille_ech_fap225(an_deb=&deb,an_fin=&fin,chemin=&chemin,
	nom_tab=libout.taille_ech&nom._&deb._&fin,
	nom_graph=Graphique taille ech sal tps comp,
	titre="Taille de l'�chantillon, salari�s  tps complet hors apprentis et stagiaires (Graphique 8 et Tableau 7)",
	var_ventil=);

