
%include "C:\Users\guillaume.toure\Desktop\DAMETH_TOURE\PSM\preparation_donnees\PSM 2021\Programmes\macros portraits fap225.sas";

%let deb=2003;
%let fin=2020;
%let nom=t4;
%let nom_rep=Tableau_4 Sp�cialit�s dipl�mes -30 fap225;

libname eec "C:\Users\guillaume.toure\Desktop\DAMETH_TOURE\PSM\preparation_donnees\Series longues 2021\Bases SAS\s�ries_longues";

%let chemin=C:\Users\guillaume.toure\Desktop\DAMETH_TOURE\PSM\preparation_donnees\PSM 2021\Sorties\&nom_rep;
libname libout "&chemin";

%macro prep_spedip(lib);

%do an=&deb %to &fin;
	data sl_dmq_&an;
		set &lib..sl_dmq_&an. ;
		%if &an<1990 %then %do;
			keep  idt_aire im_loc noi fap_3 fap_1 fap_5 poids_final age_enq var seuil
			jj ag_num naim mm rep_age_enq rep_spe forter ddipl;
		%end;
		%if &an>=1990 and &an<2003 %then %do;
			keep  aire imloc noi fap_3 fap_1 fap_5 poids_final age_enq var seuil
			jj ag_num naim mm rep_age_enq rep_spe forter ddipl;
		%end;
		%if &an>=2003 %then %do;
			keep  ident noi fap_3 fap_1 fap_5 poids_final annee age age_enq
			rep_age_enq rep_spe forter ddipl;
		%end;

		%if  &an.<1990 or &an.>2002 %then %do;
			age_enq=age;
		%end;
		%else %do;
			var=ranuni(-1);
			seuil=JJ/31;
			ag_num=input(ag,3.);
			age_enq=ag-(NAIM>MM)-(NAIM=MM)*(var>seuil);
		%end;
		select ;
		when (age_enq<25) rep_age_enq='15';
		when (age_enq<30) rep_age_enq='25';
		when (age_enq<35) rep_age_enq='30';
		when (age_enq<40) rep_age_enq='35';
		when (age_enq<45) rep_age_enq='40';
		when (age_enq<50) rep_age_enq='45';
		when (age_enq<55) rep_age_enq='50';
		when (age_enq<60) rep_age_enq='55';
		when (age_enq>=60) rep_age_enq='60';
		otherwise rep_age_enq='XX';
		end;

		rep_spe=spe;
		if spe in('  0','') then rep_spe='';
		/*changer en "000"*/
			
		if  forter="2"
		then rep_dip="EC";
		else rep_dip=DDIPL;

		if rep_dip='' then rep_dip="7";

		if rep_age_enq<'30' and forter ne "2" and ddipl in("1","3","4","5");
		
	run;
%end;
%mend;

%prep_spedip(eec);

%calculs_fap225(chemin_res=&chemin,nom=&nom,an_deb=&deb,an_fin=&fin,
	nom_table_prep=sl_dmq,lib_donnees=work,lib_sortie=libout,var=rep_spe,
	var_ventil=,var_pond=poids_final,seuil_diffusion=100);


data res&nom._&deb._&fin.;
	set libout.res&nom._&deb._&fin.;
run;
proc sort data=res&nom._&deb._&fin.;by fap_5 descending part%eval(&fin.-2)_&fin.;run;
data res&nom._&deb._&fin.;
	set res&nom._&deb._&fin.;
	by fap_5;
	retain ordre 1;
	ordre=ordre+1;
	if first.fap_5 then ordre=1;
	drop code;
run;
data res&nom._&deb._&fin.;
	set res&nom._&deb._&fin. (where=(part%eval(&fin.-2)_&fin.>=3 or ordre<=4));
	code=strip(fap_5)||strip(ordre);
run;

data resdiff&nom._&deb._&fin.;
	set libout.resdiff&nom._&deb._&fin.;
run;
proc sort data=resdiff&nom._&deb._&fin.;by fap_5 descending part%eval(&fin.-2)_&fin.;run;
data resdiff&nom._&deb._&fin.;
	set resdiff&nom._&deb._&fin.;
	by fap_5;
	retain ordre 1;
	ordre=ordre+1;
	if first.fap_5 then ordre=1;
	drop code;
run;
data resdiff&nom._&deb._&fin.;
	set resdiff&nom._&deb._&fin. (where=(part%eval(&fin.-2)_&fin.>=3 ));
	code=strip(fap_5)||strip(ordre);
run;

%prep_fichier_fap225(an_deb=&deb,an_fin=&fin,chemin=&chemin,nom_tab=res&nom._&deb._&fin.,
	nom_tab_diff=resdiff&nom._&deb._&fin.,nom_graph=&nom_rep,var=rep_spe,var_ventil=);

%prep_graph_taille_ech_fap225(an_deb=&deb,an_fin=&fin,chemin=&chemin,
	nom_tab=libout.taille_ech&nom._&deb._&fin,
	nom_graph=Graphique taille ech m30 diplmin CAPBEP,
	titre="Taille de l'�chantillon, moins de 30 ans ayant achev� leurs �tudes et poss�dant au moins un dipl�me de niveau CAP ou BEP (Tableau 4)",
	var_ventil=)




