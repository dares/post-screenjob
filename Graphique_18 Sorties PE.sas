%include "d:\documents-utilisateurs\charline.babet\Donnees\PSM\PSM 2015\Programmes\macros portraits.sas";

%let nom=g18;
%let nom_rep=Graphique_18 Sorties PE;

%let chemin=d:\documents-utilisateurs\charline.babet\Donnees\PSM\PSM 2015\Sorties\&nom_rep;
libname libout "&chemin";

libname nostra "F:\Sources\Tensions\PSM 2015";
%let annee=14;

%macro dossier(chemin_rep);
	options NOXWAIT NOXSYNC ;
		%let rc=%sysfunc(filename(fileref, &chemin_rep));
		 %if %sysfunc(fexist(&fileref))  %then %do ; 
		 	%end;
			%else %do;
			Data _null_;
				X mkdir "&chemin_rep"; /*Cration du repertoire  l'aide de la macro-variable*/
			run;
		%end;
%mend;
%dossier(&chemin)
libname libout "&chemin";

data donnees_nostra;
	set nostra.Deoe_f9nt&annee.t1 nostra.Deoe_f9nt&annee.t2 nostra.Deoe_f9nt&annee.t3 nostra.Deoe_f9nt&annee.t4 ;
	attrib fap_3 format=$3.;
	fap_3=substr(fap,1,3);
	if reg<"10" or reg>"95" then delete; /*on ne garde que la France mtropolitaine*/
run;
proc sort data=donnees_nostra;by fap_3;run; 
proc summary data=donnees_nostra noprint;
	var s_des_t s_repemp s_stage s_arretx s_radiat s_noactu s_autrex;
	class fap_3;
	output out=res(drop=_type_ _freq_) sum=s_des_t s_repemp s_stage s_arretx s_radiat s_noactu s_autrex;
run;


data libout.res;
	set res;
	part_s_repemp=100*s_repemp/s_des_t;
	part_s_stage=100*s_stage/s_des_t;
	part_s_arretx=100*s_arretx/s_des_t;
	part_s_radiat=100*s_radiat/s_des_t;
	part_s_noactu=100*s_noactu/s_des_t;
	part_s_autrex=100*s_autrex/s_des_t;
	tot=part_s_repemp+part_s_stage+part_s_arretx+part_s_radiat+part_s_noactu+part_s_autrex;
run;

%macro prep_fichier_g18(chemin,nom_tab,nom_graph);
	%let chemin_psm=d:\documents-utilisateurs\charline.babet\Donnees\PSM\PSM 2015\Modles;
	options missing="";
	options NOXWAIT NOXSYNC ;
	x copy "&chemin_psm.\Modle &nom_graph..xls" "&chemin.\&nom_graph..xls";
	options NOXWAIT NOXSYNC ; *** Open Excel ***; 
	x "start excel"; 
	data _null_ ; 
		x = SLEEP(4) ; 
	run ; 
	filename cmds DDE 'excel|system' ;
	data _null_ ;
		file cmds ;
		put '[FILE-OPEN("'"&chemin.\&nom_graph..xls"'")]' ;
	run ;
	%let plage1=L1C1:L1C40;
	%let plage2=L2C1:L1000C40;
	filename t dde "EXCEL|&chemin.\[&nom_graph..xls]eff!&plage1" notab;
	data _null_;
		file t lrecl=500;
		set &nom_tab;
		sp="09"X;
		put "Fap3" sp  "s_repemp" sp
		"s_stage" sp "s_arretx" sp "s_radiat" sp "s_noactu"  sp "s_autrex" sp "s_des_t"
		;		
	run;
	 filename t dde "EXCEL|&chemin.\[&nom_graph..xls]eff!&plage2" notab;
	data _null_;
		file t lrecl=500;
		set &nom_tab;
		sp="09"X;
		put fap_3 $3. sp 
		s_repemp commax6.2 sp s_stage commax6.2 sp
		s_arretx commax6.2 sp s_radiat commax6.2 sp s_noactu commax6.2 sp
		s_autrex commax6.2 sp s_des_t commax6.2
		;		
	run;
	filename t dde "EXCEL|&chemin.\[&nom_graph..xls]part!&plage1" notab;
	data _null_;
		file t lrecl=500;
		set &nom_tab;
		sp="09"X;
		put "Fap3" sp  "part_s_repemp" sp
		"part_s_stage" sp "part_s_arretx" sp "part_s_radiat" sp "part_s_noactu"  sp "part_s_autrex" sp "Total"
		;		
	run;
	 filename t dde "EXCEL|&chemin.\[&nom_graph..xls]part!&plage2" notab;
	data _null_;
		file t lrecl=500;
		set &nom_tab;
		sp="09"X;
		put fap_3 $3. sp 
		part_s_repemp commax6.2 sp part_s_stage commax6.2 sp
		part_s_arretx commax6.2 sp part_s_radiat commax6.2 sp part_s_noactu commax6.2 sp
		part_s_autrex commax6.2 sp tot commax6.2
		;		
	run;
	options missing=".";
	/*sauvegarde du fichier Excel*/
	FILENAME cmds DDE 'Excel|system';
	DATA _NULL_;
		FILE cmds;
		PUT '[save()]';
	RUN;
	/*fermeture du fichier*/
	data _null_ ;
		file cmds ;
		put "[FILE-CLOSE()]" ;
	run ;
	/*fermer excel*/
	data _null_ ;
		file cmds ;
		put "[QUIT()]" ;
	run ;

%mend;
%prep_fichier_g18(&chemin,libout.res,&nom_rep)


