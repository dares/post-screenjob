%let deb=1997;
%let fin=2014;
%let nom=t9;
%let nom_rep=Tableau_9 DEFM A;
libname nostra "F:\Sources\Tensions\PSM 2015";

%let chemin=d:\documents-utilisateurs\charline.babet\Donnees\PSM\PSM 2015\Sorties\&nom_rep;
libname libout "&chemin";


%macro dossier(chemin_rep);
	options NOXWAIT NOXSYNC ;
		%let rc=%sysfunc(filename(fileref, &chemin_rep));
		 %if %sysfunc(fexist(&fileref))  %then %do ; 
		 	%end;
			%else %do;
			Data _null_;
				X mkdir "&chemin_rep"; /*Cration du repertoire  l'aide de la macro-variable*/
			run;
		%end;
%mend;
%dossier(&chemin)
libname libout "&chemin";

data donnees_nostra;
	set nostra.f5fmetro;
	attrib annee format=4.;
	annee=substr(datetrim,1,4);
	fap_3=substr(fap,1,3);
	defma=df123sar;
	defmabc=defm123+defm678;
run;

/*DEFM A*/
proc summary data=donnees_nostra noprint;
	var defma;
	class fap annee;
	output out=defma mean=eff_defma;
run;
data defma;
	set defma(where=(_type_ =3));
	fap_3=substr(fap,1,3);
run;
proc summary data=defma noprint;
	var eff_defma;
	class fap_3 annee;
	output out=defma_fap3 sum=eff_defma;
run;
data defma_fap3;
	set defma_fap3(where=(_type_ =3));
run;
data defma_all;
	set defma defma_fap3;
run;
proc sql;
	create table defma_all as select *, 100*eff_defma/max(eff_defma) as part from defma_all
	group by fap_3,annee;
quit;
data defma_all;
	set defma_all;
	annee2="defma_"||strip(annee);
	annee3="part_defma_"||strip(annee);
run;
proc sort data=defma_all;by fap_3 fap;run;
proc transpose data=defma_all out=defma2;
	id annee2;
	by fap_3 fap;
	var eff_defma;
run;
proc transpose data=defma_all out=defma3;
	id annee3;
	by fap_3 fap;
	var part;
run;
data defma_tot;
	merge defma2 defma3;
	by fap_3 fap;
run;


/*DEFM ABC*/
proc summary data=donnees_nostra noprint;
	var defmabc;
	class fap annee;
	output out=defmabc mean=eff_defmabc;
run;
data defmabc;
	set defmabc(where=(_type_ =3));
	fap_3=substr(fap,1,3);
run;
proc summary data=defmabc noprint;
	var eff_defmabc;
	class fap_3 annee;
	output out=defmabc_fap3 sum=eff_defmabc;
run;
data defmabc_fap3;
	set defmabc_fap3(where=(_type_ =3));
run;
data defmabc_all;
	set defmabc defmabc_fap3;
run;
proc sql;
	create table defmabc_all as select *, 100*eff_defmabc/max(eff_defmabc) as part from defmabc_all
	group by fap_3,annee;
quit;
data defmabc_all;
	set defmabc_all;
	annee2="defmabc_"||strip(annee);
	annee3="part_defmabc_"||strip(annee);
run;
proc sort data=defmabc_all;by fap_3 fap;run;
proc transpose data=defmabc_all out=defmabc2;
	id annee2;
	by fap_3 fap;
	var eff_defmabc;
run;
proc transpose data=defmabc_all out=defmabc3;
	id annee3;
	by fap_3 fap;
	var part;
run;
data defmabc_tot;
	merge defmabc2 defmabc3;
	by fap_3 fap;
run;


/*DEFM A et DEFM ABC*/
proc sort data=defma_tot;by fap_3 fap ;run;
proc sort data=defmabc_tot;by fap_3 fap ;run;
data defm;
	merge defma_tot defmabc_tot;
	by fap_3 fap ;
run;
data defm;
	set defm;
	drop _name_;
run;

proc sort data=defm;by fap_3 fap ;run;
data defm;
	set defm;
	by fap_3 fap ;
	retain ordre 1;
	ordre=ordre+1;
	if first.fap_3 then ordre=1;
run;
data libout.defm;
	set defm;
	code=strip(fap_3)||strip(ordre);
run;


/*On considre que les donnes ne sont pas trs fiables si il y a moins de 600 offres / an*/
proc summary data=donnees_nostra noprint nway;
	var oee;
	class fap_3 annee;
	output out=offres sum=oee;
run;
data pb_offres;
	set offres(where=(oee<600));
run;


%macro prep_fichier_t09_10_de(an_deb,an_fin,chemin,nom_tab,nom_graph, liste_var,liste_onglets);
	%let chemin_psm=d:\documents-utilisateurs\charline.babet\Donnees\PSM\PSM 2015\Modles;
	%let i=1;
	%do %while(%length(%scan(&liste_var,&i))>0);
		%let var&i=%scan(&liste_var,&i);
		%let i=%eval(&i+1);
	%end;
	%let nbvar=%eval(&i-1);
	%let i=1;
	%do %while(%length(%scan(&liste_onglets,&i))>0);
		%let onglet&i=%scan(&liste_onglets,&i);
		%let i=%eval(&i+1);
	%end;
	%let nbonglets=%eval(&i-1);

	options missing="";
	options NOXWAIT NOXSYNC ;
	x copy "&chemin_psm.\Modle &nom_graph..xls" "&chemin.\&nom_graph..xls";
	options NOXWAIT NOXSYNC ; *** Open Excel ***; 
	x "start excel"; 
	data _null_ ; 
		x = SLEEP(4) ; 
	run ; 
	filename cmds DDE 'excel|system' ;
	data _null_ ;
		file cmds ;
		put '[FILE-OPEN("'"&chemin.\&nom_graph..xls"'")]' ;
	run ;
	%let plage1=L1C1:L1C40;
	%let plage2=L2C1:L1000C40;
	%do i=1 %to &nbvar;
		filename t dde "EXCEL|&chemin.\[&nom_graph..xls]&&onglet&i!&plage1" notab;
		data _null_;
			file t lrecl=500;
			set &nom_tab;
			sp="09"X;
			put "Code" sp "Fap3" sp "Fap" sp
			"&an_deb"  %do an=%eval(&an_deb+1) %to &an_fin; sp "&an"  %end;
			;		
		run;
		 filename t dde "EXCEL|&chemin.\[&nom_graph..xls]&&onglet&i!&plage2" notab;
		data _null_;
			file t lrecl=500;
			set &nom_tab;
			sp="09"X;
			put code $4. sp fap_3 $3. sp fap $5. sp 
			&&var&i.._&an_deb commax7.2 %do an=%eval(&an_deb+1) %to &an_fin; sp &&var&i.._&an commax7.2 %end;
			;		
		run;
	%end;
	options missing=".";
	/*sauvegarde du fichier Excel*/
	FILENAME cmds DDE 'Excel|system';
	DATA _NULL_;
		FILE cmds;
		PUT '[save()]';
	RUN;
	/*fermeture du fichier*/
	data _null_ ;
		file cmds ;
		put "[FILE-CLOSE()]" ;
	run ;
	/*fermer excel*/
	data _null_ ;
		file cmds ;
		put "[QUIT()]" ;
	run ;

%mend;

%let nom_rep=Tableau_9 DEFM A;
%let chemin=d:\documents-utilisateurs\charline.babet\Donnees\PSM\PSM 2015\Sorties\&nom_rep;
%prep_fichier_t09_10_de(&deb,&fin,&chemin,libout.defm,&nom_rep, defma part_defma, defma part_defma);

%let nom_rep=Tableau_10 DEFM ABC;
%let chemin=d:\documents-utilisateurs\charline.babet\Donnees\PSM\PSM 2015\Sorties\&nom_rep;
%prep_fichier_t09_10_de(&deb,&fin,&chemin,libout.defm,&nom_rep, defmabc part_defmabc, defmabc part_defmabc);





