%let deb=1997;
%let fin=2014;
%let nom=g13;
%let nom_rep=Graphique_13 Demandeurs emploi;
libname nostra "F:\Sources\Tensions\PSM 2015";

%let chemin=d:\documents-utilisateurs\charline.babet\Donnees\PSM\PSM 2015\Sorties\&nom_rep;
libname libout "&chemin";

%macro dossier(chemin_rep);
	options NOXWAIT NOXSYNC ;
		%let rc=%sysfunc(filename(fileref, &chemin_rep));
		 %if %sysfunc(fexist(&fileref))  %then %do ; 
		 	%end;
			%else %do;
			Data _null_;
				X mkdir "&chemin_rep"; /*Cration du repertoire  l'aide de la macro-variable*/
			run;
		%end;
%mend;
%dossier(&chemin)

data donnees_nostra;
	set nostra.f5fmetro;
	attrib annee format=4.;
	annee=substr(datetrim,1,4);
	fap_3=substr(fap,1,3);
	defma=df123sar;
	defmabc=defm123+defm678;
run;
proc summary data=donnees_nostra noprint;
	var defma;
	class fap_3 annee;
	output out=defma sum=eff_defma;
run;

data defma;
	set defma(where=(_type_ not in(0,2)));
	if fap_3="" then fap_3="ENS";
	annee2="defma_"||strip(annee);
	eff_defma=eff_defma/4000;
run;
proc sort data=defma; by fap_3; run;
proc transpose data=defma out=defma2;
	id annee2;
	by fap_3;
	var eff_defma;
run;

proc summary data=donnees_nostra noprint;
	var defmabc;
	class fap_3 annee;
	output out=defmabc sum=eff_defmabc;
run;
data defmabc;
	set defmabc(where=(_type_ not in(0,2)));
	if fap_3="" then fap_3="ENS";
	annee2="defmabc_"||strip(annee);
	eff_defmabc=eff_defmabc/4000;
run;
proc sort data=defmabc;by fap_3;run;
proc transpose data=defmabc out=defmabc2;
	id annee2;
	by fap_3;
	var eff_defmabc;
run;

data defm;
	merge defma2 defmabc2;
	by fap_3;
run;
data defm;
	set defm;
	drop _name_;
	code=fap_3;
run;

data libout.defm;
set defm;
run;

/* On considre que les donnes ne sont pas trs fiables si il y a moins de 600 offres / an. */
proc summary data=donnees_nostra noprint nway;
	var oee;
	class fap_3 annee;
	output out=offres sum=oee;
run;
data pb_offres;
	set offres(where=(oee<600));
run;


%macro prep_fichier_de(an_deb,an_fin,chemin,nom_tab,nom_graph, liste_var,liste_onglets);
	%let chemin_psm=d:\documents-utilisateurs\charline.babet\Donnees\PSM\PSM 2015\Modles;
	%let i=1;
	%do %while(%length(%scan(&liste_var,&i))>0);
		%let var&i=%scan(&liste_var,&i);
		%let i=%eval(&i+1);
	%end;
	%let nbvar=%eval(&i-1);
	%let i=1;
	%do %while(%length(%scan(&liste_onglets,&i))>0);
		%let onglet&i=%scan(&liste_onglets,&i);
		%let i=%eval(&i+1);
	%end;
	%let nbonglets=%eval(&i-1);

	options missing="n.d.";
	options NOXWAIT NOXSYNC ;
	x copy "&chemin_psm.\Modle &nom_graph..xls" "&chemin.\&nom_graph..xls";
	options NOXWAIT NOXSYNC ; *** Open Excel ***; 
	x "start excel"; 
	data _null_ ; 
		x = SLEEP(4) ; 
	run ; 
	filename cmds DDE 'excel|system' ;
	data _null_ ;
		file cmds ;
		put '[FILE-OPEN("'"&chemin.\&nom_graph..xls"'")]' ;
	run ;
	%let plage1=L1C1:L1C40;
	%let plage2=L2C1:L1000C40;
	%do i=1 %to &nbvar;
		filename t dde "EXCEL|&chemin.\[&nom_graph..xls]&&onglet&i!&plage1" notab;
		data _null_;
			file t lrecl=500;
			set &nom_tab;
			sp="09"X;
			put "Code" sp "Fap3" sp 
			"&an_deb"  %do an=%eval(&an_deb+1) %to &an_fin; sp "&an"  %end;
			;		
		run;
		 filename t dde "EXCEL|&chemin.\[&nom_graph..xls]&&onglet&i!&plage2" notab;
		data _null_;
			file t lrecl=500;
			set &nom_tab;
			sp="09"X;
			put code $3. sp fap_3 $3. sp 
			&&var&i.._&an_deb commax8.2 %do an=%eval(&an_deb+1) %to &an_fin; sp &&var&i.._&an commax8.2 %end;
			;		
		run;
	%end;
	options missing=".";
	/*sauvegarde du fichier Excel*/
	FILENAME cmds DDE 'Excel|system';
	DATA _NULL_;
		FILE cmds;
		PUT '[save()]';
	RUN;
	/*fermeture du fichier*/
	data _null_ ;
		file cmds ;
		put "[FILE-CLOSE()]" ;
	run ;
	/*fermer excel*/
	data _null_ ;
		file cmds ;
		put "[QUIT()]" ;
	run ;

%mend;
%prep_fichier_de(&deb,&fin,&chemin,defm,&nom_rep, defma defmabc, defma defmabc);


