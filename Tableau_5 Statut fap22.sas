%include "C:\Users\guillaume.toure\Desktop\DAMETH_TOURE\PSM\preparation_donnees\PSM 2021\Programmes\macros portraits fap22.sas";

%let deb=1982;
%let fin=2020;
%let nom=t5;
%let nom_rep=Tableau_5 Statut fap22;

libname eec "C:\Users\guillaume.toure\Desktop\DAMETH_TOURE\PSM\preparation_donnees\Series longues 2021\Bases SAS\s�ries_longues";

%let chemin=C:\Users\guillaume.toure\Desktop\DAMETH_TOURE\PSM\preparation_donnees\PSM 2021\Sorties\&nom_rep;
libname libout "&chemin";

%macro prep_statut(lib);

%do an=&deb %to &fin;
	data sl_dmq_&an;
		set &lib..sl_dmq_&an. ;
		%if &an<1990 %then %do;
			keep  idt_aire im_loc noi fap_3 poids_final 
			fap_1 stat_sl statut cstot;
		%end;
		%if &an>=1990 and &an<2003 %then %do;
			keep  aire imloc noi fap_3 poids_final fap_1 stat_sl statut cstot ;
		%end;
		%if &an>=2003 %then %do;
			keep  ident noi fap_3 poids_final fap_1 stat_sl statut cstot;
		%end;
		%if &an.<1990 %then %do;
			select;
			when (statut in ('11','12','13')) 	stat_sl='ns ';
			when (statut in ('21')) 			stat_sl='int';
			when (statut in ('22'))				stat_sl='app';
			when (statut in ('23','24','25','26','27','28','33','34','35')) 	stat_sl='cdd';
			when (statut in ('31','36','37') and (tit in ('3','4','5','6') or det in ('1','2'))) 	stat_sl='cdd';
			when (statut in ('29','32'))										stat_sl='cdi';
			when (statut in ('31','36','37') /*and tit in ('1','2')*/) 				stat_sl='cdi';
			otherwise stat_sl='zzz';
			end;
		%end;
		%else %if &an.<2003 %then %do;
			select;
			when (statut in ('11','12','13')) 	stat_sl='ns ';
			when (statut in ('21')) 			stat_sl='int';
			when (statut in ('22'))				stat_sl='app';
			when (statut in ('23','30')) stat_sl='cdd';
			when (statut in ('41','42') and (tit in ('4','5') or det in ('1','2'))) stat_sl='cdd';
			when (statut in ('41','42') /*and tit in ('1','2','3')*/) 	stat_sl='cdi';
			when (statut in ('24') ) 								stat_sl='cdi';
			otherwise stat_sl='zzz';
			end;
		 %end;
		%else %do;
			select;
			when (statut in ('11','12','13')) 		stat_sl='ns ';
			when (statut in ('21')) 				stat_sl='int';
			when (statut in ('22'))					stat_sl='app';
			when (statut in ('33','43','34','44'))  stat_sl='cdd';
			when (statut in ('35','45') ) 			stat_sl='cdi';
			otherwise stat_sl='zzz';
			end;
		%end;

		if cstot="83" then stat_sl='cdd'; /* cstot =83 : militaires du contingent */
		fap_1=substr(fap_3,1,1);
		
	run;
%end;
%mend;

%prep_statut(eec);

/*Dico des codes EEC 2017 :
STATUT
Statut dtaill mis en cohrence avec la profession
Vide Sans objet (personnes non actives occupes)
11 Indpendants
12 Employeurs
13 Aides familiaux
21 Intrimaires
22 Apprentis
33 CDD (hors Etat, coll.loc.), hors contrats aides
34 Stagiaires et contrats aides (hors Etat, coll.loc.)
35 Autres contrats (hors Etat, coll.loc.)
43 CDD (Etat, coll.loc.), hors contrats aides
44 Stagiaires et contrats aides (Etat, coll.loc.)
45 Autres contrats (Etat, coll.loc.)
99 Non renseign
*/


/* 	ods html file="d:\documents-utilisateurs\charline.babet\Donnees\PSM\PSM 2015\Sorties\Tableau_5 Statut\militaires.doc";
	proc freq data=sl_dmq_1982 (where=(fap_3='P4Z')); tables cstot*stat_sl; weight poids_final; run; 
	proc freq data=sl_dmq_1983 (where=(fap_3='P4Z')); tables cstot*stat_sl; weight poids_final; run; 
	proc freq data=sl_dmq_2013 (where=(fap_3='P4Z')); tables cstot*stat_sl; weight poids_final; run; 
	proc freq data=sl_dmq_2014 (where=(fap_3='P4Z')); tables cstot*stat_sl; weight poids_final; run; 
	ods html close; */

%calculs_fap22(chemin_res=&chemin,nom=&nom,an_deb=&deb,an_fin=&fin,nom_table_prep=sl_dmq,
	lib_donnees=work,lib_sortie=libout,var=stat_sl,var_ventil=,var_pond=poids_final,
	seuil_diffusion=100);

%macro salaries();
	data sal;
		set libout.res&nom._&deb._&fin.(where=(stat_sl ne "ns"));
	run;
	proc summary data= sal missing nway noprint;
		class stat_sl ;
		var %do an=%eval(&deb+1) %to %eval(&fin-1); eff%eval(&an-1)_%eval(&an+1) %end;;
		output out=res_sal(drop=_FREQ_ _TYPE_) sum=%do an=%eval(&deb+1) %to %eval(&fin-1); eff%eval(&an-1)_%eval(&an+1) %end;;
	run;
	proc sql;
		create table res_sal as select *, 
		100*eff&deb._%eval(&deb+2)/sum(eff&deb._%eval(&deb+2)) as part&deb._%eval(&deb+2)
		%do an=%eval(&deb+2) %to %eval(&fin-1);
			, 100*eff%eval(&an-1)_%eval(&an+1)/sum(eff%eval(&an-1)_%eval(&an+1)) as part%eval(&an-1)_%eval(&an+1)
		%end; from res_sal ;
	quit; 
%mend;
%salaries;
data res_sal;
	set res_sal;
	fap_3="SAL";
	code=strip(fap_3)||strip(stat_sl);
run;
data libout.ressal&nom._&deb._&fin.;
	set res_sal;
run;

data res&nom._&deb._&fin.;
	set libout.res&nom._&deb._&fin. libout.ressal&nom._&deb._&fin.;
run;
data resdiff&nom._&deb._&fin.;
	set libout.resdiff&nom._&deb._&fin. libout.ressal&nom._&deb._&fin.;
run;

%prep_fichier_fap22(an_deb=&deb,an_fin=&fin,chemin=&chemin,nom_tab=res&nom._&deb._&fin.,	
	nom_tab_diff=resdiff&nom._&deb._&fin.,nom_graph=&nom_rep,var=stat_sl,var_ventil=);

/* data liste_fap;
	set eec.sl_dmq_2014;
	keep fap_3;
run;
proc sql;
	insert into liste_fap(fap_3) values("T2Z");
quit;
proc sort data=liste_fap nodupkey; by fap_3; run;
data liste_stat;
	set sl_dmq_2014;
	keep stat_sl;
run;
proc sort data=liste_stat nodupkey; by stat_sl; run;

proc sql;
	create table liste_fap_stat as select * from liste_fap,liste_stat;
quit;
proc sort data=liste_fap_stat; by fap_3 stat_sl; run;
data libout.resall&nom._&deb._&fin;
	merge libout.res&nom._&deb._&fin liste_fap_stat;
	by fap_3 stat_sl;
run; */


