%include "C:\Users\guillaume.toure\Desktop\DAMETH_TOURE\PSM\preparation_donnees\Formats\Formats FAP.sas";/**/
%include "C:\Users\guillaume.toure\Desktop\DAMETH_TOURE\PSM\preparation_donnees\PSM 2021\Programmes\macros portraits.sas";/**/

%let deb=2003;
%let fin=2020;/*2014*/
%let nom=t1;
%let nom_rep=Tableau_1 Emploi;

libname eec "C:\Users\guillaume.toure\Desktop\DAMETH_TOURE\PSM\preparation_donnees\Series longues 2021\Bases SAS\s�ries_longues";/**/

%let chemin=C:\Users\guillaume.toure\Desktop\DAMETH_TOURE\PSM\preparation_donnees\PSM 2021\Sorties\&nom_rep;
libname libout "&chemin";


%macro prep_emp(lib);

%do an=&deb %to &fin;
	data sl_dmq_&an;
		set &lib..sl_dmq_&an. ;
		FAP=put(P,$FAP9PCx.);
		if FAP='' then FAP="ZZZZZ";
		keep noi ident /*idt_aire imloc aire*/ fap_3 fap poids_final;
	run;
%end;
%mend;

%prep_emp(eec);

%calculs(chemin_res=&chemin,nom=&nom,an_deb=&deb,an_fin=&fin,nom_table_prep=sl_dmq,
	lib_donnees=work,lib_sortie=libout,var=,var_ventil=fap,var_pond=poids_final,
	seuil_diffusion=100);

%prep_fichier(an_deb=&deb,an_fin=&fin,chemin=&chemin,nom_tab=libout.res&nom._&deb._&fin,
	nom_tab_diff=libout.res&nom._&deb._&fin,nom_graph=&nom_rep,var_ventil=fap,var=);
