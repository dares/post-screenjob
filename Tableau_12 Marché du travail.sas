%include "d:\documents-utilisateurs\charline.babet\Donnees\PSM\PSM 2015\Programmes\macros portraits.sas";
libname eec "d:\documents-utilisateurs\charline.babet\Donnees\Series longues\Series longues 2015\Bases SAS";

%let deb=2010;
%let fin=2014;
%let nom=t12;
%let nom_rep=Tableau_12 March du travail;

libname nostra "F:\Sources\Tensions\PSM 2015";
/* Chemin o sont stockes les rsultats trimestriels. On peut les constituer  l'aide
du programme PGM_FAP2012T1_DA.sas sous Donnes/XFAP2009/Programme(sas_creation) (dans ce cas
il faut au pralable rcuprer les donnes sous W ou auprs du dpartement March du Travail),
ou demander  Yannick Croguennec. */
%let annee=14;

%let chemin=d:\documents-utilisateurs\charline.babet\Donnees\PSM\PSM 2015\Sorties\&nom_rep;
libname libout "&chemin";

%macro dossier(chemin_rep);
	options NOXWAIT NOXSYNC ;
		%let rc=%sysfunc(filename(fileref, &chemin_rep));
		 %if %sysfunc(fexist(&fileref))  %then %do ; 
		 	%end;
			%else %do;
			Data _null_;
				X mkdir "&chemin_rep"; /*Cration du repertoire  l'aide de la macro-variable*/
			run;
		%end;
%mend;
%dossier(&chemin)
libname libout "&chemin";

data donnees_nostra;
	set nostra.Deoe_f9nt&annee.t1 nostra.Deoe_f9nt&annee.t2 nostra.Deoe_f9nt&annee.t3 nostra.Deoe_f9nt&annee.t4 ;
	attrib fap_3 format=$3.;
	fap_3=substr(fap,1,3);
	if reg<"10" or reg>"95" then delete; /*on ne garde que la France mtropolitaine*/
run;
proc sort data=donnees_nostra;by fap_3;run; 
proc summary data=donnees_nostra noprint;
	var f_defm_a f_femm_a oe_typa1 oe_typa2 oe_typa3 oe_typb1 oe_typb2 oe_typb3 oe_typc1  oe_typc2 oe_oee; 
	class fap_3;
	output out=res(drop=_type_ _freq_) sum=f_defm_a f_femm_a oe_typa1 oe_typa2 oe_typa3 oe_typb1 oe_typb2 oe_typb3 oe_typc1  oe_typc2  oe_oee;
run;
data res;
	set res;
	if fap_3 ="" then fap_3="ENS";
run;

/* donnes coulement des demandes */
proc summary data=nostra.f3fm_elu nway;
where datetrim="201412";
class fap;
var ec_df1a8;
output out=ecoul sum=ec_df1a8;
run;

proc summary data=nostra.f0fm_elu nway;
where datetrim="201412";
class fap;
var ec_df1a8;
output out=ecoul_tot sum=ec_df1a8;
run;

data ecoul_tot;
	set ecoul_tot;
	fap="ENS";
run;

data ecoulement;
	set ecoul ecoul_tot;
	rename fap=fap_3;
	drop _type_ _freq_;
run;

/*Emploi moyen sur les 3 dernires annes (enqute emploi)*/
options mprint mlogic symbolgen;
%calculs(chemin_res=&chemin,nom=&nom,an_deb=&deb,an_fin=&fin,
	nom_table_prep=sl_dmq,lib_donnees=eec,lib_sortie=libout,var=sexe_cal,var_ventil=,
	var_pond=poids_final, seuil_diffusion=100);

data donnees_eec;
	set libout.rest12_&deb._&fin;
	keep fap_3 sexe_cal lib eff%eval(&fin-2)_&fin part%eval(&fin-2)_&fin.;
	if sexe_cal="1" then lib="Eff Homme";
	if sexe_cal="2" then lib="Eff Femme";
run;
proc transpose data=donnees_eec out=donnees_eec2;
	var eff%eval(&fin-2)_&fin;
	id lib;
	by fap_3;
run;
data donnees_eec2;
	set donnees_eec2;
	drop _name_;
	Eff=Eff_femme+Eff_homme;
run;

proc sort data=res;by fap_3;run;
proc sort data=ecoulement;by fap_3;run;
data donnees_all;
	merge donnees_eec2 res ecoulement;
	by fap_3;
run;

data libout.res;
	set donnees_all;
	taux_demande=100*(f_defm_a/4)/(f_defm_a/4+1000*eff);
	taux_demande_femme=100*(f_femm_a/4)/(f_femm_a/4+1000*eff_femme);
	taux_demande_homme=100*((f_defm_a-f_femm_a)/4)/((f_defm_a-f_femm_a)/4+1000*eff_homme);
	part_contrats_plus6mois=100*(oe_typa1+oe_typa2+oe_typa3)/(oe_typa1+oe_typa2+oe_typa3+oe_typb1+oe_typb2);
run;

%macro prep_fichier_t12(chemin,nom_tab,nom_graph);
	%let chemin_psm=d:\documents-utilisateurs\charline.babet\Donnees\PSM\PSM 2015\Modles;
	options missing="";
	options NOXWAIT NOXSYNC ;
	x copy "&chemin_psm.\Modle &nom_graph..xls" "&chemin.\&nom_graph..xls";
	options NOXWAIT NOXSYNC ; *** Open Excel ***; 
	x "start excel"; 
	data _null_ ; 
		x = SLEEP(4) ; 
	run ; 
	filename cmds DDE 'excel|system' ;
	data _null_ ;
		file cmds ;
		put '[FILE-OPEN("'"&chemin.\&nom_graph..xls"'")]' ;
	run ;
	%let plage1=L1C1:L1C40;
	%let plage2=L2C1:L1000C40;
	filename t dde "EXCEL|&chemin.\[&nom_graph..xls]eff!&plage1" notab;
	data _null_;
		file t lrecl=500;
		set &nom_tab;
		sp="09"X;
		put "Fap3" sp "F_DEFM_A" sp "f_femm_a" sp
		"oe_typa1" sp "oe_typa2" sp "oe_typa3" sp "oe_typb1"  sp "oe_typb2"  sp "oe_typb3" 
		 sp "oe_typc1"  sp "oe_typc2" sp "Eff_femme" sp "Eff_homme" sp "Eff"
		;		
	run;
	 filename t dde "EXCEL|&chemin.\[&nom_graph..xls]eff!&plage2" notab;
	data _null_;
		file t lrecl=500;
		set &nom_tab;
		sp="09"X;
		put fap_3 $3. sp 
		F_DEFM_A commax6.2 sp f_femm_a commax6.2 sp
		oe_typa1 commax6.2 sp oe_typa2 commax6.2 sp oe_typa3 commax6.2 sp
		oe_typb1 commax6.2 sp oe_typb2 commax6.2 sp oe_typb3 commax6.2 sp
		oe_typc1 commax6.2 sp oe_typc2 commax6.2 sp
		Eff_femme commax6.2 sp Eff_homme commax6.2 sp Eff commax6.2 sp

		;		
	run;
	filename t dde "EXCEL|&chemin.\[&nom_graph..xls]part!&plage1" notab;
	data _null_;
		file t lrecl=500;
		set &nom_tab;
		sp="09"X;
		put "Fap3" sp "taux_demande" sp
		"taux_demande_femme" sp "taux_demande_homme" sp "ec_df1a8" sp "part_contrats_plus6mois"
		;		
	run;
	 filename t dde "EXCEL|&chemin.\[&nom_graph..xls]part!&plage2" notab;
	data _null_;
		file t lrecl=500;
		set &nom_tab;
		sp="09"X;
		put fap_3 $3. sp 
		taux_demande commax6.2 sp taux_demande_femme commax6.2 sp
		taux_demande_homme commax6.2 sp ec_df1a8 commax6.2 sp part_contrats_plus6mois commax6.2 
		;		
	run;
	options missing=".";
	/*sauvegarde du fichier Excel*/
	FILENAME cmds DDE 'Excel|system';
	DATA _NULL_;
		FILE cmds;
		PUT '[save()]';
	RUN;
	/*fermeture du fichier*/
	data _null_ ;
		file cmds ;
		put "[FILE-CLOSE()]" ;
	run ;
	/*fermer excel*/
	data _null_ ;
		file cmds ;
		put "[QUIT()]" ;
	run ;

%mend;
%prep_fichier_t12(&chemin,libout.res,&nom_rep)
