%let nom_rep_psm=PSM 2015;

%let chemin=d:\documents-utilisateurs\charline.babet\Donnees\PSM\&nom_rep_psm\Sorties\Tableaux de synthse;
%let chemin_psm=d:\documents-utilisateurs\charline.babet\Donnees\PSM\&nom_rep_psm\Modles;
%let chemin1=d:\documents-utilisateurs\charline.babet\Donnees\PSM\&nom_rep_psm\Sorties;
%let an_fin=2014;
%let anneeMMO=2014;

%include "d:\documents-utilisateurs\charline.babet\Donnees\PSM\PSM 2015\Programmes\macros portraits.sas";

	options NOXWAIT NOXSYNC ;
	x copy "&chemin_psm.\Modle A - Dynamique de l'emploi.xls" "&chemin.\A - Dynamique de l'emploi.xls";
	x copy "&chemin_psm.\Modle B - Caractristiques des personnes en emploi.xls" "&chemin.\B - Caractristiques des personnes en emploi.xls";
	x copy "&chemin_psm.\Modle C - Qualit de l'emploi.xls" "&chemin.\C - Qualit de l'emploi.xls";
	x copy "&chemin_psm.\Modle D - Caractristiques des employeurs.xls" "&chemin.\D - Caractristiques des employeurs.xls";
	x copy "&chemin_psm.\Modle E - Mobilit.xls" "&chemin.\E - Mobilit.xls";
	x copy "&chemin_psm.\Modle F - March du travail.xls" "&chemin.\F - March du travail.xls";

*options mprint mlogic symbolgen;

%macro res_synth(nom,nom_tab, var, liste_mod,type_tab);
	%let i=1;
	%do %while(%length(%scan(&liste_mod,&i))>0);
		%let mod&i=%scan(&liste_mod,&i);
		%let i=%eval(&i+1);
	%end;
	%let nbmod=%eval(&i-1);
	 data res&nom;
		set &nom_tab(where=(fap_3 ne "ZZZ"));
		keep fap_3 &var part1982_1984 part%eval(&an_fin-2)_&an_fin;
		if part%eval(&an_fin-2)_&an_fin=. and nb_%eval(&an_fin-2)_&an_fin>=100 then part%eval(&an_fin-2)_&an_fin=0;
		if part1982_1984=. and nb_1982_1984>=100 then part1982_1984=0;
		/*if part1982_1984=. then part1982_1984=0;
		if part%eval(&an_fin-2)_&an_fin=. then part%eval(&an_fin-2)_&an_fin=0;*/
		%if "&var"="stat_sl" %then %do;
			if part1982_1984=. then part1982_1984=0;
			if part%eval(&an_fin-2)_&an_fin=. then part%eval(&an_fin-2)_&an_fin=0;
		%end;			
		if fap_3 in("X0Z","T6Z") then do; 
			part%eval(&an_fin-2)_&an_fin=.;
			part1982_1984=.;
		end;
		if fap_3="T2Z" then part%eval(&an_fin-2)_&an_fin=.;
		if fap_3 in("T2A","T2B") then part1982_1984=.;
	run;
	proc sort data=res&nom;by fap_3 &var;run;
	proc transpose data=res&nom out=res&nom.a;
		id &var;
		by fap_3;
		var part%eval(&an_fin-2)_&an_fin;
	run;
	data res&nom.a;	
		set res&nom.a;
		total_%eval(&an_fin-2)_&an_fin= &mod1 %do i=2 %to &nbmod; +&&mod&i %end;;
		rename %do i=1 %to &nbmod; &&mod&i=&&mod&i.._%eval(&an_fin-2)_&an_fin %end;;
	run;
	%if &type_tab=debut_fin %then %do;
		proc transpose data=res&nom out=res&nom.b;
			id &var;
			by fap_3;
			var part1982_1984;
		run;
		data res&nom.b;	
			set res&nom.b;
			total_1982_1984= &mod1 %do i=2 %to &nbmod; +&&mod&i %end;;
			rename %do i=1 %to &nbmod; &&mod&i=&&mod&i.._1982_1984 %end;;
		run;
		data res&nom._all;
			merge res&nom.b res&nom.a;
			by fap_3;
		run;
	%end;
	%else %do;
		data res&nom._all;
			set res&nom.a;	
		run;
	%end;

	data res&nom._all;
		set res&nom._all;
		if fap_3="ENS" then fap_3="ZENS";
	run;
	proc sort data=res&nom._all;by fap_3;run;
%mend;


/*****************************************/
/* Tableau A - Dynamique de l'emploi.xls */
/*****************************************/

%let nom=Graphique_1 Emploi;
libname lib "&chemin1\&nom";

%macro graph1;
data resg1;
	set lib.Resg1_1982_2014(where=(fap_3 not in("ENS","ZZZ")));
	%do an=1982 %to %eval(&an_fin-2); eff&an._%eval(&an+2)=1000*round(eff&an._%eval(&an+2)); %end;
	if fap_3 in("T2A","T2B") then do;
		eff1992_1994=.;
		eff1993_1995=.;
	end;
	if fap_3="T2Z" then do;
		%do an=1994 %to %eval(&an_fin-2); eff&an._%eval(&an+2)=.; %end;
	end;
	if fap_3 in("T6Z","X0Z") then do;
		%do an=1982 %to %eval(&an_fin-2); eff&an._%eval(&an+2)=.; %end;
	end;
	%global liste_var_g1;
	%let liste_var_g1=;
	%do an=1982 %to %eval(&an_fin-2); %let liste_var_g1=&liste_var_g1 eff&an._%eval(&an+2); %end;
run;
%mend;
%graph1;

	options NOXWAIT NOXSYNC ; *** Open Excel ***; 
	x "start excel"; 
	data _null_ ; 
		x = SLEEP(4) ; 
	run ; 
	filename cmds DDE 'excel|system' ;
	data _null_ ;
		file cmds ;
		put '[FILE-OPEN("'"&chemin.\%bquote(A - Dynamique de l%str(')emploi).xls"'")]' ;
	run ;

%export_synthese(resg1,%bquote(A - Dynamique de l%str(')emploi),%bquote(Dynamique de l%str(')emploi),
%bquote(volution de l'emploi par famille professionnelle de 1982-1984  %eval(&an_fin-2)-&an_fin),
L9C3:L96C40,&liste_var_g1,7.0)

	/*sauvegarde du fichier Excel*/
	FILENAME cmds DDE 'Excel|system';
	DATA _NULL_;
		FILE cmds;
		PUT '[save()]';
	RUN;
	/*fermeture du fichier*/
	data _null_ ;
		file cmds ;
		put "[FILE-CLOSE()]" ;
	run ;




/************************************************************/
/* Tableau B - Caractristiques des personnes en emploi.xls */
/************************************************************/

%let nom=Graphique_2 Age dtaill;
libname lib "&chemin1\&nom";

%res_synth(g2,lib.resg2_1982_2014,rep_age_enq,_15 _25 _30 _35 _40 _45 _50 _55 _60,fin)

data resg2_all;
	set resg2_all;if fap_3="T2Z" then delete;
run;

	filename cmds DDE 'excel|system' ;
	data _null_ ;
		file cmds ;
		put '[FILE-OPEN("'"&chemin.\%bquote(B - Caractristiques des personnes en emploi).xls"'")]' ;
	run ;

%export_synthese(resg2_all,%bquote(B - Caractristiques des personnes en emploi),
Structure par ge,%bquote(Structure par ge des personnes en emploi en %eval(&an_fin-2)-&an_fin),
L9C3:L96C12,_15_%eval(&an_fin-2)_&an_fin _25_%eval(&an_fin-2)_&an_fin
_30_%eval(&an_fin-2)_&an_fin _35_%eval(&an_fin-2)_&an_fin _40_%eval(&an_fin-2)_&an_fin
_45_%eval(&an_fin-2)_&an_fin _50_%eval(&an_fin-2)_&an_fin _55_%eval(&an_fin-2)_&an_fin 
_60_%eval(&an_fin-2)_&an_fin Total_%eval(&an_fin-2)_&an_fin,commax3.2)

data resg3;
	set lib.Resg2_1982_2014(where=(fap_3 ne "ZZZ"));
	keep fap_3 rep_age_enq part1982_1984 part%eval(&an_fin-2)_&an_fin;
	if fap_3 in("X0Z","T6Z") then do; 
		part%eval(&an_fin-2)_&an_fin=.;
		part1982_1984=.;
	end;
	if fap_3="T2Z" then part%eval(&an_fin-2)_&an_fin=.;
	if fap_3 in("T2A","T2B") then part1982_1984=.;
run;
proc sort data=resg3;by fap_3 rep_age_enq;run;
proc transpose data=resg3 out=resg3a;
	id rep_age_enq;
	by fap_3;
	var part%eval(&an_fin-2)_&an_fin;
run;
data resg3a;	
	set resg3a;
	moins30_%eval(&an_fin-2)_&an_fin=round(_15)+round(_25);
	_30_50_%eval(&an_fin-2)_&an_fin=round(_30)+round(_35)+round(_40)+round(_45);
	plus50_%eval(&an_fin-2)_&an_fin=round(_50)+round(_55)+round(_60);
	total_%eval(&an_fin-2)_&an_fin=_15+_25+_30+_35+_40+_45+_50+_55+_60;
	keep fap_3 moins30_%eval(&an_fin-2)_&an_fin _30_50_%eval(&an_fin-2)_&an_fin plus50_%eval(&an_fin-2)_&an_fin
	total_%eval(&an_fin-2)_&an_fin;
run;
proc transpose data=resg3 out=resg3b;
	id rep_age_enq;
	by fap_3;
	var part1982_1984;
run;
data resg3b;	
	set resg3b;
	moins30_1982_1984=round(_15)+round(_25);
	_30_50_1982_1984=round(_30)+round(_35)+round(_40)+round(_45);
	plus50_1982_1984=round(_50)+round(_55)+round(_60);
	total_1982_1984=_15+_25+_30+_35+_40+_45+_50+_55+_60;
	keep fap_3 moins30_1982_1984 _30_50_1982_1984 plus50_1982_1984
	total_1982_1984;
run;

data resg3_all;
	merge resg3b resg3a;
	by fap_3;
run;
data resg3_all;
	set resg3_all;
	if fap_3="ENS" then fap_3="ZENS";
run;
proc sort data=resg3_all;by fap_3;run;

%export_synthese(resg3_all,%bquote(B - Caractristiques des personnes en emploi),
Evolution structure par ge,
%bquote(volution de la structure par ge des personnes en emploi entre 1982-1984 et %eval(&an_fin-2)-&an_fin),
L10C3:L98C10,moins30_1982_1984 _30_50_1982_1984 plus50_1982_1984 total_1982_1984 
moins30_%eval(&an_fin-2)_&an_fin _30_50_%eval(&an_fin-2)_&an_fin plus50_%eval(&an_fin-2)_&an_fin
total_%eval(&an_fin-2)_&an_fin,commax3.2)

%let nom=Tableau_3 Sexe Age;
libname lib "&chemin1\&nom";

data rest3;
	set lib.Resdifft3_1982_2014(where=(fap_3 ne "ZZZ" and sexe_cal="2"));
	keep fap_3 rep_age_enq_3 part1982_1984 part%eval(&an_fin-2)_&an_fin;
	if fap_3 in("X0Z","T6Z") then do; 
		part%eval(&an_fin-2)_&an_fin=.;
		part1982_1984=.;
	end;
	if fap_3="T2Z" then part%eval(&an_fin-2)_&an_fin=.;
	if fap_3 in("T2A","T2B") then part1982_1984=.;
run;

proc sql;
	create table rest3_ens as select *, 
	sum(eff1982_1984) as sumeff1982_1984,
	sum(eff%eval(&an_fin-2)_&an_fin) as sumeff%eval(&an_fin-2)_&an_fin
	from lib.Resdifft3_1982_2014 group by fap_3,sexe_cal;
quit;
proc sql;
	create table rest3_ens2 as select *, 
	100*sumeff1982_1984/sum(eff1982_1984) as _all1982_1984,
	100*sumeff%eval(&an_fin-2)_&an_fin/sum(eff%eval(&an_fin-2)_&an_fin) as _all%eval(&an_fin-2)_&an_fin
	from rest3_ens group by fap_3;
quit;
data rest3_ens2;
	set rest3_ens2(where=(sexe_cal="2"));
	keep fap_3 _all1982_1984 _all%eval(&an_fin-2)_&an_fin;
	if fap_3 in("X0Z","T6Z") then do; 
		_all1982_1984=.;
		_all%eval(&an_fin-2)_&an_fin=.;
	end;
	if fap_3="T2Z" then _all%eval(&an_fin-2)_&an_fin=.;
	if fap_3 in("T2A","T2B") then _all1982_1984=.;

run;
proc sort data=rest3_ens2 nodupkey;by fap_3;run;

proc sort data=rest3;by fap_3 rep_age_enq_3;run;
proc transpose data=rest3 out=rest3a;
	id rep_age_enq_3;
	by fap_3;
	var part%eval(&an_fin-2)_&an_fin;
run;
data rest3a;	
	set rest3a;
	rename _3n=_3n%eval(&an_fin-2)_&an_fin _5n=_5n%eval(&an_fin-2)_&an_fin 
	_5p=_5p%eval(&an_fin-2)_&an_fin;
run;

proc transpose data=rest3 out=rest3b;
	id rep_age_enq_3;
	by fap_3;
	var part1982_1984;
run;
data rest3b;	
	set rest3b;
	rename _3n=_3n1982_1984 _5n=_5n1982_1984
	_5p=_5p1982_1984;
run;

data rest3_all;
	merge rest3b rest3a rest3_ens2;
	by fap_3;
run;
data rest3_all;
	set rest3_all;
	if fap_3="ENS" then fap_3="ZENS";
run;
proc sort data=rest3_all;by fap_3;run;

%export_synthese(rest3_all,%bquote(B - Caractristiques des personnes en emploi),Part femmes,
%bquote(volution de la part des femmes parmi les personnes en emploi entre 1982-1984 et %eval(&an_fin-2)-&an_fin),
L10C3:L98C10,_3n1982_1984 _5n1982_1984 _5p1982_1984 _all1982_1984
 _3n%eval(&an_fin-2)_&an_fin  _5n%eval(&an_fin-2)_&an_fin _5p%eval(&an_fin-2)_&an_fin _all%eval(&an_fin-2)_&an_fin,commax3.2)
 
%let nom=Graphique_4 Diplme;
libname lib "&chemin1\&nom";

%res_synth(g4,lib.resg4_1982_2014,rep_dip,_1 _3 _4 _5 _7 EC,debut_fin)

%export_synthese(resg4_all,%bquote(B - Caractristiques des personnes en emploi),Niveau diplme,
%bquote(volution de la structure par niveau de diplme des personnes en emploi entre 1982-1984 et %eval(&an_fin-2)-&an_fin),
L10C3:L98C16,_1_1982_1984 _3_1982_1984 _4_1982_1984 _5_1982_1984 _7_1982_1984 EC_1982_1984 
 total_1982_1984
_1_%eval(&an_fin-2)_&an_fin _3_%eval(&an_fin-2)_&an_fin _4_%eval(&an_fin-2)_&an_fin 
_5_%eval(&an_fin-2)_&an_fin  _7_%eval(&an_fin-2)_&an_fin  EC_%eval(&an_fin-2)_&an_fin  
total_%eval(&an_fin-2)_&an_fin,commax3.2)

%let nom=Graphique_5 Diplme -30;
libname lib "&chemin1\&nom";

%res_synth(g5,lib.resdiffg5_1982_2014,rep_dip,_1 _3 _4 _5 _7 EC,fin)

data resg5_all;
	set resg5_all(where=(fap_3 ne "T2Z"));
run;

%export_synthese(resg5_all,%bquote(B - Caractristiques des personnes en emploi),
Niveau diplme -30 ans,
%bquote(Structure par niveau de diplme des personnes en emploi de moins de 30 ans en %eval(&an_fin-2)-&an_fin),
L9C3:L96C9,_1_%eval(&an_fin-2)_&an_fin _3_%eval(&an_fin-2)_&an_fin _4_%eval(&an_fin-2)_&an_fin 
_5_%eval(&an_fin-2)_&an_fin  _7_%eval(&an_fin-2)_&an_fin  EC_%eval(&an_fin-2)_&an_fin  
total_%eval(&an_fin-2)_&an_fin,commax3.2)

	/*sauvegarde du fichier Excel*/
	FILENAME cmds DDE 'Excel|system';
	DATA _NULL_;
		FILE cmds;
		PUT '[save()]';
	RUN;
	/*fermeture du fichier*/
	data _null_ ;
		file cmds ;
		put "[FILE-CLOSE()]" ;
	run ;




/***************************************/
/* Tableau C - Qualit de l'emploi.xls */
/***************************************/

%let nom=Tableau_5 Statut;
libname lib "&chemin1\&nom";

%res_synth(t5,lib.resallt5_1982_2014,stat_sl,app cdd cdi int ns,debut_fin)

	filename cmds DDE 'excel|system' ;
	data _null_ ;
		file cmds ;
		put '[FILE-OPEN("'"&chemin.\%bquote(C - Qualit de l'emploi).xls"'")]' ;
	run ;


%export_synthese(rest5_all,%bquote(C - Qualit de l'emploi),Statut,
%bquote(volution du statut des personnes en emploi entre 1982-1984 et %eval(&an_fin-2)-&an_fin),
L10C3:L98C15,app_1982_1984 cdd_1982_1984 cdi_1982_1984 int_1982_1984 ns_1982_1984 
 total_1982_1984
app_%eval(&an_fin-2)_&an_fin cdd_%eval(&an_fin-2)_&an_fin cdi_%eval(&an_fin-2)_&an_fin 
int_%eval(&an_fin-2)_&an_fin  ns_%eval(&an_fin-2)_&an_fin  
total_%eval(&an_fin-2)_&an_fin,commax3.2)

%res_synth(t5,lib.ressalt5_1982_2014,stat_sl,app cdd cdi int ns,debut_fin)

data rest5_all;
	set rest5_all;
	total_1982_1984=app_1982_1984+cdd_1982_1984 +cdi_1982_1984+int_1982_1984;
	total_%eval(&an_fin-2)_&an_fin=app_%eval(&an_fin-2)_&an_fin+cdd_%eval(&an_fin-2)_&an_fin
		+cdi_%eval(&an_fin-2)_&an_fin+int_%eval(&an_fin-2)_&an_fin;
run;

%export_synthese(rest5_all,%bquote(C - Qualit de l'emploi),Statut,
%bquote(volution du statut des personnes en emploi entre 1982-1984 et %eval(&an_fin-2)-&an_fin),
L99C3:L99C15,app_1982_1984 cdd_1982_1984 cdi_1982_1984 int_1982_1984 ns_1982_1984 
 total_1982_1984
app_%eval(&an_fin-2)_&an_fin cdd_%eval(&an_fin-2)_&an_fin cdi_%eval(&an_fin-2)_&an_fin 
int_%eval(&an_fin-2)_&an_fin  ns_%eval(&an_fin-2)_&an_fin  
total_%eval(&an_fin-2)_&an_fin,commax3.2)

%let nom=Graphique_6 Anciennet;
libname lib "&chemin1\&nom";

%res_synth(g6,lib.resg6_1982_2014,rep_ancentr,_ _1 _2 _3 _4,debut_fin)

data resg6_all;
	set resg6_all;
	moins1an_1982_1984=__1982_1984+_1_1982_1984;
	moins1an_%eval(&an_fin-2)_&an_fin=__%eval(&an_fin-2)_&an_fin+_1_%eval(&an_fin-2)_&an_fin;
run; 

%export_synthese(resg6_all,%bquote(C - Qualit de l'emploi),Anciennet,
%bquote(volution de l'anciennet dans l'entreprise des personnes en emploi entre 1982-1984 et %eval(&an_fin-2)-&an_fin),
L10C3:L98C15,moins1an_1982_1984 _2_1982_1984 _3_1982_1984 _4_1982_1984
 total_1982_1984
moins1an_%eval(&an_fin-2)_&an_fin _2_%eval(&an_fin-2)_&an_fin _3_%eval(&an_fin-2)_&an_fin
_4_%eval(&an_fin-2)_&an_fin total_%eval(&an_fin-2)_&an_fin,commax3.2)

%let nom=Graphique_7 Horaires;
libname lib "&chemin1\&nom";

 data resg7;
	set lib.resg7_2003_2014(where=(fap_3 ne "ZZZ" and rep_nuit="1"));
	keep fap_3 partn%eval(&an_fin-2)_&an_fin parts%eval(&an_fin-2)_&an_fin
	partd%eval(&an_fin-2)_&an_fin;
	if fap_3 in("X0Z","T6Z") then do; 
		partn%eval(&an_fin-2)_&an_fin=.;
		parts%eval(&an_fin-2)_&an_fin=.;
		partd%eval(&an_fin-2)_&an_fin=.;
	end;
	if fap_3="T2Z" then delete;
run;
	data resg7;
		set resg7;
		if fap_3="ENS" then fap_3="ZENS";
	run;
	proc sort data=resg7;by fap_3;run;
%export_synthese(resg7,%bquote(C - Qualit de l'emploi),Horaires atypiques,
%bquote(Horaires de travail "atypiques" des personnes en emploi en %eval(&an_fin-2)-&an_fin),
L9C3:L96C15,partn%eval(&an_fin-2)_&an_fin parts%eval(&an_fin-2)_&an_fin partd%eval(&an_fin-2)_&an_fin
,commax3.2)

%let nom=Tableau_6 Temps travail;
libname lib "&chemin1\&nom";

 data rest6;
	set lib.rest6_2003_2014(where=(fap_3 ne "ZZZ" and rep_sousempl="1"));
	keep fap_3 parttp%eval(&an_fin-2)_&an_fin parttps%eval(&an_fin-2)_&an_fin
	partss%eval(&an_fin-2)_&an_fin;
	if fap_3 in("X0Z","T6Z") then do; 
		parttp%eval(&an_fin-2)_&an_fin=.;
		parttps%eval(&an_fin-2)_&an_fin=.;
		partss%eval(&an_fin-2)_&an_fin=.;
	end;
	if fap_3="T2Z" then delete;
run;
	data rest6;
		set rest6;
		if fap_3="ENS" then fap_3="ZENS";
	run;
	proc sort data=rest6;by fap_3;run;

%export_synthese(rest6,%bquote(C - Qualit de l'emploi),Temps travail,
%bquote(Temps de travail et sous-emploi des personnes en emploi en %eval(&an_fin-2)-&an_fin),
L8C3:L96C15,parttp%eval(&an_fin-2)_&an_fin parttps%eval(&an_fin-2)_&an_fin partss%eval(&an_fin-2)_&an_fin
,commax3.2)

%let nom=Graphique_8 Salaires;
libname lib "&chemin1\&nom";

%res_synth(g8,lib.resdiffg8_2003_2014,rep_sal,_1250 _1500 _2000 _3000 _3PPP,fin)

%let nom=Tableau_7 Salaire mdian;
libname lib "&chemin1\&nom";

data mediane;
	set lib.salmedian_t7_2003_2014;
	keep fap_3 mediane_%eval(&an_fin-2)_&an_fin;
	mediane_%eval(&an_fin-2)_&an_fin=round(mediane_%eval(&an_fin-2)_&an_fin,100);
	if fap_3="ENS" then fap_3="ZENS";
run;
proc sort data=mediane;by fap_3;run;
data res_sal;
	merge resg8_all mediane;
	by fap_3;
	if fap_3 in("T2Z","ZZZ") then delete;
	if total_%eval(&an_fin-2)_&an_fin=. then mediane_%eval(&an_fin-2)_&an_fin=.;
run;
%export_synthese(res_sal,%bquote(C - Qualit de l'emploi),Salaires,
%bquote(Salaire mensuel net des salaris  temps complet en emploi en %eval(&an_fin-2)-&an_fin),
L9C3:L96C15,_1250_%eval(&an_fin-2)_&an_fin _1500_%eval(&an_fin-2)_&an_fin _2000_%eval(&an_fin-2)_&an_fin
_3000_%eval(&an_fin-2)_&an_fin _3PPP_%eval(&an_fin-2)_&an_fin total_%eval(&an_fin-2)_&an_fin 
mediane_%eval(&an_fin-2)_&an_fin
,commax4.2)

	/*sauvegarde du fichier Excel*/
	FILENAME cmds DDE 'Excel|system';
	DATA _NULL_;
		FILE cmds;
		PUT '[save()]';
	RUN;
	/*fermeture du fichier*/
	data _null_ ;
		file cmds ;
		put "[FILE-CLOSE()]" ;
	run ;




/***************************************************/
/* Tableau D - Caractristiques des employeurs.xls */
/***************************************************/

%let nom=Graphique_9 Catgorie employeur;
libname lib "&chemin1\&nom";

data resg9;
	set lib.resdiffg9_2003_2014;
	attrib cat format=$8.;
	if type="Etablissements < 10 salaris" then cat="Moins10";
	if type="Etablissements < 50 salaris" then cat="Moins50";
	if type="Etablissements < 500 salaris" then cat="Moins500";
	if type="Etablissements >=500 salaris" then cat="Plus500";
	if type="Etat, collectivits, hpitaux publics" then cat="Etat";
	if type="Particuliers" then cat="Part";
	if type in("Etablissements < 10 salaris","Etablissements < 50 salaris",
	"Etablissements < 500 salaris","Etablissements >=500 salaris", 
	"Taille inconnue, hors particulier, Eta") then groupe="priv ou entreprise publique";
	else groupe="Etat ou particulier";
	if substr(type,1,2) ne "NR";
run;
proc sql;
	create table resg9 as select *,100*part2012_2014/sum(part2012_2014) as part from resg9 group by
	fap_3;
quit;
proc sql;
	create table resg9 as select *,sum(part) as sum from resg9 group by
	fap_3,groupe;
quit;
data resg9_2;
	set resg9;
	if type ne "Taille inconnue, hors particulier, Eta";
	drop part2012_2014;
run;
proc sql;
	create table resg9_2 as select *,part*sum/sum(part) as part2 from resg9_2 group by
	fap_3,groupe;
quit;
data resg9_2;
	set resg9_2;
	rename part2=part2012_2014;
run;

%res_synth(g9,resg9_2,cat,moins10 moins50 moins500 plus500 etat part,fin)

data resg9_all;
	set resg9_all;
	if fap_3 in("ZZZ","T2Z") then delete;
run;

	filename cmds DDE 'excel|system' ;
	data _null_ ;
		file cmds ;
		put '[FILE-OPEN("'"&chemin.\%bquote(D - Caractristiques des employeurs).xls"'")]' ;
	run ;

%export_synthese(resg9_all,%bquote(D - Caractristiques des employeurs),Catgorie employeur,
%bquote(Catgorie d'employeur des salaris en emploi en %eval(&an_fin-2)-&an_fin),
L9C3:L96C15,moins10_%eval(&an_fin-2)_&an_fin moins50_%eval(&an_fin-2)_&an_fin
moins500_%eval(&an_fin-2)_&an_fin plus500_%eval(&an_fin-2)_&an_fin 
etat_%eval(&an_fin-2)_&an_fin part_%eval(&an_fin-2)_&an_fin total_%eval(&an_fin-2)_&an_fin
,commax3.2)

	/*sauvegarde du fichier Excel*/
	FILENAME cmds DDE 'Excel|system';
	DATA _NULL_;
		FILE cmds;
		PUT '[save()]';
	RUN;
	/*fermeture du fichier*/
	data _null_ ;
		file cmds ;
		put "[FILE-CLOSE()]" ;
	run ;




/****************************/
/* Tableau E - Mobilit.xls */
/****************************/

%let nom=Graphique_10 MMO;
libname lib "&chemin1\&nom";

%macro g10;
data resg10;
	set lib.resmmo;
	
	if fap_3 in("A0Z","L6Z","P0Z","P1Z","P2Z","P3Z","P4Z","S3Z","T1Z","T2B","T6Z","V2Z","W0Z","X0Z") then do;
		%do an=2007 %to &anneeMMO;
		taux_entree_&an=.;
		taux_sortie_&an=.;
		taux_rotation_&an=.;
		%end;
	end;
	if fap_3 in("T2Z","ZZZ") then delete;
	if fap_3="ENS" then fap_3="ZENS";
run;
%mend;
%g10;

proc sort data=resg10;by fap_3;run;

	filename cmds DDE 'excel|system' ;
	data _null_ ;
		file cmds ;
		put '[FILE-OPEN("'"&chemin.\%bquote(E - Mobilit).xls"'")]' ;
	run ;

%export_synthese(resg10,%bquote(E - Mobilit),%bquote(Taux d%str(')entre),
%bquote(Evolution des taux d'entre des salaris entre 2007 et %eval(&anneeMMO)),
L8C3:L96C15, taux_entree_2007 taux_entree_2008 taux_entree_2009 taux_entree_2010 taux_entree_2011 taux_entree_2012 taux_entree_2013 taux_entree_2014,commax4.2)
%export_synthese(resg10,%bquote(E - Mobilit),%bquote(Taux de sortie),
%bquote(Evolution des taux de sortie des salaris entre 2007 et %eval(&anneeMMO)),
L8C3:L96C15, taux_sortie_2007 taux_sortie_2008 taux_sortie_2009 taux_sortie_2010 taux_sortie_2011 taux_sortie_2012 taux_sortie_2013 taux_sortie_2014,commax4.2)
%export_synthese(resg10,%bquote(E - Mobilit),%bquote(Taux de rotation),
%bquote(Evolution des taux de rotation des salaris entre 2007 et %eval(&anneeMMO)),
L8C3:L96C15, taux_rotation_2007 taux_rotation_2008 taux_rotation_2009 taux_rotation_2010 taux_rotation_2011 taux_rotation_2012 taux_rotation_2013 taux_rotation_2014,commax4.2)

%let nom=Graphique_11 Entres;
libname lib "&chemin1\&nom";

%macro g11;
data resg11;
	set lib.entrees_mmo(where=(fap_3 ne "ZZZ"));

	if fap_3 in("A0Z","L6Z","P0Z","P1Z","P2Z","P3Z","P4Z","S3Z","T1Z","T2B","T6Z","V2Z","W0Z","X0Z") then do;
		%do an=2007 %to &anneeMMO;
		part&an=.;
		%end;
	end;
	if fap_3="ENS" then fap_3="ZENS";
	if fap_3 in("T2Z","ZZZ") then delete;
run;
%mend;
%g11;
proc sort data=resg11;by type_entree fap_3;run;
data resg11;
set resg11;
if type_entree="CDD";
run;

%export_synthese(resg11,%bquote(E - Mobilit),Embauches,
%bquote(Evolution de la part des CDD dans les embauches des salaris entre 2007 et %eval(&anneeMMO)),
L8C3:L96C15, part2007 part2008 part2009 part2010 part2011 part2012 part2013 part2014,commax3.2)

data resg11;
	set lib.sorties_mmo(where=(fap_3 ne "ZZZ"));
	if fap_3 in("A0Z","L6Z","P0Z","P1Z","P2Z","P3Z","P4Z","S3Z","T1Z","T2B","T6Z","V2Z","W0Z","X0Z") then do;
		part&anneeMMO=.;
	end;
	if fap_3 in("T2Z","ZZZ") then delete;
	if fap_3="ENS" then fap_3="ZENS";
	keep fap_3 type_sortie code part&anneeMMO sortie;

	if type_sortie="Dmissions" then sortie="Demission";
	if type_sortie="Dparts en retraite" then sortie="Retraite";
	if type_sortie="Licenciements" then sortie="Licenc";
	if type_sortie="Fins de CDD" then sortie="FinCDD";
	if type_sortie="Autres sorties" then sortie="Autre";
	if type_sortie="Fins de priode d'essai" then sortie="FinEssai";
	if type_sortie="Ruptures conventionnelles" then sortie="Conv";

run;

proc sort data=resg11;by fap_3 sortie;run;
proc transpose data=resg11 out=resg11;
	id sortie;
	by fap_3;
	var part&anneeMMO;
run;
data resg11;
	set resg11;
	total=demission+retraite+licenc+fincdd+autre+finessai+conv;
run;

%export_synthese(resg11,%bquote(E - Mobilit),Sorties,
%bquote(Sorties),
L9C3:L96C15,fincdd demission finessai conv licenc retraite autre total,commax3.2)

	/*sauvegarde du fichier Excel*/
	FILENAME cmds DDE 'Excel|system';
	DATA _NULL_;
		FILE cmds;
		PUT '[save()]';
	RUN;
	/*fermeture du fichier*/
	data _null_ ;
		file cmds ;
		put "[FILE-CLOSE()]" ;
	run ;




/*************************************/
/* Tableau F - March du travail.xls */
/*************************************/

%let nom=Tableau_9 DEFM A;
libname lib "&chemin1\&nom";

data defm;
	set lib.defm (where=(fap ne ""));
run;

%macro defm;
data defm;
	set defm;
	%do an=1997 %to &an_fin;
		defma_&an=round(defma_&an,100);
		defmabc_&an=round(defmabc_&an,100);
	%end;
run;
proc means data=defm noprint sum;
	var %do an=1997 %to &an_fin;
		defma_&an defmabc_&an %end;;
	output out=defm2 sum= %do an=1997 %to &an_fin; defma_&an defmabc_&an %end;;
	by fap_3;
run;
%mend;
%defm;


proc sql;
	insert into defm2(fap_3) values("X0Z");
quit;
data defm2;
	set defm2;
	if fap_3 in("T2Z","ZZZ") then delete;
	if fap_3 in("A0Z","L6Z","P0Z","P1Z","P2Z","P3Z","P4Z","S3Z","T6Z","V2Z","W0Z","X0Z") then do; 
		defma_1997=.;defma_1998=.;defma_1999=.;defma_2000=.;defma_2001=.;defma_2002=.;
		defma_2003=.;defma_2004=.;defma_2005=.;defma_2006=.;defma_2007=.;defma_2008=.;
		defma_2009=.;defma_2010=.;defma_2011=.;defma_2012=.;defma_2013=.;defma_2014=.;
		defmabc_1997=.;defmabc_1998=.;defmabc_1999=.;defmabc_2000=.;defmabc_2001=.;defmabc_2002=.;
		defmabc_2003=.;defmabc_2004=.;defmabc_2005=.;defmabc_2006=.;defmabc_2007=.;defmabc_2008=.;
		defmabc_2009=.;defmabc_2010=.;defmabc_2011=.;defmabc_2012=.;defmabc_2013=.;defmabc_2014=.;
	end;
	if fap_3="ENS" then delete;
run;
proc sort data=defm2;by fap_3;run;

	filename cmds DDE 'excel|system' ;
	data _null_ ;
		file cmds ;
		put '[FILE-OPEN("'"&chemin.\%bquote(F - March du travail).xls"'")]' ;
	run ;

%export_synthese(defm2,%bquote(F - March du travail),DEFM A,
%bquote(volution du nombre de demandeurs d'emploi de catgorie A entre 1997 et &an_fin),
L9C3:L96C20,defma_1997 defma_1998 defma_1999 defma_2000 defma_2001 defma_2002 
		defma_2003 defma_2004 defma_2005 defma_2006 defma_2007 defma_2008 
		defma_2009 defma_2010 defma_2011 defma_2012 defma_2013 defma_2014,7.0)

%export_synthese(defm2,%bquote(F - March du travail),DEFM ABC,
%bquote(volution du nombre de demandeurs d'emploi de catgorie A, B, C entre 1997 et &an_fin),
L9C3:L96C20,defmabc_1997 defmabc_1998 defmabc_1999 defmabc_2000 defmabc_2001 defmabc_2002 
		defmabc_2003 defmabc_2004 defmabc_2005 defmabc_2006 defmabc_2007 defmabc_2008 
		defmabc_2009 defmabc_2010 defmabc_2011 defmabc_2012 defmabc_2013 defmabc_2014,7.0)

%let nom=Graphique_14 Age DE;
libname lib "&chemin1\&nom";

data res14;
	set lib.res;
	if fap_3 in("T2Z","ZZZ") then delete;
	if fap_3 in("A0Z","L6Z","P0Z","P1Z","P2Z","P3Z","P4Z","S3Z","T6Z","V2Z","W0Z","X0Z") then do; 
		part_agem1=.;part_agem2=.;part_agem3=.;part_agem4=.;part_agem5=.;part_tot=.;
	end;
	if fap_3="ENS" then fap_3="ZENS";
	total=part_tot;
run;
proc sql;
	insert into res14(fap_3) values ("X0Z");
quit;
proc sort data=res14;by fap_3;run;

%export_synthese(res14,%bquote(F - March du travail),Age DEFM A,
%bquote(Structure par ge des demandeurs d'emploi de catgorie A en &an_fin),
L9C3:L96C20,part_agem1 part_agem2 part_agem3 part_agem4 part_agem5 total
,commax3.2)

%let nom=Tableau_11 Caractristiques DEFM A;
libname lib "&chemin1\&nom";

data res11;
	set lib.res;
	if fap_3 in("T2Z","ZZZ") then delete;
	if fap_3 in("A0Z","L6Z","P0Z","P1Z","P2Z","P3Z","P4Z","S3Z","T6Z","V2Z","W0Z","X0Z") then do; 
		part_femmes_a=.; part_defm_1an_ou_plus=.;
	end;
	if fap_3="ENS" then fap_3="ZENS";
run;
proc sql;
	insert into res11(fap_3) values ("X0Z");
quit;
proc sort data=res11;by fap_3;run;
%export_synthese(res11,%bquote(F - March du travail),Caractristiques DEFM A,
%bquote(Caractristiques des demandeurs d'emploi de catgorie A en &an_fin),
L8C3:L96C20,part_femmes_a part_defm_1an_ou_plus,commax3.2)

%let nom=Graphique_15 Diplme DE;
libname lib "&chemin1\&nom";

data res15;
	set lib.res;
	if fap_3 in("T2Z","ZZZ") then delete;
	if fap_3 in("A0Z","L6Z","P0Z","P1Z","P2Z","P3Z","P4Z","S3Z","T6Z","V2Z","W0Z","X0Z") then do; 
		part_dipl1=.; part_dipl2=.; part_dipl3=.; part_dipl4=.; part_dipl5=.; part_tot=.;
	end;
	if fap_3="ENS" then fap_3="ZENS";
run;
proc sql;
	insert into res15(fap_3) values ("X0Z");
quit;

proc sort data=res15;by fap_3;run;
%export_synthese(res15,%bquote(F - March du travail),Diplme DEFM A,
%bquote(Niveau de diplme des demandeurs d'emploi de catgorie A en &an_fin),
L9C3:L96C20,part_dipl1 part_dipl2 part_dipl3 part_dipl4 part_dipl5 part_tot,commax3.2)

%let nom=Tableau_12 March du travail;
libname lib "&chemin1\&nom";
data rest12;
	set lib.res;
	if fap_3 in("T2Z","ZZZ") then delete;
	if fap_3 in("A0Z","L6Z","P0Z","P1Z","P2Z","P3Z","P4Z","S3Z","T6Z","V2Z","W0Z","X0Z") then do; 
		taux_demande=.; taux_demande_femme=.; taux_demande_homme=.; ec_df1a8=.;
		part_contrats_plus6mois=.;
	end;
	if fap_3="ENS" then fap_3="ZENS";
run;
/* proc sql;
	insert into rest12(fap_3) values ("X0Z");
quit; */

proc sort data=rest12;by fap_3;run;
%export_synthese(rest12,%bquote(F - March du travail),Caractristiques march travail,
%bquote(Caractristiques du march du travail en &an_fin),
L8C3:L96C20,taux_demande taux_demande_femme taux_demande_homme ec_df1a8 
part_contrats_plus6mois,commax3.2)

%let nom=Graphique_16 Tensions;
libname lib "&chemin1\&nom";

data resg16;
	set lib.tensions(where=(fap ne "ZZZ"));
	if fap in("A0Z","L6Z","P0Z","P1Z","P2Z","P3Z","P4Z","S3Z","T6Z","V2Z","W0Z","X0Z") then do; 
		tension_ga=.;
	end;
	if fap="ENS" then fap="ZENS";
	if fap in("T2Z","ZZZ") then delete;
run;
proc sort data=resg16;by fap;run;
proc transpose data=resg16 out=resg16;
	id datetrim;
	by fap;
	var tension_ga;
run;
proc sql;
	insert into resg16(fap) values("A0Z");
	insert into resg16(fap) values("L6Z");
	insert into resg16(fap) values("P0Z");
	insert into resg16(fap) values("P1Z");
	insert into resg16(fap) values("P2Z");
	insert into resg16(fap) values("P3Z");
	insert into resg16(fap) values("P4Z");
	insert into resg16(fap) values("S3Z");
	*insert into resg16(fap) values("T6Z");
	insert into resg16(fap) values("V2Z");
	insert into resg16(fap) values("W0Z");
	insert into resg16(fap) values("X0Z");
quit;
proc sort data=resg16;by fap;run;
%export_synthese(resg16,%bquote(F - March du travail),Tensions,
%bquote(volution des tensions sur le march du travail entre 1998 et &an_fin),
L9C3:L96C80,_199803 _199806 _199809 _199812 _199903 _199906 _199909 _199912
_200003 _200006 _200009 _200012 _200103 _200106 _200109 _200112
_200203 _200206 _200209 _200212 _200303 _200306 _200309 _200312
_200403 _200406 _200409 _200412 _200503 _200506 _200509 _200512
_200603 _200606 _200609 _200612 _200703 _200706 _200709 _200712
_200803 _200806 _200809 _200812 _200903 _200906 _200909 _200912
_201003 _201006 _201009 _201012 _201103 _201106 _201109 _201112
_201203 _201206 _201209 _201212 _201303 _201306 _201309 _201312
_201403 _201406 _201409 _201412 ,commax4.3)

%let nom=Graphique_17 Entres PE;
libname lib "&chemin1\&nom";

data resg17;
	set lib.res(where=(fap_3 ne "ZZZ"));
	if fap_3 in("A0Z","L6Z","P0Z","P1Z","P2Z","P3Z","P4Z","S3Z","T6Z","V2Z","W0Z","X0Z") then do; 
		part_e_inlic=.;part_e_indem=.;part_e_infin=.;part_e_in1er=.;part_e_inrep=.;
		part_e_inxxx=.;tot=.;
	end;
	if fap_3 in("T2Z","ZZZ") then delete;
	if fap_3="" then fap_3="ZENS";
run;
proc sql;
	insert into resg17(fap_3) values ("X0Z");
quit;

proc sort data=resg17;by fap_3;run;

%export_synthese(resg17,%bquote(F - March du travail),Entres,
%bquote(Entres  Ple emploi par motif en &an_fin (catgories A, B, C)),
L9C3:L96C15,part_e_infin part_e_inlic part_e_indem part_e_in1er part_e_inrep part_e_inxxx tot,commax3.2)

%let nom=Graphique_18 Sorties PE;
libname lib "&chemin1\&nom";

data resg18;
	set lib.res(where=(fap_3 ne "ZZZ"));
	if fap_3 in("A0Z","L6Z","P0Z","P1Z","P2Z","P3Z","P4Z","S3Z","T6Z","V2Z","W0Z","X0Z") then do; 
		part_s_repemp=.;part_s_stage=.;part_s_arretx=.;part_s_radiat=.;part_s_noactu=.;
		part_s_autrex=.;tot=.;
	end;
	if fap_3 in("T2Z","ZZZ") then delete;
	if fap_3="" then fap_3="ZENS";
run;
proc sql;
	insert into resg18(fap_3) values ("X0Z");
quit;

proc sort data=resg18;by fap_3;run;

%export_synthese(resg18,%bquote(F - March du travail),Sorties,
%bquote(Sorties  Ple emploi par motif en &an_fin (catgories A, B, C)),
L9C3:L96C60,part_s_repemp part_s_stage part_s_arretx part_s_radiat part_s_noactu part_s_autrex tot,commax3.2)

	/*sauvegarde du fichier Excel*/
	FILENAME cmds DDE 'Excel|system';
	DATA _NULL_;
		FILE cmds;
		PUT '[save()]';
	RUN;
	/*fermeture du fichier*/
	data _null_ ;
		file cmds ;
		put "[FILE-CLOSE()]" ;
	run ;

	/*fermer excel*/
	data _null_ ;
		file cmds ;
		put "[QUIT()]" ;
	run ;
