%include "C:\Users\guillaume.toure\Desktop\DAMETH_TOURE\PSM\preparation_donnees\PSM 2021\Programmes\macros portraits fap22.sas";

%let deb=1982;
%let fin=2020;/*2014*/
%let nom=g6;
%let nom_rep=Graphique_6 Anciennet� fap22;

libname eec "C:\Users\guillaume.toure\Desktop\DAMETH_TOURE\PSM\preparation_donnees\Series longues 2021\Bases SAS\s�ries_longues";

%let chemin=C:\Users\guillaume.toure\Desktop\DAMETH_TOURE\PSM\preparation_donnees\PSM 2021\Sorties\&nom_rep;
libname libout "&chemin";

%macro prep_anciennete(lib);

%do an=&deb %to &fin;
	data sl_dmq_&an;
		set &lib..sl_dmq_&an. ;
		%if &an.<2003 %then rep_ancentr=ancentr1;
		%else rep_ancentr=ancentr4;
		;
		/* Ancien programme -> f�vrier 2013 :
		if rep_ancentr='' then rep_ancentr="#"; */
		/* La variable ancentr n'est pas renseign�e pour les int�rimaires.
		La non-r�ponse est comptabilis�e dans la modalit� "Moins d'un an".
		Il parait plus pertinent d'affecter la modalit� "Moins d'un an" seulement 
		aux int�rimaires (statut="21") et � d'autres cat�gories pour lesquelles
		c'est justifi� (militaires du contingent, saisonniers, stagiaires)
		et pas � tous les non-r�pondants. */

		if rep_ancentr="" then do;
			%if &an>=2003 %then %do;
				if statut="21" then rep_ancentr="#"; /*interim*/
				if statut in("34","44") then rep_ancentr="#"; /*stagiaire*/
				if contra="3" then rep_ancentr="#"; /*saisonnier*/
			%end;
			%if &an<=1989 %then %do;
				if statut="21" then rep_ancentr="#"; /*interim*/
				if statut ="23" then rep_ancentr="#"; /*stagiaire*/
				if statut="24" then rep_ancentr="#"; /*saisonnier*/
			%end;
			%if &an>1989 and &an<2003 %then %do;
				if statut="21" then rep_ancentr="#"; /*interim*/
				if statut ="30" then rep_ancentr="#"; /*stagiaire*/
			%end;
			%if &an<2003 %then %do;
				if det="1" then rep_ancentr="#"; /*saisonnier*/
			%end;
			if cstot="83" then rep_ancentr="#";/*militaires du contingent*/
		end;

	run;
%end;
%mend;

%prep_anciennete(eec);

%calculs_fap22(chemin_res=&chemin,nom=&nom,an_deb=&deb,an_fin=&fin,nom_table_prep=sl_dmq,
	lib_donnees=work,lib_sortie=libout,var=rep_ancentr,var_ventil=,var_pond=poids_final,
	seuil_diffusion=100);

%prep_fichier_fap22(an_deb=&deb,an_fin=&fin,chemin=&chemin,nom_tab=libout.res&nom._&deb._&fin,
	nom_tab_diff=libout.resdiff&nom._&deb._&fin,nom_graph=&nom_rep,var=rep_ancentr,
	var_ventil=);








