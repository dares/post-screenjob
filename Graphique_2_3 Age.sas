%include "C:\Users\guillaume.toure\Desktop\DAMETH_TOURE\PSM\preparation_donnees\PSM 2021\Programmes\macros portraits.sas";

%let deb=1982;
%let fin=2020;/*2014*/
%let nom=g2;
%let nom_rep1=Graphique_2 Age d�taill�;
%let nom_rep2=Graphique_3 Age;

libname eec "C:\Users\guillaume.toure\Desktop\DAMETH_TOURE\PSM\preparation_donnees\Series longues 2021\Bases SAS\s�ries_longues";

%let chemin1=C:\Users\guillaume.toure\Desktop\DAMETH_TOURE\PSM\preparation_donnees\PSM 2021\Sorties\&nom_rep1;
libname libout "&chemin1";

%let chemin2=C:\Users\guillaume.toure\Desktop\DAMETH_TOURE\PSM\preparation_donnees\PSM 2021\Sorties\&nom_rep2;


/* Prparation prliminaire des donnes. */
%macro prep_age(lib);

%do an=&deb %to &fin;
	data sl_dmq_&an;
		set &lib..sl_dmq_&an. ;
		%if &an<1990 %then %do;
			keep  idt_aire im_loc noi fap_3 poids_final age_enq naim rep_age_enq;
		%end;
		%if &an>=1990 and &an<2003 %then %do;
			keep  aire imloc noi fap_3 poids_final age_enq var seuil
			jj ag_num naim mm rep_age_enq;
		%end;
		%if &an>=2003 %then %do;
			keep  ident noi fap_3 poids_final annee age age_enq
			rep_age_enq;
		%end;
		%if  &an.<1990 or &an.>2002 %then %do;
			age_enq=age;
		%end;
		%else %do;
			var=ranuni(-1);
			seuil=JJ/31;
			ag_num=input(ag,3.);
			age_enq=ag-(NAIM>MM)-(NAIM=MM)*(var>seuil);
		%end;

		select ;
		when (age_enq<25) rep_age_enq='15';
		when (age_enq<30) rep_age_enq='25';
		when (age_enq<35) rep_age_enq='30';
		when (age_enq<40) rep_age_enq='35';
		when (age_enq<45) rep_age_enq='40';
		when (age_enq<50) rep_age_enq='45';
		when (age_enq<55) rep_age_enq='50';
		when (age_enq<60) rep_age_enq='55';
		when (age_enq>=60) rep_age_enq='60';
		otherwise rep_age_enq='XX';
		end;

	run;
%end;
%mend;
%prep_age(eec);

options mprint mlogic symbolgen;

%calculs(chemin_res=&chemin1,nom=&nom,an_deb=&deb,an_fin=&fin,
	nom_table_prep=sl_dmq,lib_donnees=work,lib_sortie=libout,var=rep_age_enq,
	var_ventil=,var_pond=poids_final,seuil_diffusion=100)

/*graphique 2*/
%prep_fichier(an_deb=&deb,an_fin=&fin,chemin=&chemin1,
	nom_tab=libout.res&nom._&deb._&fin,nom_tab_diff=libout.resdiff&nom._&deb._&fin,
	nom_graph=&nom_rep1,var=rep_age_enq,var_ventil=);

/*graphique 3*/
%prep_fichier(an_deb=&deb,an_fin=&fin,chemin=&chemin2,
	nom_tab=libout.res&nom._&deb._&fin,nom_tab_diff=libout.resdiff&nom._&deb._&fin,
	nom_graph=&nom_rep2,var=rep_age_enq,var_ventil=);

