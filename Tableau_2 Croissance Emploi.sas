%include "C:\Users\guillaume.toure\Desktop\DAMETH_TOURE\PSM\preparation_donnees\PSM 2021\Programmes\macros portraits.sas";

%let deb=1982;
%let fin=2020;
libname eec "C:\Users\guillaume.toure\Desktop\DAMETH_TOURE\PSM\preparation_donnees\Series longues 2021\Bases SAS\s�ries_longues";

%let nom=t2;
%let nom_rep=Tableau_2 Croissance Emploi;

%let chemin=C:\Users\guillaume.toure\Desktop\DAMETH_TOURE\PSM\preparation_donnees\PSM 2021\Sorties\&nom_rep;
libname libout "&chemin";

%calculs(chemin_res=&chemin,nom=&nom,an_deb=&deb,an_fin=&fin,nom_table_prep=sl_dmq,
	lib_donnees=eec,lib_sortie=libout,var=,var_ventil=,var_pond=poids_final,
	seuil_diffusion=100)

%prep_fichier(an_deb=&deb,an_fin=&fin,chemin=&chemin,nom_tab=libout.res&nom._&deb._&fin,
	nom_tab_diff=libout.resdiff&nom._&deb._&fin,nom_graph=&nom_rep,var= ,var_ventil= )

