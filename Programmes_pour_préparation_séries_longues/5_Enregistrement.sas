
%macro enregistrement;

%do annee=1982 %to 2014;
	data lib.sl_dmq_&annee.;
	set calage_2_&annee.;
	drop poids poids_cal;
	run;
%end;

%mend;

%macro enregistrement;

%do annee=2003 %to 2020;
	data lib.sl_dmq_&annee.;
	set calage_2_&annee.;
	drop poids poids_cal;
	run;
%end;

%mend;

proc freq data=lib.sl_dmq_2019;
table fap_5;
run;

/* Enregistrement dans mes fichiers. */
libname lib "C:\Users\guillaume.toure\Desktop\DAMETH_TOURE\PSM\preparation_donnees\Series longues 2021\Bases SAS\s�ries_longues";
%enregistrement;

/* Enregistrement sous W:\ */
libname lib "W:\SERIES_LONGUES_DMQ";
%enregistrement;

%macro comparaison(listan=);
libname refer "C:\Users\guillaume.toure\Desktop\DAMETH_TOURE\PSM\preparation_donnees\donn�es_series_longues"; 
%let j = 1;
%do %while(%scan(&listan., &j., %str( )) ne );
	%let an = %scan(&listan., &j., %str( ));
	proc sort data = refer.sl_dmq_&an.; by id; run;
	proc sort data = lib.sl_dmq_&an.; by id; run;
	proc compare base = refer.sl_dmq_&an. comp = lib.sl_dmq_&an. criterion = 0.1;
		id ID;
	run;

	%let j = %eval(&j. + 1);
%end;

%mend;

%comparaison(listan= 1982 1983 1984 1985 1986 1987 1988 1989 1990 1991 1992 1993 1994 1995 1996 1997 1998 1999 2000 2001 2002 2003 2004 2005 2006 2007 2008 2009 2010 2011 2012 2013);


