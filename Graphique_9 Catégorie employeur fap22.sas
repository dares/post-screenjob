%include "C:\Users\guillaume.toure\Desktop\DAMETH_TOURE\PSM\preparation_donnees\PSM 2021\Programmes\macros portraits fap22.sas";

%let deb=2003;
%let fin=2020;
%let nom=g9;
%let nom_rep=Graphique_9 Cat�gorie employeur fap22;

libname eec "C:\Users\guillaume.toure\Desktop\DAMETH_TOURE\PSM\preparation_donnees\Series longues 2021\Bases SAS\s�ries_longues";

%let chemin=C:\Users\guillaume.toure\Desktop\DAMETH_TOURE\PSM\preparation_donnees\PSM 2021\Sorties\&nom_rep;
libname libout "&chemin";

%macro prep_cat(lib);

%do an=&deb %to &fin;
	data sl_dmq_&an;
		set &lib..sl_dmq_&an. ;

		/* select ;
		when (chpub in('1','2','3')) type="Etat, collectivit�s, h�pitaux publics ";
		when (chpub='4') type="Particuliers		";
		when (chpub in('5','6') and efet ne . and efet<10) type="Etablissements < 10 salari�s			";
		when (chpub in('5','6') and efet ne . and efet<50) type="Etablissements < 50 salari�s			";
		when (chpub in('5','6') and efet ne . and efet<200) type="Etablissements < 200 salari�s			";
		when (chpub in('5','6') and efet ne . and efet>=200) type="Etablissements >=200 salari�s			";
		when (chpub in('5','6') and nbsala in('1','2','3')) type="Etablissements < 10 salari�s			";
		when (chpub in('5','6') and nbsala in('4','5')) type="Etablissements < 50 salari�s			";
		when (chpub in('5','6') and nbsala in('6')) type="Etablissements < 200 salari�s			";
		when (chpub in('5','6') and nbsala in('7','8','9')) type="Etablissements >=200 salari�s			";
		when (chpub in('5','6') and efet=. and nbsala in("","99")) type="Taille inconnue, hors particulier, Etat";
		when (chpub="") type="NR";
		end; */

		%if %eval(&an.)<2013 %then %do;
		select ;
		when (chpub in('1','2','3')) type="Etat, collectivit�s, h�pitaux publics ";
		when (chpub='4') type="Particuliers		";
		when (chpub in('5','6') and efet ne . and efet<10) type="Etablissements < 10 salari�s			";
		when (chpub in('5','6') and efet ne . and efet<50) type="Etablissements < 50 salari�s			";
		when (chpub in('5','6') and efet ne . and efet<500) type="Etablissements < 500 salari�s			";
		when (chpub in('5','6') and efet ne . and efet>=500) type="Etablissements >=500 salari�s			";
		when (chpub in('5','6') and nbsala in('1','2','3')) type="Etablissements < 10 salari�s			";
		when (chpub in('5','6') and nbsala in('4','5')) type="Etablissements < 50 salari�s			";
		when (chpub in('5','6') and nbsala in('6','7')) type="Etablissements < 500 salari�s			";
		when (chpub in('5','6') and nbsala in('8','9')) type="Etablissements >=500 salari�s			";
		when (chpub in('5','6') and efet=. and nbsala in("","99")) type="Taille inconnue, hors particulier, Etat";
		when (chpub="") type="NR";
		otherwise type="XXX";
		end;
		%end;
		%else %do; 
		select ;
		when (chpub in ('3','4','5','6')) type="Etat, collectivit�s, h�pitaux publics ";
		when (chpub='7') type="Particuliers		";
		when (chpub in('2','1') and trefet in ('00','01','02','03')) type="Etablissements < 10 salari�s			";
		when (chpub in('2','1') and trefet in ('11','12')) type="Etablissements < 50 salari�s			";
		when (chpub in('2','1') and trefet in ('21','22','31','32')) type="Etablissements < 500 salari�s			";
		when (chpub in('2','1') and trefet in ('41','42','51','52','53')) type="Etablissements >=500 salari�s			";
		when (chpub in('2','1') and nbsala in ('1','2','3','4','5','6','7','8','9')) type="Etablissements < 10 salari�s			";
		when (chpub in('2','1') and nbsala in ('10')) type="Etablissements < 50 salari�s			";
		/* A partir de 2013, la variable nbsala n'a plus qu'une seule modalit� pour les
		�tablissements de 50 � 499 salari�s. */ 
		when (chpub in('2','1') and nbsala in ('11')) type="Etablissements < 500 salari�s			";
		when (chpub in('2','1') and nbsala in ('12'))  type="Etablissements >=500 salari�s			";
		when (chpub in('2','1') and trefet="" and nbsala in("","99"))  type="Taille inconnue, hors particulier, Etat";
		when (chpub="") type="NR";
		otherwise type="XXX";
		end;
		%end;

		if stat2="2";
	run;
%end;
%mend;

%prep_cat(eec);

%calculs_fap22(chemin_res=&chemin,nom=&nom,an_deb=&deb,an_fin=&fin,nom_table_prep=sl_dmq,
	lib_donnees=work,lib_sortie=libout,var=type,var_ventil=,var_pond=poids_final,
	seuil_diffusion=100);

%prep_fichier_fap22(an_deb=&deb,an_fin=&fin,chemin=&chemin,nom_tab=libout.res&nom._&deb._&fin,
	nom_tab_diff=libout.resdiff&nom._&deb._&fin,nom_graph=&nom_rep,var=type,var_ventil=);

%prep_graph_taille_ech_fap22(an_deb=&deb,an_fin=&fin,chemin=&chemin,
	nom_tab=libout.taille_ech&nom._&deb._&fin,nom_graph=Graphique taille ech sal,
	titre="Taille de l'�chantillon, salari�s (Graphique 9)",var_ventil=)



