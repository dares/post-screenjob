/**********************************************************/
/* D�finir les libnames des programmes SAS en modifiant   */
/* chacune des macro-variable ci-dessous                  */
/**********************************************************/

%let lib_map = C:\Users\guillaume.toure\Desktop\DAMETH_TOURE\PSM\preparation_donnees\Cartes pour SAS;

%let lib_formats = C:\Users\guillaume.toure\Desktop\DAMETH_TOURE\PSM\preparation_donnees\Formats\Formats FAP.sas;

%let chemin_sortie = C:\Users\guillaume.toure\Desktop\DAMETH_TOURE\PSM\preparation_donnees\PSM 2021\Sorties ;

%let chemin_input = C:\Users\guillaume.toure\Desktop\DAMETH_TOURE\PSM\preparation_donnees\PSM 2021\Input;

%let chemin_rpmet = C:\Users\guillaume.toure\Desktop\DAMETH_TOURE\PSM\preparation_donnees\PSM 2021\rpmet;

%let pgm_macro_portrait = C:\Users\guillaume.toure\Desktop\DAMETH_TOURE\PSM\preparation_donnees\PSM 2015\Programmes\macros portraits.sas;

%let donnees_eec = C:\Users\guillaume.toure\Desktop\DAMETH_TOURE\PSM\preparation_donnees\PSM 2015\donn�es ;

%let donnees_nostra = C:\Users\guillaume.toure\Desktop\DAMETH_TOURE\PSM\preparation_donnees\PSM 2015\donn�es;



proc sql noprint;
	create table chemin(
		MACROVAR VARCHAR(30), VALEUR VARCHAR(150));

	insert into chemin values('lib_map', "&lib_map.");
	insert into chemin values('lib_formats', "&lib_formats.");
	insert into chemin values('lib_sortie', "&lib_sortie.");
	insert into chemin values('lib_input', "&chemin_input.");
	insert into chemin values('lib_rpmet', "&chemin_rpmet.");
	insert into chemin values('pgm_macro_portrait', "&pgm_macro_portrait.");
	insert into chemin values('donnees_eec', "&donnees_eec.");
	insert into chemin values('donnees_nostra', "&donnees_nostra.");

quit;
