%include "d:\documents-utilisateurs\charline.babet\Donnees\PSM\PSM 2015\Programmes\macros portraits.sas";
%include "d:\documents-utilisateurs\charline.babet\Donnees\Formats\Formats FAP.sas";

%let deb=2007;
%let fin=2014;
%let annee=2014;

%let nom_rep_entrees=Graphique_11 Entres;
%let nom_rep_sorties=Graphique_12 Sorties;

%let chemin_entrees=d:\documents-utilisateurs\charline.babet\Donnees\PSM\PSM 2015\Sorties\&nom_rep_entrees;
%let chemin_sorties=d:\documents-utilisateurs\charline.babet\Donnees\PSM\PSM 2015\Sorties\&nom_rep_sorties;

%macro dossier(chemin_rep);
	options NOXWAIT NOXSYNC ;
		%let rc=%sysfunc(filename(fileref, &chemin_rep));
		 %if %sysfunc(fexist(&fileref))  %then %do ; 
		 	%end;
			%else %do;
			Data _null_;
				X mkdir "&chemin_rep"; /*Cration du repertoire  l'aide de la macro-variable*/
			run;
		%end;
%mend;
%dossier(&chemin_entrees)
%dossier(&chemin_sorties)


libname mmo "&chemin_entrees";

%macro donnees_entrees_sorties(an_deb,an_fin);
	%do an=&an_deb %to &an_fin;
		proc import datafile="F:\Sources\MMO\mmo191115.xls"
			out=mmo.mmo&an dbms=excel2000 replace;
			sheet="&an";
		run;
		proc summary data=mmo.mmo&an noprint;
			var pds;
			class typees;
			output out=stat&an sum=total&an;
		run;
		data mmo&an;
			set mmo.mmo&an;
			attrib fap format=$5. fap_3 format=$3. type_entree format=$5. type_sortie format=$25.;
				fap=put(lowcase(pcs),$pcse_fap.);
			fap_3=substr(fap,1,3);

			/*  AU	Autre sortie
				CO	Rupture conventionnelle
				DM	Dmission
				EI	Entre inconnue
				ES	Fin de priode essai
				FD	Fin CDD
				FE	Fin CNE initiative employeur
				FN	Fin CNE initiative salari
				LA	Licenciement autre
				LE	Licenciement conomique
				OF	Fin CDD  objet dfini
				OR	Recrutement en CDD  objet dfini
				PR	Dpart en prretraite
				RD	Recrutement CDD
				RI	Recrutement CDI
				RN	Recrutement CNE
				RT	Retraite
				SI	Sortie inconnue
				SN	Service national
				TE	Transfert entre 
				TS	Transfert sortie
			Transferts : transferts entre tablissements d'une mme entreprise
			*/
			/*Entres*/
			if typees="RD" or typees="OR" then type_entree="CDD";
			if typees="RI" then type_entree="CDI";
			if typees in("RN","EI") then type_entree="Autres";
			/*Sorties*/
			if typees in("FD","OF") then type_sortie="Fins de CDD";
			if typees="DM" then type_sortie="Dmissions";
			if typees="RT" then type_sortie="Dparts en retraite";
			if typees="CO" then type_sortie="Ruptures conventionnelles";
			if typees="ES" then type_sortie="Fins de priode d'essai";
			if typees in("LA","LE") then type_sortie="Licenciements";
			if typees in("PR","SI","AU","FE","FN") then type_sortie="Autres sorties";

		run;

		data entrees&an;
			set mmo&an(where=(type_entree ne ""));
		run;
		data sorties&an;
			set mmo&an(where=(type_sortie ne ""));
		run;

		proc summary data=entrees&an noprint;
			var pds;
			class fap_3 type_entree;
			output out=entrees_mmo&an sum=eff&an;
		run;
		data entrees_mmo&an;
			set entrees_mmo&an(where=(_type_ not in(0, 2)));
			if fap_3 ="" then fap_3="ENS";
			code=strip(fap_3)||strip(type_entree);
		run;
		proc sql;
			create table entrees_mmo&an as select *, 100*eff&an/sum(eff&an) as part&an
			from entrees_mmo&an group by fap_3;
		quit;

		proc summary data=sorties&an noprint;
			var pds;
			class fap_3 type_sortie;
			output out=sorties_mmo&an sum=eff&an;
		run;
		data sorties_mmo&an;
			set sorties_mmo&an(where=(_type_ not in(0, 2)));
			if fap_3 ="" then fap_3="ENS";
			code=strip(fap_3)||strip(type_sortie);
		run;
		proc sql;
			create table sorties_mmo&an as select *, 100*eff&an/sum(eff&an) as part&an 
			from sorties_mmo&an group by fap_3;
		quit;
		proc sort data=entrees_mmo&an;by fap_3 type_entree;run;
		proc sort data=sorties_mmo&an;by fap_3 type_sortie;run;
	%end;
	data mmo.entrees_mmo;
		merge %do an=&an_deb %to &an_fin; entrees_mmo&an %end;;
		by fap_3 type_entree;
	run;
	data mmo.sorties_mmo;
		merge %do an=&an_deb %to &an_fin;sorties_mmo&an %end;;
		by fap_3 type_sortie;
	run;
%mend;
%donnees_entrees_sorties(2007,2014)

%macro prep_fichier_mmo(chemin,nom_tab,nom_graph, var, an_deb,an_fin);
	%let chemin_psm=d:\documents-utilisateurs\charline.babet\Donnees\PSM\PSM 2015\Modles;
	proc sql;
		create table temp as select *, max(length(code)) as longueur_code, max(length(&var)) as longueur_var
		from &nom_tab;
	quit;
	data _null_;
		set temp;
		call symput("longueur_code",strip(longueur_code));
		call symput("longueur_var",strip(longueur_var));
	run;
	options missing="";
	options NOXWAIT NOXSYNC ;
	x copy "&chemin_psm.\Modle &nom_graph..xls" "&chemin.\&nom_graph..xls";
	options NOXWAIT NOXSYNC ; *** Open Excel ***; 
	x "start excel"; 
	data _null_ ; 
		x = SLEEP(4) ; 
	run ; 
	filename cmds DDE 'excel|system' ;
	data _null_ ;
		file cmds ;
		put '[FILE-OPEN("'"&chemin.\&nom_graph..xls"'")]' ;
	run ;
	%let plage1=L1C1:L1C40;
	%let plage2=L2C1:L1000C40;
	filename t dde "EXCEL|&chemin.\[&nom_graph..xls]mmo!&plage1" notab;
	data _null_;
		file t lrecl=500;
		set &nom_tab;
		sp="09"X;
		put "Code" sp "Fap3" sp %if "&var" ne "" %then %do; "&var" sp %end;
		%do an=&an_deb  %to &an_fin ; /*"Part */"&an" sp %end;;
		;		
	run;
	 filename t dde "EXCEL|&chemin.\[&nom_graph..xls]mmo!&plage2" notab;
	data _null_;
		file t lrecl=500;
		set &nom_tab;
		sp="09"X;
		put code $&longueur_code.. sp fap_3 $3. sp %if "&var" ne "" %then %do; &var $&longueur_var.. sp %end;
		%do an=&an_deb  %to &an_fin ; part&an commax6.2 sp %end;;
		;		
	run;
	options missing=".";
	/*sauvegarde du fichier Excel*/
	FILENAME cmds DDE 'Excel|system';
	DATA _NULL_;
		FILE cmds;
		PUT '[save()]';
	RUN;
	/*fermeture du fichier*/
	data _null_ ;
		file cmds ;
		put "[FILE-CLOSE()]" ;
	run ;
	/*fermer excel*/
	data _null_ ;
		file cmds ;
		put "[QUIT()]" ;
	run ;

%mend;

%macro zero(an_deb,an_fin);
	%do an=&an_deb %to &an_fin;
		data mmo.entrees_mmo;
		set mmo.entrees_mmo;
			if part&an=. then part&an=0;
		run;
		data mmo.sorties_mmo;
		set mmo.sorties_mmo;
			if part&an=. then part&an=0;
		run;
	%end;
%mend;
%zero(2007,2014);

%prep_fichier_mmo(&chemin_entrees,mmo.entrees_mmo,&nom_rep_entrees, type_entree,2007,2014);
%prep_fichier_mmo(&chemin_sorties,mmo.sorties_mmo,&nom_rep_sorties, type_sortie,2007,2014);

