libname eea "C:\Users\guillaume.toure\Desktop\DAMETH_TOURE\PSM\preparation_donnees\Series longues 2021\Bases SAS\EEA"; /*"D:\Utilisateurs\charline.babet\Donnees\Sources\EEA";*/
libname eec "C:\Users\guillaume.toure\Desktop\DAMETH_TOURE\PSM\preparation_donnees\Series longues 2021\Bases SAS\EEC";
libname lib "C:\Users\guillaume.toure\Desktop\DAMETH_TOURE\PSM\preparation_donnees\Series longues 2021\Bases SAS"; /*"d:\utilisateurs\charline.babet\Donnees\Series longues\Series longues 2015\Bases SAS";*/
%inc "C:\Users\guillaume.toure\Desktop\DAMETH_TOURE\PSM\preparation_donnees\Series longues 2021\Programmes et sorties SAS 2021\0_Formats.sas";

*options mprint mlogic symbolgen;

/*** Recodage des FAP. */
%macro codage_FAP2009;

%do an=1982 %to 2020;

data a&an.;
 FAP_5="ZZZZZ";
%if %eval(&an.)<2003
	%then %do; /* Cas de l"EEA */
	set eea.q&an. (where=(%if %eval(&an.)<1990 	%then actbit1 in ("1","5") ;
												%else act= "1"; ));
	FAP_3=put(P,$P82F09_.);
%end;

%else %do; /* Cas de l"EEC */
	set eec.qc&an. (where=(acteu="1"));
	FAP_3=substr(put(P,$P03F09_.),1,3);
 	FAP_5=put(P,$P03F09_.);
%end;

/* Am�lioration des correspondace PCS->FAP en utilisant la NAF & la NAP dans les EE avant 2003. */
%if %eval(&an.)<2003
%then %do;

	if p="2113" then do;  
		if  %if %eval(&an.)<1993 %then nap1 in ("55");
			%else naf in ("452A","452B","452J","452K","452L","452T","452V","453A","453C","453E","453F","454A","454C","454D","454F","454H","454J","454L","454M","742A","742B","742C","743A","743B");
		then FAP_3="B4Z";
		else FAP_3="D6Z";
	end;

	if p="4627" then do;  
		if  %if %eval(&an.)<1993 %then nap1 in ("78","88","89");
			%else substr(naf,1,2) in ("65","66","67");
		then FAP_3="Q1Z";
		else FAP_3="R2Z";
	end;

	if p="6226" then do;  
		if  %if %eval(&an.)<1993 %then nap1 in ("55");
			%else naf in ("452A","452B","452J","452K","452L","452T","452V","453A","453C","453E","453F","454A","454C","454D","454F","454H","454J","454L","454M");
		then FAP_3="B2Z";
		else FAP_3="D1Z";
	end;

	if p="6255" then do;  
		if  %if %eval(&an.)<1993 %then nap1 = "35";	
			%else substr(naf,1,3) = "151";
		then FAP_3="S0Z";
		else FAP_3="E1Z";
	end;

	if p="6799" then do;  
		if  %if %eval(&an.)<1993 %then nap1 = "51";
			%else substr(naf,1,2) = "22";
		then FAP_3="F4Z";
		else FAP_3="E0Z";
	end;

	if p="5631" then do;  
		%if %eval(&an.)<1994 %then FAP_3="T2Z";
		%else %if %eval(&an.)<2003 %then %do;
		if MAISON='1' then do; FAP_3="T2B"; end;
		else FAP_3="T2A";
		%end;
		%else %do;
		if MAISOC='1' then FAP_3="T2B";
		else FAP_3="T2A";
		%end;;
	end;

	if p="5611" then do;  
 		%if %eval(&an.)<1990 %then %do;
		if fonct in ('4','5') then FAP_3="S1Z";
		else FAP_3="S2Z";
		%end;
 		%else %if %eval(&an.)<2003 %then %do;
		if fonct in ('1','2','3','4') then FAP_3="S1Z";
		else FAP_3="S2Z";
		%end;
		%else %do;
		if fonctc in ('1','2','3','4') then FAP_3="S1Z";
		else FAP_3="S2Z";
		%end;
	end;

	if p="5518" then do;  
 		%if %eval(&an.)<1990 %then %do;
		if fonct in ('5') then FAP_3="R0Z";
		else FAP_3="R1Z";
 		%end;
 		%else %if %eval(&an.)<2003 %then %do;
		if fonct in ('2','3','4') then FAP_3="R0Z";
		else FAP_3="R1Z";
 		%end;
 		%else %do;
		if fonctc in ('2','3','4') then FAP_3="R0Z";
		else FAP_3="R1Z";
		%end;
	end;

	if p="4723" then do;  
 		%if %eval(&an.)<1990 %then %do;
		if fonct in ('5') then FAP_3="G1Z";
		else FAP_3="D6Z";
 		%end;
 		%else %if %eval(&an.)<2003 %then %do;
		if fonct in ('2','3','4') then FAP_3="G1Z";
		else FAP_3="D6Z";
 		%end;
 		%else %do;
		if fonctc in ('2','3','4') then FAP_3="G1Z";
		else FAP_3="D6Z";
		%end;
	end;

	if p="5632" then do;  
		%if %eval(&an.)<1994 %then %do; 
		FAP_3="T1Z";
		%end;
		%else %if %eval(&an.)<2003 %then %do;
		if MAISON='1' then FAP_3="T2B";
		else FAP_3="T1Z";
		%end;
		%else %do;
		if MAISOC='1' then FAP_3="T2B";
		else FAP_3="T1Z";
		%end;
	end;

%end;


/* Codage CS. */
if substr(p,1,1) in ("1","2","3","4") then cs=substr(p,1,1)||"X";
if substr(p,1,1)="5" then do;
	if p in ("5216","5217","5222","5317","5415","5417","5512","5518","5519","5521","5611","5614","5631","5632","5633","5634")
	or p in ("525a","525b","525c","525d","533c","534a","541d","542b","551a","552a","553a","554a","554h","554j","555a","561a","561d","561e", "561f" ,"563a" ,"563b","563c","564a","564b")
	then cs="5N";
	else cs="5Q";
end;

if substr(p,1,1)="6" then do;
	if substr(p,1,2) in ("67","68","69") then cs="6N";
	else cs="6Q";
end;

/* Traitement des CS "autres". */
if p="5499" then p="";
if substr(p,1,1) in (""," ","0","7") then cs="7X";
if substr(p,1,1) in (""," ","0","7") then FAP_3="ZZZ";

/* Traitement des CS des militaires du contingent. */
if cstot="83" then cs="7X";
if cstot="83" then FAP_3="P4Z";
/* Cas particulier de 2002 o� 5 professions sont mal cod�es. */
%if %eval(&an.)=2002 %then %do; if cs='7X' and fap_3="P4Z" then fap_3="ZZZ"; %end;

cs_2=substr(p,1,2);

/* On d�finit une unique variable de pond�ration. */
	%if %eval(&an.)<2003 %then poids=extri;
	%else %if %eval(&an.)<2013 %then poids=extri/4;
	%else poids=extri/4;; /*extri16/4;;*/ 
	poids=poids/1000;
	if poids not in (0,.);

	%if %eval(&an.)>2002 %then %do; 
		poids_cal=poids;
		poids_final=poids;
	%end;

/* Variables utiles pour le calage. */
	ID=_N_;

	if cs="1X" then cs_cal=1;
	if cs="2X" then cs_cal=2;
	if cs="3X" then cs_cal=3;
	if cs="4X" then cs_cal=4;
	if cs="5Q" then cs_cal=5;
	if cs="5N" then cs_cal=6;
	if cs="6Q" then cs_cal=7;
	if cs="6N" then cs_cal=8;
	if cs="7X" then cs_cal=9;

	if fap_3="A0Z" then fap_cal=1;
	if fap_3="A1Z" then fap_cal=2;
	if fap_3="A2Z" then fap_cal=3;
	if fap_3="A3Z" then fap_cal=4;
	if fap_3="B0Z" then fap_cal=5;
	if fap_3="B1Z" then fap_cal=6;
	if fap_3="B2Z" then fap_cal=7;
	if fap_3="B3Z" then fap_cal=8;
	if fap_3="B4Z" then fap_cal=9;
	if fap_3="B5Z" then fap_cal=10;
	if fap_3="B6Z" then fap_cal=11;
	if fap_3="B7Z" then fap_cal=12;
	if fap_3="C0Z" then fap_cal=13;
	if fap_3="C1Z" then fap_cal=14;
	if fap_3="C2Z" then fap_cal=15;
	if fap_3="D0Z" then fap_cal=16;
	if fap_3="D1Z" then fap_cal=17;
	if fap_3="D2Z" then fap_cal=18;
	if fap_3="D3Z" then fap_cal=19;
	if fap_3="D4Z" then fap_cal=20;
	if fap_3="D6Z" then fap_cal=21;
	if fap_3="E0Z" then fap_cal=22;
	if fap_3="E1Z" then fap_cal=23;
	if fap_3="E2Z" then fap_cal=24;
	if fap_3="F0Z" then fap_cal=25;
	if fap_3="F1Z" then fap_cal=26;
	if fap_3="F2Z" then fap_cal=27;
	if fap_3="F3Z" then fap_cal=28;
	if fap_3="F4Z" then fap_cal=29;
	if fap_3="F5Z" then fap_cal=30;
	if fap_3="G0A" then fap_cal=31;
	if fap_3="G0B" then fap_cal=32;
	if fap_3="G1Z" then fap_cal=33;
	if fap_3="H0Z" then fap_cal=34;
	if fap_3="J0Z" then fap_cal=35;
	if fap_3="J1Z" then fap_cal=36;
	if fap_3="J3Z" then fap_cal=37;
	if fap_3="J4Z" then fap_cal=38;
	if fap_3="J5Z" then fap_cal=39;
	if fap_3="J6Z" then fap_cal=40;
	if fap_3="K0Z" then fap_cal=41;
	if fap_3="L0Z" then fap_cal=42;
	if fap_3="L1Z" then fap_cal=43;
	if fap_3="L2Z" then fap_cal=44;
	if fap_3="L3Z" then fap_cal=45;
	if fap_3="L4Z" then fap_cal=46;
	if fap_3="L5Z" then fap_cal=47;
	if fap_3="L6Z" then fap_cal=48;
	if fap_3="M0Z" then fap_cal=49;
	if fap_3="M1Z" then fap_cal=50;
	if fap_3="M2Z" then fap_cal=51;
	if fap_3="N0Z" then fap_cal=52;
	if fap_3="P0Z" then fap_cal=53;
	if fap_3="P1Z" then fap_cal=54;
	if fap_3="P2Z" then fap_cal=55;
	if fap_3="P3Z" then fap_cal=56;
	if fap_3="P4Z" then fap_cal=57;
	if fap_3="Q0Z" then fap_cal=58;
	if fap_3="Q1Z" then fap_cal=59;
	if fap_3="Q2Z" then fap_cal=60;
	if fap_3="R0Z" then fap_cal=61;
	if fap_3="R1Z" then fap_cal=62;
	if fap_3="R2Z" then fap_cal=63;
	if fap_3="R3Z" then fap_cal=64;
	if fap_3="R4Z" then fap_cal=65;
	if fap_3="S0Z" then fap_cal=66;
	if fap_3="S1Z" then fap_cal=67;
	if fap_3="S2Z" then fap_cal=68;
	if fap_3="S3Z" then fap_cal=69;
	if fap_3="T0Z" then fap_cal=70;
	if fap_3="T1Z" then fap_cal=71;

	%if %eval(&an.)<1994 %then %do; 
	if fap_3="T2Z" then fap_cal=72;
	if fap_3="T3Z" then fap_cal=73;
	if fap_3="T4Z" then fap_cal=74;
	if fap_3="T6Z" then fap_cal=75;
	if fap_3="U0Z" then fap_cal=76;
	if fap_3="U1Z" then fap_cal=77;
	if fap_3="V0Z" then fap_cal=78;
	if fap_3="V1Z" then fap_cal=79;
	if fap_3="V2Z" then fap_cal=80;
	if fap_3="V3Z" then fap_cal=81;
	if fap_3="V4Z" then fap_cal=82;
	if fap_3="V5Z" then fap_cal=83;
	if fap_3="W0Z" then fap_cal=84;
	if fap_3="W1Z" then fap_cal=85;
	if fap_3="X0Z" then fap_cal=86;
	if fap_3="ZZZ" then fap_cal=87;
	%end;
	%else %do;
	if fap_3="T2A" then fap_cal=72;
	if fap_3="T2B" then fap_cal=73;
	if fap_3="T3Z" then fap_cal=74;
	if fap_3="T4Z" then fap_cal=75;
	if fap_3="T6Z" then fap_cal=76;
	if fap_3="U0Z" then fap_cal=77;
	if fap_3="U1Z" then fap_cal=78;
	if fap_3="V0Z" then fap_cal=79;
	if fap_3="V1Z" then fap_cal=80;
	if fap_3="V2Z" then fap_cal=81;
	if fap_3="V3Z" then fap_cal=82;
	if fap_3="V4Z" then fap_cal=83;
	if fap_3="V5Z" then fap_cal=84;
	if fap_3="W0Z" then fap_cal=85;
	if fap_3="W1Z" then fap_cal=86;
	if fap_3="X0Z" then fap_cal=87;
	if fap_3="ZZZ" then fap_cal=88;
	%end; 

	%if %eval(&an.)<2003 %then sexe_cal=s;
	%else sexe_cal=sexe;;

	%if %eval(&an.)<2003 %then ag3_cal=(ag3="15")+2*(ag3="25")+3*(ag3="50");
	%else ag3_cal=(age3b="15")+2*(age3b="25")+3*(age3b="50");;

	if sexe_cal=1 and ag3_cal=1 then sexe_ag3_cal=1;
	if sexe_cal=1 and ag3_cal=2 then sexe_ag3_cal=2;
	if sexe_cal=1 and ag3_cal=3 then sexe_ag3_cal=3;
	if sexe_cal=2 and ag3_cal=1 then sexe_ag3_cal=4;
	if sexe_cal=2 and ag3_cal=2 then sexe_ag3_cal=5;
	if sexe_cal=2 and ag3_cal=3 then sexe_ag3_cal=6;

	if cs_cal=1 and sexe_ag3_cal=1 then cs_sexe_ag3_cal=1 ;
	if cs_cal=1 and sexe_ag3_cal=2 then cs_sexe_ag3_cal=2 ;
	if cs_cal=1 and sexe_ag3_cal=3 then cs_sexe_ag3_cal=3 ;
	if cs_cal=1 and sexe_ag3_cal=4 then cs_sexe_ag3_cal=4 ;
	if cs_cal=1 and sexe_ag3_cal=5 then cs_sexe_ag3_cal=5 ;
	if cs_cal=1 and sexe_ag3_cal=6 then cs_sexe_ag3_cal=6 ;
	if cs_cal=2 and sexe_ag3_cal=1 then cs_sexe_ag3_cal=7 ;
	if cs_cal=2 and sexe_ag3_cal=2 then cs_sexe_ag3_cal=8 ;
	if cs_cal=2 and sexe_ag3_cal=3 then cs_sexe_ag3_cal=9 ;
	if cs_cal=2 and sexe_ag3_cal=4 then cs_sexe_ag3_cal=10 ;
	if cs_cal=2 and sexe_ag3_cal=5 then cs_sexe_ag3_cal=11 ;
	if cs_cal=2 and sexe_ag3_cal=6 then cs_sexe_ag3_cal=12 ;
	if cs_cal=3 and sexe_ag3_cal=1 then cs_sexe_ag3_cal=13 ;
	if cs_cal=3 and sexe_ag3_cal=2 then cs_sexe_ag3_cal=14 ;
	if cs_cal=3 and sexe_ag3_cal=3 then cs_sexe_ag3_cal=15 ;
	if cs_cal=3 and sexe_ag3_cal=4 then cs_sexe_ag3_cal=16 ;
	if cs_cal=3 and sexe_ag3_cal=5 then cs_sexe_ag3_cal=17 ;
	if cs_cal=3 and sexe_ag3_cal=6 then cs_sexe_ag3_cal=18 ;
	if cs_cal=4 and sexe_ag3_cal=1 then cs_sexe_ag3_cal=19 ;
	if cs_cal=4 and sexe_ag3_cal=2 then cs_sexe_ag3_cal=20 ;
	if cs_cal=4 and sexe_ag3_cal=3 then cs_sexe_ag3_cal=21 ;
	if cs_cal=4 and sexe_ag3_cal=4 then cs_sexe_ag3_cal=22 ;
	if cs_cal=4 and sexe_ag3_cal=5 then cs_sexe_ag3_cal=23 ;
	if cs_cal=4 and sexe_ag3_cal=6 then cs_sexe_ag3_cal=24 ;
	if cs_cal=5 and sexe_ag3_cal=1 then cs_sexe_ag3_cal=25 ;
	if cs_cal=5 and sexe_ag3_cal=2 then cs_sexe_ag3_cal=26 ;
	if cs_cal=5 and sexe_ag3_cal=3 then cs_sexe_ag3_cal=27 ;
	if cs_cal=5 and sexe_ag3_cal=4 then cs_sexe_ag3_cal=28 ;
	if cs_cal=5 and sexe_ag3_cal=5 then cs_sexe_ag3_cal=29 ;
	if cs_cal=5 and sexe_ag3_cal=6 then cs_sexe_ag3_cal=30 ;
	if cs_cal=6 and sexe_ag3_cal=1 then cs_sexe_ag3_cal=31 ;
	if cs_cal=6 and sexe_ag3_cal=2 then cs_sexe_ag3_cal=32 ;
	if cs_cal=6 and sexe_ag3_cal=3 then cs_sexe_ag3_cal=33 ;
	if cs_cal=6 and sexe_ag3_cal=4 then cs_sexe_ag3_cal=34 ;
	if cs_cal=6 and sexe_ag3_cal=5 then cs_sexe_ag3_cal=35 ;
	if cs_cal=6 and sexe_ag3_cal=6 then cs_sexe_ag3_cal=36 ;
	if cs_cal=7 and sexe_ag3_cal=1 then cs_sexe_ag3_cal=37 ;
	if cs_cal=7 and sexe_ag3_cal=2 then cs_sexe_ag3_cal=38 ;
	if cs_cal=7 and sexe_ag3_cal=3 then cs_sexe_ag3_cal=39 ;
	if cs_cal=7 and sexe_ag3_cal=4 then cs_sexe_ag3_cal=40 ;
	if cs_cal=7 and sexe_ag3_cal=5 then cs_sexe_ag3_cal=41 ;
	if cs_cal=7 and sexe_ag3_cal=6 then cs_sexe_ag3_cal=42 ;
	if cs_cal=8 and sexe_ag3_cal=1 then cs_sexe_ag3_cal=43 ;
	if cs_cal=8 and sexe_ag3_cal=2 then cs_sexe_ag3_cal=44 ;
	if cs_cal=8 and sexe_ag3_cal=3 then cs_sexe_ag3_cal=45 ;
	if cs_cal=8 and sexe_ag3_cal=4 then cs_sexe_ag3_cal=46 ;
	if cs_cal=8 and sexe_ag3_cal=5 then cs_sexe_ag3_cal=47 ;
	if cs_cal=8 and sexe_ag3_cal=6 then cs_sexe_ag3_cal=48 ;
	if cs_cal=9 				   then cs_sexe_ag3_cal=49 ;

	/* if cs_cal=9 and sexe_ag3_cal=1 then cs_sexe_ag3_cal=49 ;
	if cs_cal=9 and sexe_ag3_cal in (2,3) then cs_sexe_ag3_cal=50 ;
	if cs_cal=9 and sexe_ag3_cal=4 then cs_sexe_ag3_cal=51 ;
	if cs_cal=9 and sexe_ag3_cal=5 then cs_sexe_ag3_cal=52 ;
	if cs_cal=9 and sexe_ag3_cal=6 then cs_sexe_ag3_cal=53 ; */

	un=1;
	fap_1=substr(fap_3,1,1);

run;
%end;
%mend;

%codage_FAP2009;

proc freq data=a2020;
table fap_5;
run;

/*** Sortie des s�ries par CS, par DP et par FAP. */
%macro series;
%do annee=1982 %to 2020;
	proc summary data=a&annee. nway missing;
	class fap_1 fap_3 cs;
	var poids;
	output out=serie&annee.(drop=_FREQ_ _TYPE_) sum=_&annee.;
	run;
	/* serie&annee. = tables contenant les effectifs par fap*cs de chaque ann�e */
%end;

data serie;
merge %do annee=1982 %to 2020; serie&annee. %end;;
by fap_1 fap_3 cs;
run;
/* serie = table contenant les s�ries pas DP*FAP*CS */

proc summary data=serie nway missing;
class FAP_1;
var _1982-_2020;
output out=series_dp(drop=_FREQ_ _TYPE_) sum=;
run;
/* series_dp = table contenant les s�ries pas DP */

proc summary data=serie nway missing;
class FAP_3;
var _1982-_2020;
output out=series_fap(drop=_FREQ_ _TYPE_) sum=;
run;
/* series_fap = table contenant les s�ries pas FAP */

proc summary data=serie nway missing;
class cs;
var _1982-_2020;
output out=series_cs(drop=_FREQ_ _TYPE_) sum=;
run;
/* series_cs = table contenant les s�ries pas CS */

%do annee=1982 %to 2020;
proc delete data=serie&annee.; run;
%end;

%mend;

%series;

proc export data=series_dp 
		 	outfile="C:\Users\guillaume.toure\Desktop\DAMETH_TOURE\PSM\preparation_donnees\Series longues 2021\Methodo\X_Series a chaque etape\Sorties SAS\1_Recodage_FAP_series par DP.xls" 
			DBMS=tab REPLACE; 
run;
proc export data=series_fap 
		 	outfile="C:\Users\guillaume.toure\Desktop\DAMETH_TOURE\PSM\preparation_donnees\Series longues 2021\Methodo\X_Series a chaque etape\Sorties SAS\1_Recodage_FAP_series par FAP.xls" 
			DBMS=tab REPLACE; 
run;
proc export data=series_cs 
		 	outfile="C:\Users\guillaume.toure\Desktop\DAMETH_TOURE\PSM\preparation_donnees\Series longues 2021\Methodo\X_Series a chaque etape\Sorties SAS\1_Recodage_FAP_series par CS.xls" 
			DBMS=tab REPLACE; 
run;



/*** V�rifications. */
%macro verif_etape_1(var);

%do annee=1982 %to 2020;
proc summary data=a&annee. nway missing;
where FAP_3 in ("S1Z","S2Z");
class &var.;
var poids;
output out=serie&annee.(drop=_FREQ_ _TYPE_) sum=_&annee.;
run;
%end;

data serie;
merge %do annee=1982 %to 2020; serie&annee. %end;;
by &var.;
run;

proc print data=serie;
run;

%mend;

%verif_etape_1(FAP_3);
%verif_etape_1(cs_cal sexe_ag3_cal);
%verif_etape_1(cs_cal);
%verif_etape_1(cstot);
%verif_etape_1(sexe_ag3_cal);
%verif_etape_1(cs_2);
%verif_etape_1(FAP_3 p);
