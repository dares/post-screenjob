%include "C:\Users\guillaume.toure\Desktop\DAMETH_TOURE\PSM\preparation_donnees\PSM 2021\Programmes\macros portraits.sas";

%let deb=1982;
%let fin=2020;/*2014*/
%let nom=g5;
%let nom_rep=Graphique_5 Dipl�me -30;

libname eec "C:\Users\guillaume.toure\Desktop\DAMETH_TOURE\PSM\preparation_donnees\Series longues 2021\Bases SAS\s�ries_longues";

%let chemin=C:\Users\guillaume.toure\Desktop\DAMETH_TOURE\PSM\preparation_donnees\PSM 2021\Sorties\&nom_rep;
libname libout "&chemin";


%macro prep_dipl(lib);

%do an=&deb %to &fin;
	data sl_dmq_&an;
		set &lib..sl_dmq_&an. ;
		%if &an<1990 %then %do;
			keep  idt_aire im_loc noi fap_3 poids_final age_enq
			naim rep_age_enq sexe_cal rep_age_enq_3 rep_dip;
		%end;
		%if &an>=1990 and &an<2003 %then %do;
			keep  aire imloc noi fap_3 poids_final age_enq var seuil
			jj ag_num naim mm rep_age_enq sexe_cal rep_age_enq_3 rep_dip;
		%end;
		%if &an>=2003 %then %do;
			keep  ident noi fap_3 poids_final annee age age_enq
			rep_age_enq sexe_cal rep_age_enq_3 forter rep_dip;
		%end;

		%if  &an.<1990 or &an.>2002 %then %do;
			age_enq=age;
		%end;
		%else %do;
			var=ranuni(-1);
			seuil=JJ/31;
			ag_num=input(ag,3.);
			age_enq=ag-(NAIM>MM)-(NAIM=MM)*(var>seuil);
		%end;
		select ;
		when (age_enq<25) rep_age_enq='15';
		when (age_enq<30) rep_age_enq='25';
		when (age_enq<35) rep_age_enq='30';
		when (age_enq<40) rep_age_enq='35';
		when (age_enq<45) rep_age_enq='40';
		when (age_enq<50) rep_age_enq='45';
		when (age_enq<55) rep_age_enq='50';
		when (age_enq<60) rep_age_enq='55';
		when (age_enq>=60) rep_age_enq='60';
		otherwise rep_age_enq='XX';
		end;

		select ;
		when (age_enq<30) rep_age_enq_3='3-';
		when (age_enq<50) rep_age_enq_3='5-';
		when (age_enq>=50) rep_age_enq_3='5+';
		otherwise rep_age_enq_3='XX';
		end;

		if %if  &an.<1990 %then  %do; (et="1" and foact ne "5") %end; 
		 %else %if &an.<2003 %then %do; (et="1" and init="2") %end;
		 %if &an.>=2003 %then %do; forter="2" %end; 
		then rep_dip="EC";
		else rep_dip=DDIPL;

		if rep_dip='' then rep_dip="7";
		if rep_dip="6" then rep_dip="7";
		if rep_age_enq<'30';

	run;
%end;
%mend;
options mprint mlogic symbolgen;
%prep_dipl(eec);

%calculs(chemin_res=&chemin,nom=&nom,an_deb=&deb,an_fin=&fin,nom_table_prep=sl_dmq,
	lib_donnees=work,lib_sortie=libout,var=rep_dip,var_ventil=,var_pond=poids_final,
	seuil_diffusion=100);

%prep_fichier(an_deb=&deb,an_fin=&fin,chemin=&chemin,
	nom_tab=libout.res&nom._&deb._&fin,nom_tab_diff=libout.resdiff&nom._&deb._&fin,
	nom_graph=&nom_rep,var=rep_dip,var_ventil=);

%prep_graph_taille_ech(an_deb=&deb,an_fin=&fin,chemin=&chemin,
	nom_tab=libout.taille_ech&nom._&deb._&fin,nom_graph=Graphique taille ech moins 30,
	titre="Taille de l'�chantillon, moins de 30 ans (Graphique 5)",var_ventil=);








