libname lib "C:\Users\guillaume.toure\Desktop\DAMETH_TOURE\PSM\preparation_donnees\Series longues 2021\Bases SAS"; /*"d:\utilisateurs\charline.babet\Donnees\Series longues\Series longues 2015\Bases SAS";*/

libname calmar "C:\Users\guillaume.toure\Desktop\DAMETH_TOURE\PSM\preparation_donnees\macro_calmar_insee";
options sasmstore=calmar mstored nodate;

proc import replace datafile="C:\Users\guillaume.toure\Desktop\DAMETH_TOURE\PSM\preparation_donnees\Series longues 2021\Programmes et sorties SAS 2021\2_Marges.xls"
dbms="xls"
out=marges;
run;

/*** Calage sur les s�ries de l'Insee. */
%macro calage_CS_SEXE_AGE;

%do annee=1982 %to 2002;

	data marges&annee.;
	set marges (where=(ANNEE="&annee." and VAR="cs_sexe_ag3_cal"));
	drop ANNEE;
	run;

	%calmar(
	data = a&annee., /* table en entr�e */
	contpoi=NON, /*  ne pas afficher le contenu de la table des poids */
	poids = poids, /* variable de pond�ration initiale */
	ident = ID, /* identifiant des observations */
	datamar = marges&annee., /* table des marges */
	pct = NON, /* les marges ne sont pas donn�es en % */
	m = 2, /* m�thode : 1 = lin�aire, 2 = raking ratio, 3 = logit, 4 = lin�aire tronqu�e */
	up=, /*  borne sup�rieure des rapports de poids (si M=3 ou 4) */ 
    lo=, /*  borne inf�rieure des rapports de poids (si M=3 ou 4) */
	editpoi=,/*  �dition de tous les poids finaux */
	datapoi = a&annee._cal, /* table contenant la pond�ration finale */
	poidsfin = poids_cal, /*  variable de pond�ration finale */
	stat = oui, /* �diter des statistiques (moyenne, �cart-type, quantiles, valeurs extr�mes...) et des graphiques relatifs aux distributions des variables "rapport de poids" et "pond�ration finale" */
	obseli = oui, /* sortie d'une table __OBSELI contenant les observations �limin�es, les variables du calage, les variables de pond�ration et la variable &IDENT */
	maxiter = 15, /* nombre maximum d'it�rations au cours de l'algorithme de Newton */
	seuil = 0.0001 /* seuil e pour le test d'arr�t de l'algorithme de Newton */
	);

	data calage&annee.;
	merge a&annee.  a&annee._cal;
	by ID;
	run;
	/*  a&annee = table EE 
		a&annee._cal = table contenant uniquement les id et les poids cal�s 
		calage&annee. = table EE avec les poids cal�s */

%end;

%do annee=2003 %to 2020;
	data calage&annee.;
	set a&annee.;
	run;
	/* A partir de 2003, on prend les tables de l'EEC "brutes". */
%end;

%mend;

%calage_CS_SEXE_AGE;

/*** Sortie des s�ries par FAP, CS et DP. */
%macro series;

%do annee=1982 %to 2014;
	proc summary data=calage&annee. nway missing;
	class fap_1 fap_3 cs;
	var poids_cal;
	output out=serie&annee.(drop=_FREQ_ _TYPE_) sum=_&annee.;
	run;
	/* serie&annee. = tables contenant les effectifs par fap*cs de chaque ann�e */
%end;

data serie;
merge %do annee=1982 %to 2014; serie&annee. %end;;
by fap_1 FAP_3 cs;
run;
/* serie = table contenant les s�ries pas FAP*CS */

proc summary data=serie nway missing;
class FAP_3;
var _1982-_2014;
output out=series_fap(drop=_FREQ_ _TYPE_) sum=;
run;
/* series_fap = table contenant les s�ries pas FAP */

proc summary data=serie nway missing;
class cs;
var _1982-_2014;
output out=series_cs(drop=_FREQ_ _TYPE_) sum=;
run;
/* series_cs = table contenant les s�ries pas CS */

proc summary data=serie nway missing;
class FAP_1;
var _1982-_2014;
output out=series_dp(drop=_FREQ_ _TYPE_) sum=;
run;
/* series_dp = table contenant les s�ries pas DP */

%mend;

%series;

proc export data=series_fap 
		 	outfile="C:\Users\guillaume.toure\Desktop\DAMETH_TOURE\PSM\preparation_donnees\Series longues 2021\Methodo\X_Series a chaque etape\Sorties SAS\2_Calage_series par FAP.xls" 
			DBMS=tab REPLACE; 
run;
proc export data=series_cs 
		 	outfile="C:\Users\guillaume.toure\Desktop\DAMETH_TOURE\PSM\preparation_donnees\Series longues 2021\Methodo\X_Series a chaque etape\Sorties SAS\2_Calage_series par CS.xls" 
			DBMS=tab REPLACE; 
run;
proc export data=series_dp 
		 	outfile="C:\Users\guillaume.toure\Desktop\DAMETH_TOURE\PSM\preparation_donnees\Series longues 2021\Methodo\X_Series a chaque etape\Sorties SAS\2_Calage_series par DP.xls" 
			DBMS=tab REPLACE; 
run;


/*** V�rifications. */
%macro verif_etape_2(var);

%do annee=1982 %to 2009;
proc summary data=calage&annee. nway missing;
class &var.;
var poids_cal;
output out=serie&annee.(drop=_FREQ_ _TYPE_) sum=_&annee.;
run;
%end;

data serie;
merge %do annee=1982 %to 2009; serie&annee. %end;;
by &var.;
run;

proc print data=serie;
run;

%mend;

%verif_etape_2(fap_3);
%verif_etape_2(nes36);


/*** V de Cramer */
proc freq data=a2003;
weight poids;
table cs*fap_3/missing CHISQ;
run;








