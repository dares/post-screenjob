# Post-Screenjob

Programmes de préparation des données pour l'application R-shiny Screenjob.

## Description

Les programmes de ce git ont pour objectif de préparer les données pour la datavisualisation développée en Rshiny (Screenjob). Ces programmes mobilisent :
- les anciennes enquêtes Emploi annuelles et leur version rétropolées (SL_DMQ_1982 à SL_DMQ_2014) ;
- les enquêtes Emploi en continue de 2015 à la dernière année disponible ;
- le dernier recensement disponible sur le champ France métropolitaine (fichier complémentaire individu MET).

## Getting started

Il est nécessaire de mettre à jour les libnames décrit dans chaque programme mais ils sont centralisés dans le fichier 0-définir tous les libnames.sas


