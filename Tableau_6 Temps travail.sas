%include "C:\Users\guillaume.toure\Desktop\DAMETH_TOURE\PSM\preparation_donnees\PSM 2021\Programmes\macros portraits.sas";

%let deb=2003;
%let fin=2020;
%let nom=t6;
%let nom_rep=Tableau_6 Temps travail;

libname eec "C:\Users\guillaume.toure\Desktop\DAMETH_TOURE\PSM\preparation_donnees\Series longues 2021\Bases SAS\s�ries_longues";

%let chemin=C:\Users\guillaume.toure\Desktop\DAMETH_TOURE\PSM\preparation_donnees\PSM 2021\Sorties\&nom_rep;
libname libout "&chemin";


%macro prep_temps(lib);

%do an=&deb %to &fin;
	data sl_dmq_&an;
		set &lib..sl_dmq_&an. ;
		%if &an<1990 %then %do;
			keep  idt_aire im_loc noi fap_3 poids_final 
			act7 sousempl du temps_partiel rep_sousempl rep_tps rep_tps_partiel poids_tps;
		%end;
		%if &an>=1990 and &an<2003 %then %do;
			keep  aire imloc noi fap_3 poids_final 
			act7 sousempl du temps_partiel rep_sousempl rep_tps rep_tps_partiel poids_tps;
		%end;
		%if &an>=2003 %then %do;
			keep  ident noi fap_3 poids_final 
			sousempl temps_partiel rep_sousempl rep_tps rep_tps_partiel poids_tps;
		%end;
		%if &an.<1998 %then %do;
			if act7 ne '1' then sousempl='X';
			else if du in ('2','3','4') and (
				(dre1='5' and ( (ner in ('1','2')) or (ner in ('3','4') and creact='8') or (stpl in ('1','2')) or
				(ohtm in ('4','5') and ht<hh and hh ne 'SP') ) )
				or (ult='1' and (nerult='1' or (nerult='2' and creact='8') or stpl in ('1','2') or (ohtm in
				('4','5') and ht<hh and hh ne 'SP')) )
				) then sousempl='1';
			else if du in ('2','3','4') and disppl='1' and (stpl in ('1','2') or (ohtm in ('4','5') and ht<hh
				and hh ne 'SP') ) then sousempl='2';
			else if (du not in ('2','3','4') and ohtm in ('4','5') and HT<HH and hh ne 'SP') then
				sousempl='3';
			else sousempl='X';
		%end;
		%else %do;
			if sousempl='' then sousempl='X';
		%end;

		%if &an.>2002 %then %do;
			temps_partiel=tppred;
		%end;
		%else %do;
			if du in ('2','3','4') then temps_partiel='2';
			else if du='1' then temps_partiel='1';
		%end;

		if sousempl in ('1','2','3') then rep_sousempl="1";
		else rep_sousempl="0";
		if temps_partiel='1' and hhc6='5' then rep_tps="1";
		else rep_tps="0";
		if temps_partiel ne '1' then rep_tps="";
		if temps_partiel='2' then rep_tps_partiel="1";
		else rep_tps_partiel="0";

		poids_tps=poids_final;
		if temps_partiel ne '1' then poids_tps=0;
		
	run;
%end;
%mend;

%prep_temps(eec);

/* Pour ce graphique, on utilise plusieurs variables -> on lance plusieurs fois la macro
calculs et on renomme les tables en sortie. */
%macro renomm(nom,suff);
	PROC DATASETS LIBRARY = libout ;
	 CHANGE res&nom._&deb._&fin=res&nom.&suff._&deb._&fin ;  
	 CHANGE resdiff&nom._&deb._&fin=resdiff&nom.&suff._&deb._&fin ; 
	 CHANGE resNR&nom._&deb._&fin=resNR&nom.&suff._&deb._&fin ; 
	 *CHANGE Poids&nom._1an_&deb._&fin=Poids&nom.&suff._1an_&deb._&fin ; 
	 *CHANGE Res&nom._1an_&deb._&fin=Res&nom.&suff._1an_&deb._&fin ;
	 CHANGE Taille_ech&nom._&deb._&fin=Taille_ech&nom.&suff._&deb._&fin ;
	 *CHANGE Taille_ech&nom._1an_&deb._&fin=Taille_ech&nom.&suff._1an_&deb._&fin ;
	  QUIT;   RUN  ;
	data libout.res&nom.&suff._&deb._&fin;
		set libout.res&nom.&suff._&deb._&fin;
		rename %do an=%eval(&deb+1) %to %eval(&fin-1); eff%eval(&an-1)_%eval(&an+1)=eff&suff%eval(&an-1)_%eval(&an+1)
		part%eval(&an-1)_%eval(&an+1)=part&suff%eval(&an-1)_%eval(&an+1) %end;;
	run;
%mend;

%calculs(chemin_res=&chemin,nom=&nom,an_deb=&deb,an_fin=&fin,nom_table_prep=sl_dmq,
	lib_donnees=work,lib_sortie=libout,var=rep_sousempl,var_ventil=,var_pond=poids_final,
	seuil_diffusion=100);
%renomm(t6,ss);

%calculs(chemin_res=&chemin,nom=&nom,an_deb=&deb,an_fin=&fin,nom_table_prep=sl_dmq,
	lib_donnees=work,lib_sortie=libout,var=rep_tps,var_ventil=,var_pond=poids_tps,
	seuil_diffusion=100);
%renomm(t6,tps);

%calculs(chemin_res=&chemin,nom=&nom,an_deb=&deb,an_fin=&fin,nom_table_prep=sl_dmq,
	lib_donnees=work,lib_sortie=libout,var=rep_tps_partiel,var_ventil=,var_pond=poids_final,
	seuil_diffusion=100);
%renomm(t6,tp);

data libout.rest6_&deb._&fin;
	merge libout.rest6ss_&deb._&fin libout.rest6tps_&deb._&fin libout.rest6tp_&deb._&fin;
	by fap_3;
run;

data libout.resdifft6_&deb._&fin;
	merge libout.resdifft6ss_&deb._&fin libout.resdifft6tps_&deb._&fin 
		  libout.resdifft6tp_&deb._&fin;
	by fap_3;
run;

%macro prep_fichier_tps(an_deb,an_fin,chemin,/*nom_tab_an,*/nom_tab_3ans,
						nom_graph,liste_var,liste_suff);
	%let i=1;
	%do %while(%length(%scan(&liste_var,&i))>0);
		%let var&i=%scan(&liste_var,&i);
		%let i=%eval(&i+1);
	%end;
	%let nbvar=%eval(&i-1);
	%let i=1;
	%do %while(%length(%scan(&liste_suff,&i))>0);
		%let suff&i=%scan(&liste_suff,&i);
		%let i=%eval(&i+1);
	%end;
	%let nbsuff=%eval(&i-1);

	%let chemin_psm=C:\Users\christophe.michel\Documents\PSM\PSM 2020\Mod�les;
	options missing="";
	options NOXWAIT NOXSYNC ;
	x copy "&chemin_psm.\Modle &nom_graph..xls" "&chemin.\&nom_graph..xls";
	options NOXWAIT NOXSYNC ; *** Open Excel ***; 
	x "start excel"; 
	data _null_ ; 
		x = SLEEP(4) ; 
	run ; 
	filename cmds DDE 'excel|system' ;
	data _null_ ;
		file cmds ;
		put '[FILE-OPEN("'"&chemin.\&nom_graph..xls"'")]' ;
	run ;
	%let plage1=L1C1:L1C40;
	%let plage2=L2C1:L1000C40;
	%do i=1 %to &nbvar;
		/* filename t dde "EXCEL|&chemin.\[&nom_graph..xls]eff&&suff&i.._an!&plage1" notab;
		data _null_;
			file t lrecl=500;
			set &nom_tab_an;
			sp="09"X;
			put "Code" sp "Fap3" sp %if "&&var&i" ne "" %then %do; "&&var&i" sp %end;
			"&an_deb"  %do an=%eval(&an_deb+1) %to &an_fin; sp "&an"  %end;
			;		
		run;
		 filename t dde "EXCEL|&chemin.\[&nom_graph..xls]eff&&suff&i.._an!&plage2" notab;
		data _null_;
			file t lrecl=500;
			set &nom_tab_an;
			sp="09"X;
			put code $5. sp fap_3 $3. sp %if "&&var&i" ne "" %then %do; &&var&i $6. sp %end;
			eff&&suff&i..&an_deb commax6.2 %do an=%eval(&an_deb+1) %to &an_fin; sp eff&&suff&i..&an commax6.2 %end;
			;		
		run; */
		filename t dde "EXCEL|&chemin.\[&nom_graph..xls]eff&&suff&i.._3ans!&plage1" notab;
		data _null_;
			file t lrecl=500;
			set &nom_tab_3ans;
			sp="09"X;
			put "Code" sp "Fap3" sp %if "&&var&i" ne "" %then %do; "&&var&i" sp %end;
			"%eval(&an_deb)-%eval(&an_deb+2)"  %do an=%eval(&an_deb+2) %to %eval(&an_fin-1); sp "%eval(&an-1)-%eval(&an+1)"  %end;
			;		
		run;
		 filename t dde "EXCEL|&chemin.\[&nom_graph..xls]eff&&suff&i.._3ans!&plage2" notab;
		data _null_;
			file t lrecl=500;
			set &nom_tab_3ans;
			sp="09"X;
			put code $5. sp fap_3 $3. sp %if "&&var&i" ne "" %then %do; &&var&i $6. sp %end;
			eff&&suff&i%eval(&an_deb)_%eval(&an_deb+2) commax6.2 %do an=%eval(&an_deb+2) %to %eval(&an_fin-1); sp eff&&suff&i%eval(&an-1)_%eval(&an+1) commax6.2 %end;
			;		
		run;
		%if &&var&i ne %then %do;
			/* filename t dde "EXCEL|&chemin.\[&nom_graph..xls]part&&suff&i.._an!&plage1" notab;
			data _null_;
				file t lrecl=500;
				set &nom_tab_an;
				sp="09"X;
				put "Code" sp "Fap3" sp %if "&&var&i" ne "" %then %do; "&&var&i" sp %end;
				"&an_deb"  %do an=%eval(&an_deb+1) %to &an_fin; sp "&an"  %end;
				;		
			run;
			 filename t dde "EXCEL|&chemin.\[&nom_graph..xls]part&&suff&i.._an!&plage2" notab;
			data _null_;
				file t lrecl=500;
				set &nom_tab_an;
				sp="09"X;
				put code $5. sp fap_3 $3. sp %if "&&var&i" ne "" %then %do; &&var&i $6. sp %end;
				part&&suff&i..&an_deb commax6.2 %do an=%eval(&an_deb+1) %to &an_fin; sp part&&suff&i..&an commax6.2 %end;
				;		
			run; */
			filename t dde "EXCEL|&chemin.\[&nom_graph..xls]part&&suff&i.._3ans!&plage1" notab;
			data _null_;
				file t lrecl=500;
				set &nom_tab_3ans;
				sp="09"X;
				put "Code" sp "Fap3" sp %if "&&var&i" ne "" %then %do; "&&var&i" sp %end;
				"%eval(&an_deb)-%eval(&an_deb+2)"  %do an=%eval(&an_deb+2) %to %eval(&an_fin-1); sp "%eval(&an-1)-%eval(&an+1)"  %end;
				;		
			run;
			 filename t dde "EXCEL|&chemin.\[&nom_graph..xls]part&&suff&i.._3ans!&plage2" notab;
			data _null_;
				file t lrecl=500;
				set &nom_tab_3ans;
				sp="09"X;
				put code $5. sp fap_3 $3. sp %if "&&var&i" ne "" %then %do; &&var&i $6. sp %end;
				part&&suff&i%eval(&an_deb)_%eval(&an_deb+2) commax6.2 %do an=%eval(&an_deb+2) %to %eval(&an_fin-1); sp part&&suff&i%eval(&an-1)_%eval(&an+1) commax6.2 %end;
				;		
			run;
		%end;
	%end;
	options missing=".";
	/*sauvegarde du fichier Excel*/
	FILENAME cmds DDE 'Excel|system';
	DATA _NULL_;
		FILE cmds;
		PUT '[save()]';
	RUN;
	/*fermeture du fichier*/
	data _null_ ;
		file cmds ;
		put "[FILE-CLOSE()]" ;
	run ;
	/*fermer excel*/
	data _null_ ;
		file cmds ;
		put "[QUIT()]" ;
	run ;

%mend;
%prep_fichier_tps(&deb,&fin,&chemin,/*libout.res&nom._1an_&deb._&fin,*/
	libout.res&nom._&deb._&fin,&nom_rep,rep_sousempl rep_tps rep_tps_partiel,ss tps tp);

%prep_graph_taille_ech(an_deb=&deb,an_fin=&fin,chemin=&chemin,
	nom_tab=libout.taille_ech&nom.tps_&deb._&fin,nom_graph=Graphique taille ech tps complet,
	titre="Taille de l'�chantillon, tps complet (Tableau 6)",var_ventil= )



