%include "d:\documents-utilisateurs\charline.babet\Donnees\PSM\PSM 2015\Programmes\macros portraits.sas";
%include "d:\documents-utilisateurs\charline.babet\Donnees\Formats\Formats FAP.sas";

%let deb=2005;
%let fin=2014;
%let nom=g10;
%let nom_rep=Graphique_10 MMO;

libname eec "d:\documents-utilisateurs\charline.babet\Donnees\Series longues\Series longues 2015\Bases SAS";

%let chemin=d:\documents-utilisateurs\charline.babet\Donnees\PSM\PSM 2015\Sorties\&nom_rep;
libname libout "&chemin";

*options mprint mlogic symbolgen;

%macro prep_mmo(lib);

%do an=&deb %to &fin;
	data sl_dmq_&an;
		set &lib..sl_dmq_&an. ;
			keep  ident noi fap_3 poids_final annee acteu chpub statut;

			/* Champ MMO : salaris du secteur priv hors interim. */
			if acteu='1' and stat2="2" /* On ne garde que les salaris. */ 
			and 
				/* On supprime Etat, collectivits, hpitaux publics, particuliers, et nature de l'employeur inconnue. 
				Les modalits ont chang en 2013. */
				%if &an<=2012 %then %do; chpub in ('5','6') %end;
				%if &an>=2013 %then %do; chpub in ('1','2') %end;

			and statut in ('22','33','34','35')
				 /* statut in ('22','33','34','35') : 
					apprentis, CDD (hors Etat, collectivits),
					stagiaires et contrats aids (hors Etat, collectivits),
					autres contrats (hors Etat,collectivits) */ ;
	run;
%end;
%mend;
%prep_mmo(eec)

%calculs(chemin_res=&chemin,nom=&nom,an_deb=&deb,an_fin=&fin,nom_table_prep=sl_dmq,
	lib_donnees=work,lib_sortie=libout,var=,var_ventil=,var_pond=poids_final,
	seuil_diffusion=100)

%prep_graph_taille_ech(an_deb=&deb,an_fin=&fin,chemin=&chemin,
	nom_tab=libout.taille_ech&nom._&deb._&fin,
	nom_graph=Graphique taille ech salaris champ MMO,
	titre="Taille de l'chantillon, champ MMO : salaris du priv hors intrim (Graphique 10)", 
	var_ventil=)


libname mmo "&chemin";

%macro donnees_MMO(an_deb,an_fin);
	%do an=&an_deb %to &an_fin;
		proc import datafile="F:\Sources\MMO\mmo191115.xls"
			out=mmo.mmo&an dbms=excel2000 replace;
			sheet="&an";
		run;
		proc summary data=mmo.mmo&an noprint;
			var pds;
			class typees;
			output out=stat&an sum=total_&an;
		run;
		data mmo&an;
			set mmo.mmo&an;
			attrib fap format=$5. fap_3 format=$3.;
				fap=put(lowcase(PCS),$pcse_fap.);/* format pour faire la correspondance PCS-ESE -> FAP */
				fap_3=substr(fap,1,3);
			/*  AU	Autre sortie
				CO	Rupture conventionnelle
				DM	Dmission
				EI	Entre inconnue
				ES	Fin de priode essai
				FD	Fin CDD
				FE	Fin CNE initiative employeur
				FN	Fin CNE initiative salari
				LA	Licenciement autre
				LE	Licenciement conomique
				OF	Fin CDD  objet dfini
				OR	Recrutement en CDD  objet dfini
				PR	Dpart en prretraite
				RD	Recrutement CDD
				RI	Recrutement CDI
				RN	Recrutement CNE
				RT	Retraite
				SI	Sortie inconnue
				SN	Service national
				TE	Transfert entre 
				TS	Transfert sortie
			Transferts : transferts entre tablissements d'une mme entreprise
			*/
		run;

		data entrees&an;
			set mmo&an(where=(typees in("RD","OR","RI","RN","EI")));
		run;
		data sorties&an;
			set mmo&an(where=(typees in("FD","OF","DM","RT","CO","ES","LA","LE","PR","SI","AU","FE","FN")));
		run;

		proc summary data=entrees&an noprint;
			var pds;
			class fap_3;
			output out=entrees_mmo&an sum=entrees_&an;
		run;
		data entrees_mmo&an;
			set entrees_mmo&an;
			if fap_3 ="" then fap_3="ENS";
		run;
		proc summary data=sorties&an noprint;
			var pds;
			class fap_3;
			output out=sorties_mmo&an sum=sorties_&an;
		run;
		data sorties_mmo&an;
			set sorties_mmo&an;
			if fap_3 ="" then fap_3="ENS";
		run;
		proc sort data=entrees_mmo&an;by fap_3;run;
		proc sort data=sorties_mmo&an;by fap_3;run;
	%end;
	data res;
		merge %do an=&an_deb %to &an_fin; entrees_mmo&an sorties_mmo&an %end;
		libout.resg10_&deb._&fin;
		by fap_3;
	run;
	data libout.resmmo;
		set res;
		%do an=&an_deb %to &an_fin;
			taux_entree_&an=100*entrees_&an/(1000*eff%eval(&an-2)_&an);
			taux_sortie_&an=100*sorties_&an/(1000*eff%eval(&an-2)_&an);
			taux_rotation_&an=1/2*(taux_entree_&an+taux_sortie_&an);
		%end;
	run;
%mend;
%donnees_MMO(2007,2014);

%macro prep_fichier_mmo(an_deb,an_fin,chemin,nom_tab,nom_graph,liste_var,liste_onglets);
	%let chemin_psm=d:\documents-utilisateurs\charline.babet\Donnees\PSM\PSM 2015\Modles;
	%let i=1;
	%do %while(%length(%scan(&liste_var,&i))>0);
		%let var&i=%scan(&liste_var,&i);
		%let i=%eval(&i+1);
	%end;
	%let nbvar=%eval(&i-1);
	%let i=1;
	%do %while(%length(%scan(&liste_onglets,&i))>0);
		%let onglet&i=%scan(&liste_onglets,&i);
		%let i=%eval(&i+1);
	%end;
	%let nbonglets=%eval(&i-1);

	options missing=".";
	options NOXWAIT NOXSYNC ;
	x copy "&chemin_psm.\Modle &nom_graph..xls" "&chemin.\&nom_graph..xls";
	options NOXWAIT NOXSYNC ; *** Open Excel ***; 
	x "start excel"; 
	data _null_ ; 
		x = SLEEP(4) ; 
	run ; 
	filename cmds DDE 'excel|system' ;
	data _null_ ;
		file cmds ;
		put '[FILE-OPEN("'"&chemin.\&nom_graph..xls"'")]' ;
	run ;
	%let plage1=L1C1:L1C40;
	%let plage2=L2C1:L1000C40;
	%do i=1 %to &nbvar;
		filename t dde "EXCEL|&chemin.\[&nom_graph..xls]&&onglet&i!&plage1" notab;
		data _null_;
			file t lrecl=500;
			set &nom_tab;
			sp="09"X;
			put "Fap3" sp 
			"&an_deb"  %do an=%eval(&an_deb+1) %to &an_fin; sp "&an"  %end;
			;		
		run;
		 filename t dde "EXCEL|&chemin.\[&nom_graph..xls]&&onglet&i!&plage2" notab;
		data _null_;
			file t lrecl=500;
			set &nom_tab;
			sp="09"X;
			put fap_3 $3. sp 
			&&var&i.._&an_deb commax8.2 %do an=%eval(&an_deb+1) %to &an_fin; sp &&var&i.._&an commax8.2 %end;
			;		
		run;
	%end;
	options missing=".";
	/*sauvegarde du fichier Excel*/
	FILENAME cmds DDE 'Excel|system';
	DATA _NULL_;
		FILE cmds;
		PUT '[save()]';
	RUN;
	/*fermeture du fichier*/
	data _null_ ;
		file cmds ;
		put "[FILE-CLOSE()]" ;
	run ;
	/*fermer excel*/
	data _null_ ;
		file cmds ;
		put "[QUIT()]" ;
	run ;

%mend;
%prep_fichier_mmo(2007,2014,&chemin,libout.resmmo,&nom_rep, taux_entree taux_sortie taux_rotation, entree sortie rotation);
