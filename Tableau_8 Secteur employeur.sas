%include "C:\Users\guillaume.toure\Desktop\DAMETH_TOURE\PSM\preparation_donnees\PSM 2021\Programmes\macros portraits.sas";

%let deb=2016;
%let fin=2020;
%let nom=t8;
%let nom_rep=Tableau_8 Secteur employeur;

libname eec "C:\Users\guillaume.toure\Desktop\DAMETH_TOURE\PSM\preparation_donnees\Series longues 2021\Bases SAS\s�ries_longues";

%let chemin=C:\Users\guillaume.toure\Desktop\DAMETH_TOURE\PSM\preparation_donnees\PSM 2021\Sorties\&nom_rep;
libname libout "&chemin";

%macro prep_sect(lib);

%do an=&deb %to &fin;
	data sl_dmq_&an;
		set &lib..sl_dmq_&an. ;
		keep  ident noi fap_3 poids_final annee naf;
			%if &an <= 2012 %then %do;
			naf=nafg38un;
			%end;
			%if &an >= 2013 %then %do;
			naf=nafg038un;
			%end;

		if naf in ("","  ","00") then naf="ZZ";
	run;
%end;
%mend;

%prep_sect(eec);

%calculs(chemin_res=&chemin,nom=&nom,an_deb=&deb,an_fin=&fin,nom_table_prep=sl_dmq,
	lib_donnees=work,lib_sortie=libout,var=naf,var_ventil=,var_pond=poids_final,
	seuil_diffusion=100);


data res&nom._&deb._&fin.;
	set libout.res&nom._&deb._&fin.;
run;
proc sort data=res&nom._&deb._&fin.; by fap_3 descending part%eval(&fin.-2)_&fin.; run;
data res&nom._&deb._&fin.;
	set res&nom._&deb._&fin.;
	by fap_3;
	retain ordre 1;
	ordre=ordre+1;
	if first.fap_3 then ordre=1;
	drop code;
run;
data res&nom._&deb._&fin._autres;
	set res&nom._&deb._&fin. (where=(part%eval(&fin.-2)_&fin.<3 or ordre>3));
run;
proc means data=res&nom._&deb._&fin._autres noprint;
	var eff%eval(&fin.-3)_%eval(&fin.-1) eff%eval(&fin.-2)_&fin. part%eval(&fin.-3)_%eval(&fin.-1) part%eval(&fin.-2)_&fin.;
	by fap_3;
	output out=autres (drop=_type_ _freq_) sum=eff%eval(&fin.-3)_%eval(&fin.-1) eff%eval(&fin.-2)_&fin. part%eval(&fin.-3)_%eval(&fin.-1) part%eval(&fin.-2)_&fin.;
run;
data autres;
	set autres;
	naf="AU";
	ordre=4;
run;
data res&nom._&deb._&fin.;
	set res&nom._&deb._&fin.(where=(part%eval(&fin.-2)_&fin.>=3 and ordre<=3)) autres;
	code=strip(fap_3)||strip(ordre);
run;
proc sort data=res&nom._&deb._&fin.;
	by fap_3 ordre;
run;

%prep_fichier(an_deb=&deb,an_fin=&fin,chemin=&chemin,nom_tab=res&nom._&deb._&fin.,
nom_tab_diff=resdiff&nom._&deb._&fin.,nom_graph=&nom_rep,var=naf,var_ventil=);




