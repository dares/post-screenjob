/*La macro "calculs" permet de calculer les �l�ments utiles pour les diff�rents graphiques
et tableaux des portraits statistiques. Il faut r�aliser les calculs sur au moins 3 ann�es de donn�es (car on fait des moyennes
sur 3 ans).

Param�tres de la macro :
	chemin_res : 	chemin pour les r�sultats en sortie. Si le r�pertoire indiqu� n'existe 
					pas encore, la macro le cr��e.
	nom : 		 	nom abr�g� du graphique ou du tableau, par exemple t1 pour le tableau 1,
		  		 	g1 pour le graphique 1. Ce nom est utilis� pour le nom des tables SAS
				 	en sortie.
	an_deb :	 	ann�e de d�but de la p�riode �tudi�e
	an_fin : 	 	ann�e de fin de la p�riode �tudi�e
	nom_table_prep: pr�fixe du nom des tables de d�part (sl_dmq par exemple pour les tables
					sl_dmq_1982,sl_dmq_1983 etc.
	lib_donnees : 	librairie des donn�es de d�part
	lib_sortie :	librairie en sortie
	var :			variable �tudi�e, par exemple l'�ge(rep_age_enq) pour les graphiques 2 
					et 3 des portraits. Pour le graphique 1 sur l'�volution de l'emploi, on
					ne la renseigne pas.
	var_ventil :	variable de ventilation, utile pour le tableau 3, o� on s'int�resse � la
					r�partition hommes/femmes, ventil�e par classes d'�ge. On ne renseigne
					pas cette variable pour tous les graphiques ou tableaux.
	var_pond :		variable de pond�ration. En g�n�ral on pond�re les donn�es par la
					variable poids_final pr�sente dans les tables de donn�es. Pour les donn�es
					sur les salaires, on n'utilise pas cette pond�ration (les questions
					sur le salaire sont pos�es seulement en 1�re et 6�me vague d'interrogation).
	seuil_diffusion:seuil pour la diffusion des r�sultats. Dans certaines Fap, l'�chantillon
					est trop r�duit pour certaines statistiques, notamment pour la r�partition
					par sexe crois�e avec l'�ge (tableau 3).

*/

%macro calculs_fap22(chemin_res=,nom=,an_deb=,an_fin=,nom_table_prep=,lib_donnees=,lib_sortie=,var=,var_ventil=,var_pond=,seuil_diffusion=);

	/*cr�ation du r�pertoire pour le chemin en sortie (chemin_res) s'il n'existe pas encore*/	
	options NOXWAIT NOXSYNC ;
	%let rc=%sysfunc(filename(fileref, &chemin_res));
	 %if %sysfunc(fexist(&fileref))  %then %do ; 
	 	%end;
		%else %do;
		Data _null_;
			X mkdir "&chemin_res"; /*Cr�ation du repertoire � l'aide de la macro-variable*/
		run;
	%end;

	/*Pr�paration des donn�es*/
	/************************/
	/*L'enqu�te Emploi a connu des �volutions, notamment le dictionnaire des variables a
	chang� � plusieurs reprises*/
	/*On garde uniquement les variables utiles pour les calculs + les variables individus
	(pour pouvoir calculer le nombre d'individus distincts dans l'�chantillon)*/ 

	%do an=&an_deb %to &an_fin;
		data temp_&nom_table_prep._&an;
			set &lib_donnees..&nom_table_prep._&an;
			keep noi fap_1 fap_1 &var &var_ventil &var_pond
			%if &an<1990 %then %do;
				idt_aire im_loc;
			%end;
			%if &an>=1990 and &an<2003 %then %do;
				aire imloc;
			%end;
			%if &an>=2003 %then %do;
				ident;
			%end;
		run;
		data temp_&nom_table_prep._&an;
			set temp_&nom_table_prep._&an;
			%if &an<1990 %then %do;
				annee="&an";
				idlog=COMPRESS(idt_aire||"_"||im_loc);
			%end;
			%if &an>=1990 and &an<2003 %then %do;
				annee="&an";
				idlog=COMPRESS(aire||"_"||imloc);
			%end;
			%if &an>=2003 %then %do;
				idlog=ident;
			%end;
		run;
	%end;

	/*Donn�es agr�g�es sur 3 ann�es glissantes*/
	/******************************************/

	%do an=%eval(&an_deb+1) %to %eval(&an_fin-1);
		data combin%eval(&an-1)_%eval(&an+1);
			set temp_&nom_table_prep._%eval(&an-1)  temp_&nom_table_prep._&an temp_&nom_table_prep._%eval(&an+1);
			%if (&an ne 1993 and &an ne 1994) %then %do;
				&var_pond=&var_pond/3;
				/*on divise la pond�ration par 3 comme on regroupe les donn�es sur 3 ans*/
			%end;
			%if &an=1993 %then %do;
				&var_pond=&var_pond/3;
				/*if fap_3="T2Z" then	&var_pond=&var_pond/2;
				else if fap_3 in("T2A","T2B") then &var_pond=&var_pond;*/
				/*cas particulier de la Fap T2Z, qui s'est divis�e en 2 sous-Fap, T2A et 
				T2B, en 1994 : donc pour 1992-1994 on a les donn�es 1 ann�e pour T2A et T2B,
				et 2 ann�es pour T2Z*/
				/*else &var_pond=&var_pond/3;*/
			%end;
			%if &an=1994 %then %do;
				/*if fap_3="T2Z" then	&var_pond=&var_pond;
				else if fap_3 in("T2A","T2B") then &var_pond=&var_pond/2;*/
				/*pour 1993-1995 on a les donn�es 2 ann�es pour T2A et T2B,
				et 1 ann�e pour T2Z*/
				/*else*/ &var_pond=&var_pond/3;
			%end;
		run;

		/*Calcul de la r�partition*/
		/*************************/

		/*R�partition sans les non-r�ponses*/

		proc summary data= combin%eval(&an-1)_%eval(&an+1)  nway noprint;
			class fap_1 %if "&var_ventil" ne "" %then %do; &var_ventil %end; &var ;
			var &var_pond;
			output out=res_%eval(&an-1)_%eval(&an+1)(drop=_FREQ_ _TYPE_) sum=eff%eval(&an-1)_%eval(&an+1);
		run;
		%if "&var" ne "" %then %do;
			proc sql;
				create table res_%eval(&an-1)_%eval(&an+1) as select *, 
				100*eff%eval(&an-1)_%eval(&an+1)/sum(eff%eval(&an-1)_%eval(&an+1)) 
				as part%eval(&an-1)_%eval(&an+1) from res_%eval(&an-1)_%eval(&an+1) 
				group by fap_1 %if "&var_ventil" ne "" %then %do; , &var_ventil %end;;
			quit; 
		%end;
		/*Ensemble*/
		proc summary data= combin%eval(&an-1)_%eval(&an+1)  nway noprint;
			%if "&var" ne "" or "&var_ventil" ne "" %then %do;
				class %if "&var_ventil" ne "" %then %do; &var_ventil %end; &var ;
			%end;
			var &var_pond;
			output out=res_ens_%eval(&an-1)_%eval(&an+1)(drop=_FREQ_ _TYPE_) 
					sum=eff%eval(&an-1)_%eval(&an+1);
		run;
		%if "&var" ne "" %then %do;
			proc sql;
				create table res_ens_%eval(&an-1)_%eval(&an+1) as select *, 
				100*eff%eval(&an-1)_%eval(&an+1)/sum(eff%eval(&an-1)_%eval(&an+1)) 
				as part%eval(&an-1)_%eval(&an+1) from res_ens_%eval(&an-1)_%eval(&an+1)  
				%if "&var_ventil" ne "" %then %do; group by &var_ventil %end;;
			quit; 
		%end;
		data res_ens_%eval(&an-1)_%eval(&an+1);
			set res_ens_%eval(&an-1)_%eval(&an+1);
			fap_1="Y";
		run;
		/*cas particulier de la fap T2Z, remplac�e par T2A et T2B*/
		/*%if &an>=1995 %then %do;
			proc summary data= combin%eval(&an-1)_%eval(&an+1)(where=(fap_3 in("T2A","T2B"))) nway noprint;
				var &var_pond;
				class %if "&var_ventil" ne "" %then %do;  &var_ventil %end; &var ;
				output out=resT2_%eval(&an-1)_%eval(&an+1)(drop=_FREQ_ _TYPE_) 
				sum=eff%eval(&an-1)_%eval(&an+1);
			run;
			%if "&var" ne "" %then %do;
				proc sql;
					create table resT2_%eval(&an-1)_%eval(&an+1) as select *, 
					100*eff%eval(&an-1)_%eval(&an+1)/sum(eff%eval(&an-1)_%eval(&an+1)) 
					as part%eval(&an-1)_%eval(&an+1) from resT2_%eval(&an-1)_%eval(&an+1) 
					%if "&var_ventil" ne "" %then %do; group by &var_ventil %end;;
				quit; 
			%end;
			data resT2_%eval(&an-1)_%eval(&an+1);
				set resT2_%eval(&an-1)_%eval(&an+1);
				fap_3="T2Z";
			run;
			data res%eval(&an-1)_%eval(&an+1);
				set res_%eval(&an-1)_%eval(&an+1) resT2_%eval(&an-1)_%eval(&an+1) 
					res_ens_%eval(&an-1)_%eval(&an+1);
			run;
			proc sort data=res%eval(&an-1)_%eval(&an+1);by fap_3;run;
		%end;*/
		/*%else %do;*/
			data res%eval(&an-1)_%eval(&an+1);
				set res_%eval(&an-1)_%eval(&an+1) res_ens_%eval(&an-1)_%eval(&an+1);
			run;
		/*%end;*/
		proc sort data=res%eval(&an-1)_%eval(&an+1);by fap_1 
			%if "&var_ventil" ne "" %then %do; &var_ventil %end; &var;
		run;
		proc summary data=res%eval(&an-1)_%eval(&an+1) noprint nway;
			var eff%eval(&an-1)_%eval(&an+1);
			class fap_1 %if "&var_ventil" ne "" %then %do; &var_ventil %end;;
			output out=eff%eval(&an-1)_%eval(&an+1) sum=eff%eval(&an-1)_%eval(&an+1);
		run;

		/*R�partition en comptant les non-r�ponses*/
		
		proc summary data= combin%eval(&an-1)_%eval(&an+1) missing nway noprint;
			class fap_1 %if "&var_ventil" ne "" %then %do; &var_ventil %end; &var ;
			var &var_pond;
			output out=resNR_%eval(&an-1)_%eval(&an+1)(drop=_FREQ_ _TYPE_) 
			sum=eff%eval(&an-1)_%eval(&an+1);
		run;
		%if "&var" ne "" %then %do;
			proc sql;
				create table resNR_%eval(&an-1)_%eval(&an+1) as select *, 
				100*eff%eval(&an-1)_%eval(&an+1)/sum(eff%eval(&an-1)_%eval(&an+1)) 
				as part%eval(&an-1)_%eval(&an+1) from resNR_%eval(&an-1)_%eval(&an+1) 
				group by fap_1 %if "&var_ventil" ne "" %then %do; , &var_ventil %end;;
			quit; 
		%end;
		/*Ensemble*/
		proc summary data= combin%eval(&an-1)_%eval(&an+1) missing nway noprint;
			%if "&var" ne "" or "&var_ventil" ne "" %then %do;
				class %if "&var_ventil" ne "" %then %do; &var_ventil %end; &var ;
			%end;
			var &var_pond;
			output out=resNR_ens_%eval(&an-1)_%eval(&an+1)(drop=_FREQ_ _TYPE_) 
			sum=eff%eval(&an-1)_%eval(&an+1);
		run;
		%if "&var" ne "" %then %do;
			proc sql;
				create table resNR_ens_%eval(&an-1)_%eval(&an+1) as select *, 
				100*eff%eval(&an-1)_%eval(&an+1)/sum(eff%eval(&an-1)_%eval(&an+1)) 
				as part%eval(&an-1)_%eval(&an+1) from resNR_ens_%eval(&an-1)_%eval(&an+1)  
				%if "&var_ventil" ne "" %then %do; group by &var_ventil %end;;
			quit; 
		%end;
		data resNR_ens_%eval(&an-1)_%eval(&an+1);
			set resNR_ens_%eval(&an-1)_%eval(&an+1);
			fap_1="Y";
		run;
		/*cas particulier de la fap T2Z, remplac�e par T2A et T2B*/
		/*%if &an>=1995 %then %do;
			proc summary data= combin%eval(&an-1)_%eval(&an+1)(where=(fap_3 in("T2A","T2B"))) missing nway noprint;
				var &var_pond;
				class %if "&var_ventil" ne "" %then %do;  &var_ventil %end; &var ;
				output out=resNRT2_%eval(&an-1)_%eval(&an+1)(drop=_FREQ_ _TYPE_) sum=eff%eval(&an-1)_%eval(&an+1);
			run;
			%if "&var" ne "" %then %do;
				proc sql;
					create table resNRT2_%eval(&an-1)_%eval(&an+1) as select *, 100*eff%eval(&an-1)_%eval(&an+1)/sum(eff%eval(&an-1)_%eval(&an+1)) as part%eval(&an-1)_%eval(&an+1) from resNRT2_%eval(&an-1)_%eval(&an+1) 
					%if "&var_ventil" ne "" %then %do; group by &var_ventil %end;;
				quit; 
			%end;
			data resNRT2_%eval(&an-1)_%eval(&an+1);
				set resNRT2_%eval(&an-1)_%eval(&an+1);
				fap_3="T2Z";
			run;
			data resNR%eval(&an-1)_%eval(&an+1);
				set resNR_%eval(&an-1)_%eval(&an+1) resNRT2_%eval(&an-1)_%eval(&an+1) resNR_ens_%eval(&an-1)_%eval(&an+1);
			run;
			proc sort data=resNR%eval(&an-1)_%eval(&an+1);by fap_3;run;
		%end;
		%else %do;*/
			data resNR%eval(&an-1)_%eval(&an+1);
				set resNR_%eval(&an-1)_%eval(&an+1) resNR_ens_%eval(&an-1)_%eval(&an+1);
			run;
		/*%end;*/
		proc sort data=resNR%eval(&an-1)_%eval(&an+1);by fap_1 %if "&var_ventil" ne "" %then %do; &var_ventil %end; &var;
		run;
		proc summary data=resNR%eval(&an-1)_%eval(&an+1) noprint nway;
			var eff%eval(&an-1)_%eval(&an+1);
			class fap_1 %if "&var_ventil" ne "" %then %do; &var_ventil %end;;
			output out=effNR%eval(&an-1)_%eval(&an+1) sum=eff%eval(&an-1)_%eval(&an+1);
		run;


		/*Taille de l'�chantillon (r�ponses distinctes)*/
		/**********************************************/
		proc sort data=combin%eval(&an-1)_%eval(&an+1) %if "&var" ne "" %then %do; 
			(where=(&var ne "" %if "&var_ventil" ne "" %then %do; and  &var_ventil ne "" 
			%end;)) %end; 
			nodupkey out=distinct%eval(&an-1)_%eval(&an+1);
			by idlog noi fap_1;
		run;
		%if "&var_ventil" ne "" %then %do; 
			proc sort data=distinct%eval(&an-1)_%eval(&an+1); by &var_ventil;run; 
		%end;;
		proc freq data = distinct%eval(&an-1)_%eval(&an+1) noprint;
			table fap_1/out=taille_ech%eval(&an-1)_%eval(&an+1);
			%if "&var_ventil" ne "" %then %do; by &var_ventil %end;;
		run;
		data taille_ech%eval(&an-1)_%eval(&an+1);
			set taille_ech%eval(&an-1)_%eval(&an+1);
			rename count=nb_%eval(&an-1)_%eval(&an+1);
		run;
		proc sort data=taille_ech%eval(&an-1)_%eval(&an+1);
			by fap_1 %if "&var_ventil" ne "" %then %do; &var_ventil %end;;
		run;

		/*Statistiques sur les poids*/
		/****************************/
		/*pour regarder par exemple si certaines observations contribuent beaucoup aux
		r�sultats*/
		proc summary data=combin%eval(&an-1)_%eval(&an+1) sum noprint nway missing;
			var &var_pond;
			class fap_1 idlog noi %if "&var_ventil" ne "" %then %do; &var_ventil %end; ;
			output out=pond%eval(&an-1)_%eval(&an+1) sum=poids%eval(&an-1)_%eval(&an+1);
		run;
		proc summary data=pond%eval(&an-1)_%eval(&an+1) min mean max std noprint nway missing;
			var poids%eval(&an-1)_%eval(&an+1);
			class fap_1 %if "&var_ventil" ne "" %then %do; &var_ventil %end;;
			output out=pond2%eval(&an-1)_%eval(&an+1) 
			min=poidsmin%eval(&an-1)_%eval(&an+1) mean=poidsmoy%eval(&an-1)_%eval(&an+1)
			max=poidsmax%eval(&an-1)_%eval(&an+1) std=ectype%eval(&an-1)_%eval(&an+1);
		run;
		data poids%eval(&an-1)_%eval(&an+1);
			merge eff%eval(&an-1)_%eval(&an+1) pond2%eval(&an-1)_%eval(&an+1);
			by fap_1 %if "&var_ventil" ne "" %then %do; &var_ventil %end; ;
		run;
		data poids%eval(&an-1)_%eval(&an+1) ;	
			set poids%eval(&an-1)_%eval(&an+1) ;
			contribmax%eval(&an-1)_%eval(&an+1)=round(100*poidsmax%eval(&an-1)_%eval(&an+1)/eff%eval(&an-1)_%eval(&an+1),0.01);
			contribmoy%eval(&an-1)_%eval(&an+1)=round(100*poidsmoy%eval(&an-1)_%eval(&an+1)/eff%eval(&an-1)_%eval(&an+1),0.01);
			rapport%eval(&an-1)_%eval(&an+1)=poidsmax%eval(&an-1)_%eval(&an+1)/poidsmin%eval(&an-1)_%eval(&an+1);
			keep fap_1 %if "&var_ventil" ne "" %then %do; &var_ventil %end; poidsmin%eval(&an-1)_%eval(&an+1) 
			poidsmoy%eval(&an-1)_%eval(&an+1) poidsmax%eval(&an-1)_%eval(&an+1) ectype%eval(&an-1)_%eval(&an+1) 
			rapport%eval(&an-1)_%eval(&an+1) eff%eval(&an-1)_%eval(&an+1) contribmax%eval(&an-1)_%eval(&an+1)
			contribmoy%eval(&an-1)_%eval(&an+1);
		run;

	%end;

	data &lib_sortie..res&nom._&an_deb._&an_fin;
		merge %do an=%eval(&an_deb+1) %to %eval(&an_fin-1); res%eval(&an-1)_%eval(&an+1) %end;;
		by fap_1 %if "&var_ventil" ne "" %then %do; &var_ventil %end; &var;
	run;
	data &lib_sortie..resNR&nom._&an_deb._&an_fin;
		merge %do an=%eval(&an_deb+1) %to %eval(&an_fin-1); resNR%eval(&an-1)_%eval(&an+1) %end;;
		by fap_1 %if "&var_ventil" ne "" %then %do; &var_ventil %end; &var;
	run;

	%if "&var" ne "" %then %do;
		data &lib_sortie..res&nom._&an_deb._&an_fin;
			set &lib_sortie..res&nom._&an_deb._&an_fin;
			%do an=%eval(&an_deb+1) %to %eval(&an_fin-1); 
				if eff%eval(&an-1)_%eval(&an+1)=. then eff%eval(&an-1)_%eval(&an+1)=0; 
				if part%eval(&an-1)_%eval(&an+1)=. then part%eval(&an-1)_%eval(&an+1)=0; 
			%end;
		run;
		data &lib_sortie..resNR&nom._&an_deb._&an_fin;
			set &lib_sortie..resNR&nom._&an_deb._&an_fin;
			%do an=%eval(&an_deb+1) %to %eval(&an_fin-1); 
				if eff%eval(&an-1)_%eval(&an+1)=. then eff%eval(&an-1)_%eval(&an+1)=0; 
				if part%eval(&an-1)_%eval(&an+1)=. then part%eval(&an-1)_%eval(&an+1)=0; 
			%end;
		run;
	%end;

	proc sql;
		create table &lib_sortie..res&nom._&an_deb._&an_fin as select *,
		%if "&var" ne "" %then %do; std(part&an_deb._%eval(&an_deb+2) %do an=%eval(&an_deb+2) 
		%to %eval(&an_fin-1);, part%eval(&an-1)_%eval(&an+1) %end; ) as ectype_part,
		mean(part&an_deb._%eval(&an_deb+2) %do an=%eval(&an_deb+2) %to %eval(&an_fin-1);
		, part%eval(&an-1)_%eval(&an+1) %end; ) as mean_part, %end;
		mean(eff&an_deb._%eval(&an_deb+2) %do an=%eval(&an_deb+2) %to %eval(&an_fin-1);
		, eff%eval(&an-1)_%eval(&an+1) %end; ) as mean_eff,
		std(eff&an_deb._%eval(&an_deb+2) %do an=%eval(&an_deb+2) %to %eval(&an_fin-1);
		, eff%eval(&an-1)_%eval(&an+1) %end; ) as ectype_eff
		from &lib_sortie..res&nom._&an_deb._&an_fin group by
		fap_1 %if "&var_ventil" ne "" %then %do; ,&var_ventil %end; 
		%if "&var" ne "" %then %do; ,&var %end;;
	quit;

	data &lib_sortie..taille_ech&nom._&an_deb._&an_fin;
		merge %do an=%eval(&an_deb+1) %to %eval(&an_fin-1); taille_ech%eval(&an-1)_%eval(&an+1) %end;;
		by fap_1 %if "&var_ventil" ne "" %then %do; &var_ventil %end;;
	run;
	data &lib_sortie..taille_ech&nom._&an_deb._&an_fin;
		set &lib_sortie..taille_ech&nom._&an_deb._&an_fin;
		drop percent;
	run;
	data &lib_sortie..poids&nom._&an_deb._&an_fin;
		merge %do an=%eval(&an_deb+1) %to %eval(&an_fin-1); poids%eval(&an-1)_%eval(&an+1) %end;;
		by fap_1 %if "&var_ventil" ne "" %then %do; &var_ventil %end;;
	run;
	data &lib_sortie..poids&nom._&an_deb._&an_fin;
		retain fap_1 &var_ventil %do an=%eval(&an_deb+1) %to %eval(&an_fin-1);
		poidsmax%eval(&an-1)_%eval(&an+1) %end;
		%do an=%eval(&an_deb+1) %to %eval(&an_fin-1);
		poidsmin%eval(&an-1)_%eval(&an+1) %end;
		%do an=%eval(&an_deb+1) %to %eval(&an_fin-1);
		poidsmoy%eval(&an-1)_%eval(&an+1) %end;
		%do an=%eval(&an_deb+1) %to %eval(&an_fin-1);
		ectype%eval(&an-1)_%eval(&an+1) %end;
		%do an=%eval(&an_deb+1) %to %eval(&an_fin-1);
		rapport%eval(&an-1)_%eval(&an+1) %end;
		%do an=%eval(&an_deb+1) %to %eval(&an_fin-1);
		contribmax%eval(&an-1)_%eval(&an+1) %end;
		%do an=%eval(&an_deb+1) %to %eval(&an_fin-1);
		contribmoy%eval(&an-1)_%eval(&an+1) %end;;
		set &lib_sortie..poids&nom._&an_deb._&an_fin;
		keep fap_1 &var_ventil %do an=%eval(&an_deb+1) %to %eval(&an_fin-1);
		poidsmax%eval(&an-1)_%eval(&an+1) %end;
		%do an=%eval(&an_deb+1) %to %eval(&an_fin-1);
		poidsmin%eval(&an-1)_%eval(&an+1) %end;
		%do an=%eval(&an_deb+1) %to %eval(&an_fin-1);
		poidsmoy%eval(&an-1)_%eval(&an+1) %end;
		%do an=%eval(&an_deb+1) %to %eval(&an_fin-1);
		ectype%eval(&an-1)_%eval(&an+1) %end;
		%do an=%eval(&an_deb+1) %to %eval(&an_fin-1);
		rapport%eval(&an-1)_%eval(&an+1) %end;
		%do an=%eval(&an_deb+1) %to %eval(&an_fin-1);
		contribmax%eval(&an-1)_%eval(&an+1) %end;
		%do an=%eval(&an_deb+1) %to %eval(&an_fin-1);
		contribmoy%eval(&an-1)_%eval(&an+1) %end;;
	run; 

	%if "&var" ne "" %then %do;
		data liste_fap;
			set &lib_donnees..&nom_table_prep._&an_fin;
			keep fap_1;
			if fap_1="" then delete;
		run;
		/*proc sql;
			insert into liste_fap(fap_1) values("T2Z");
		quit;*/
		proc sort data=liste_fap nodupkey;by fap_1;run;
		data liste_&var;
			set &lib_donnees..&nom_table_prep._&an_fin;
			keep &var;
			if &var="" then delete;
		run;
		proc sort data=liste_&var nodupkey;by &var;run;
		%if &var_ventil ne %then %do;
			data liste_ventil;
				set &lib_donnees..&nom_table_prep._&an_fin;
				keep &var_ventil;
			run;
			proc sort data=liste_ventil nodupkey;by &var_ventil;run;
		%end;
		proc sql;
			create table liste_fap_&var as select * from liste_fap,liste_&var
			%if &var_ventil ne %then %do; ,liste_ventil %end; ;
		quit;
		proc sort data=liste_fap_&var;by fap_1 &var %if &var_ventil ne %then %do; &var_ventil %end;;run;
		data libout.res&nom._&deb._&fin;
			merge libout.res&nom._&deb._&fin liste_fap_&var;
			by fap_1 &var %if &var_ventil ne %then %do; &var_ventil %end;;
		run;
	%end;

	data &lib_sortie..res&nom._&an_deb._&an_fin;
		retain code fap_1 &var &var_ventil %do an=%eval(&an_deb+1) %to %eval(&an_fin-1);
		eff%eval(&an-1)_%eval(&an+1) %end; 
		%if "&var" ne "" %then %do; %do an=%eval(&an_deb+1) %to %eval(&an_fin-1);
		part%eval(&an-1)_%eval(&an+1) %end; %end;;
		set &lib_sortie..res&nom._&an_deb._&an_fin;
		%if "&var" ne "" %then %do;
			%if "&var_ventil" ne "" %then %do;
				code=strip(fap_1)||strip(&var)||strip(&var_ventil);
			%end;
			%else %do;
				code=strip(fap_1)||strip(&var);
			%end;
		%end;
		%else %do;
			%if "&nom" ne "t1" %then %do;
				code=strip(fap_1);
			%end;
		%end;
		format fap_1 $1.;
	run;

	%if "&nom"="t1" %then %do; /*tableau sur le d�tail des effectifs par FAP*/
		proc sort data=&lib_sortie..res&nom._&an_deb._&an_fin;by fap_1 fap;run;
		data &lib_sortie..res&nom._&an_deb._&an_fin;
			set &lib_sortie..res&nom._&an_deb._&an_fin;
			by fap_1;
			retain num 1;
			num=num+1;
			if first.fap_1 then num=1;
		run;
		data &lib_sortie..res&nom._&an_deb._&an_fin;
			set &lib_sortie..res&nom._&an_deb._&an_fin;
			code=strip(fap_1)||strip(num);
		run;
	%end;
	
	%if "&var" ne "" %then %do;
		data donnees_diffusion;
			merge &lib_sortie..res&nom._&an_deb._&an_fin &lib_sortie..taille_ech&nom._&an_deb._&an_fin;
				by fap_1 %if "&var_ventil" ne "" %then %do; &var_ventil %end;;
		run;
		data donnees_diffusion;
			set donnees_diffusion;
			%do an=%eval(&an_deb+1) %to %eval(&an_fin-1);
				if fap_1 ne "ENS" and nb_%eval(&an-1)_%eval(&an+1)<&seuil_diffusion then part%eval(&an-1)_%eval(&an+1)=.;
			%end;
		run;
		data &lib_sortie..resdiff&nom._&an_deb._&an_fin;set donnees_diffusion;run;
		/*table de donn�es avec un filtre pour les donn�es pour lesquelles l'�chantillon 
		est trop r�duit*/

	%end;


%mend;



/*Export des donn�es sous Excel pour constituer les graphiques ou tableaux.
Le dossier "Mod�les" contient les fichiers excel qui servent de mod�les pour les fichiers
cr��s. Pour chaque graphique, la macro copie le fichier mod�le et exporte les
donn�es dans le nouveau fichier cr��*/
%macro prep_fichier_fap22(an_deb=,an_fin=,chemin=,nom_tab=,nom_tab_diff=,nom_graph=, var=, var_ventil=);
	%let chemin_psm=C:\Users\guillaume.toure\Desktop\DAMETH_TOURE\PSM\preparation_donnees\PSM 2021\Mod�les;/**/
	proc sql;
		create table temp as select *, max(length(code)) as longueur_code %if "&var" ne "" %then %do; , max(length(&var)) as longueur_var %end;
		%if "&var_ventil" ne "" %then %do; ,max(length(&var_ventil)) as longueur_var_ventil %end;
		from &nom_tab;
	quit;
	data _null_;
		set temp;
		call symput("longueur_code",strip(longueur_code));
		%if "&var" ne "" %then %do; call symput("longueur_var",strip(longueur_var)); %end;
		%if "&var_ventil" ne "" %then %do; call symput("longueur_var_ventil",strip(longueur_var_ventil)); %end;
	run;
	options missing="";
	options NOXWAIT NOXSYNC ;
	x copy "&chemin_psm.\Mod�le &nom_graph..xls" "&chemin.\&nom_graph..xls";
	options NOXWAIT NOXSYNC ; *** Open Excel ***; 
	x "start excel"; 
	data _null_ ; 
		x = SLEEP(4) ; 
	run ; 
	filename cmds DDE 'excel|system' ;
	data _null_ ;
		file cmds ;
		put '[FILE-OPEN("'"&chemin.\&nom_graph..xls"'")]' ;
	run ;
	%let plage1=L1C1:L1C60;
	%let plage2=L2C1:L1000C60;
	filename t dde "EXCEL|&chemin.\[&nom_graph..xls]eff_3ans!&plage1" notab;
	data _null_;
		file t lrecl=500;
		set &nom_tab;
		sp="09"X;
		put "Code" sp "Fap" sp %if "&var" ne "" %then %do; "&var" sp %end;
		%if "&var_ventil" ne "" %then %do; "&var_ventil" sp %end;
		"%eval(&an_deb)-%eval(&an_deb+2)"  %do an=%eval(&an_deb+2) %to %eval(&an_fin-1); sp "%eval(&an-1)-%eval(&an+1)"  %end;
		;		
	run;
	 filename t dde "EXCEL|&chemin.\[&nom_graph..xls]eff_3ans!&plage2" notab;
	data _null_;
		file t lrecl=500;
		set &nom_tab;
		sp="09"X;
		put code $&longueur_code.. sp fap_1 $1. sp %if "&var" ne "" %then %do; &var $&longueur_var.. sp %end;
		%if "&var_ventil" ne "" %then %do; &var_ventil $&longueur_var_ventil.. sp %end;
		eff%eval(&an_deb)_%eval(&an_deb+2) commax10.5 %do an=%eval(&an_deb+2) %to %eval(&an_fin-1); sp eff%eval(&an-1)_%eval(&an+1) commax10.5 %end;
		;		
	run;
	%if &var ne %then %do;
		filename t dde "EXCEL|&chemin.\[&nom_graph..xls]part_3ans!&plage1" notab;
		data _null_;
			file t lrecl=500;
			set &nom_tab;
			sp="09"X;
			put "Code" sp "Fap" sp %if "&var" ne "" %then %do; "&var" sp %end;
			%if "&var_ventil" ne "" %then %do; "&var_ventil" sp %end;
			"%eval(&an_deb)-%eval(&an_deb+2)"  %do an=%eval(&an_deb+2) %to %eval(&an_fin-1); sp "%eval(&an-1)-%eval(&an+1)"  %end;
			;		
		run;
		 filename t dde "EXCEL|&chemin.\[&nom_graph..xls]part_3ans!&plage2" notab;
		data _null_;
			file t lrecl=500;
			set &nom_tab;
			sp="09"X;
			put code $&longueur_code.. sp fap_1 $1. sp %if "&var" ne "" %then %do; &var $&longueur_var.. sp %end;
			%if "&var_ventil" ne "" %then %do; &var_ventil $&longueur_var_ventil.. sp %end;
			part%eval(&an_deb)_%eval(&an_deb+2) commax10.5 %do an=%eval(&an_deb+2) %to %eval(&an_fin-1); sp part%eval(&an-1)_%eval(&an+1) commax10.5 %end;
			;		
		run;
		filename t dde "EXCEL|&chemin.\[&nom_graph..xls]part_3ans_diff!&plage1" notab;
		/*donn�es non affich�es quand la taille de l'�chantillon est insuffisante*/
		data _null_;
			file t lrecl=500;
			set &nom_tab_diff;
			sp="09"X;
			put "Code" sp "Fap" sp %if "&var" ne "" %then %do; "&var" sp %end;
			%if "&var_ventil" ne "" %then %do; "&var_ventil" sp %end;
			"%eval(&an_deb)-%eval(&an_deb+2)"  %do an=%eval(&an_deb+2) %to %eval(&an_fin-1); sp "%eval(&an-1)-%eval(&an+1)"  %end;
			;		
		run;
		 filename t dde "EXCEL|&chemin.\[&nom_graph..xls]part_3ans_diff!&plage2" notab;
		data _null_;
			file t lrecl=500;
			set &nom_tab_diff;
			sp="09"X;
			put code $&longueur_code.. sp fap_1 $1. sp %if "&var" ne "" %then %do; &var $&longueur_var.. sp %end;
			%if "&var_ventil" ne "" %then %do; &var_ventil $&longueur_var_ventil.. sp %end;
			part%eval(&an_deb)_%eval(&an_deb+2) commax10.5 %do an=%eval(&an_deb+2) %to %eval(&an_fin-1); sp part%eval(&an-1)_%eval(&an+1) commax10.5 %end;
			;		
		run;
	%end;
	options missing=".";
	/*sauvegarde du fichier Excel*/
	FILENAME cmds DDE 'Excel|system';
	DATA _NULL_;
		FILE cmds;
		PUT '[save()]';
	RUN;
	/*fermeture du fichier*/
	data _null_ ;
		file cmds ;
		put "[FILE-CLOSE()]" ;
	run ;
	/*fermer excel*/
	data _null_ ;
		file cmds ;
		put "[QUIT()]" ;
	run ;

%mend;

/*Export des donn�s sous Excel pour constituer les graphiques de taille d'�chantillon.
De m�me on part d'un mod�le de fichier, situ� dans le dossier "Mod�les"*/
%macro prep_graph_taille_ech_fap22(an_deb=,an_fin=,chemin=,nom_tab=,nom_graph=,titre=, var_ventil=);
	%let chemin_psm=C:\Users\guillaume.toure\Desktop\DAMETH_TOURE\PSM\preparation_donnees\PSM 2021\Mod�les;
	proc sql;
		create table temp as select *	%if "&var_ventil" ne "" %then %do; ,max(length(&var_ventil)) as longueur_var_ventil %end;
		from &nom_tab;
	quit;
	data _null_;
		set temp;
		%if "&var_ventil" ne "" %then %do; call symput("longueur_var_ventil",strip(longueur_var_ventil)); %end;
	run;
	options missing="";
	options NOXWAIT NOXSYNC ;
	x copy "&chemin_psm.\Mod�le Graphique Taille �chantillon.xls" "&chemin.\&nom_graph..xls";
	options NOXWAIT NOXSYNC ; *** Open Excel ***; 
	x "start excel"; 
	data _null_ ; 
		x = SLEEP(4) ; 
	run ; 
	filename cmds DDE 'excel|system' ;
	data _null_ ;
		file cmds ;
		put '[FILE-OPEN("'"&chemin.\&nom_graph..xls"'")]' ;
	run ;
	%let plage1=L1C1:L1C40;
	%let plage2=L2C1:L1000C40;
	%let plage3=L5C1:L5C1;
	filename t dde "EXCEL|&chemin.\[&nom_graph..xls]taille_ech!&plage1" notab;
	data _null_;
		file t lrecl=500;
		set &nom_tab;
		sp="09"X;
		put "Fap" sp 
		%if "&var_ventil" ne "" %then %do; "&var_ventil"  sp %end;
		"&an_deb._%eval(&an_deb+2)"  %do an=%eval(&an_deb+2) %to %eval(&an_fin-1); sp "%eval(&an-1)_%eval(&an+1)"  %end;
		;		
	run;
	 filename t dde "EXCEL|&chemin.\[&nom_graph..xls]taille_ech!&plage2" notab;
	data _null_;
		file t lrecl=500;
		set &nom_tab;
		sp="09"X;
		put fap_1 $1. sp 
		%if "&var_ventil" ne "" %then %do; &var_ventil $&longueur_var_ventil.. sp %end;
		nb_%eval(&an_deb)_%eval(&an_deb+2) commax10.5 %do an=%eval(&an_deb+2) %to %eval(&an_fin-1); sp nb_%eval(&an-1)_%eval(&an+1) commax10.5 %end;
		;		
	run;
	 filename t dde "EXCEL|&chemin.\[&nom_graph..xls]graph!&plage3" notab;
	data _null_;
		file t lrecl=500;
		set &nom_tab;
		sp="09"X;
		put "%bquote(&titre)";
		;		
	run;
	options missing=".";
	/*sauvegarde du fichier Excel*/
	FILENAME cmds DDE 'Excel|system';
	DATA _NULL_;
		FILE cmds;
		PUT '[save()]';
	RUN;
	/*fermeture du fichier*/
	data _null_ ;
		file cmds ;
		put "[FILE-CLOSE()]" ;
	run ;
	/*fermer excel*/
	data _null_ ;
		file cmds ;
		put "[QUIT()]" ;
	run ;

%mend;


%macro export_synthese_fap22(tab,fichier,onglet,label,plage,liste_var,format);
	%let i=1;
	%do %while(%length(%scan(&liste_var,&i))>0);
		%let var&i=%scan(&liste_var,&i);
		%let i=%eval(&i+1);
	%end;
	%let nbvar=%eval(&i-1);

	options missing="-";
	%if "&label" ne "" %then %do;
		%let plage_label=L1C2:L1C2;
		filename t dde "EXCEL|&chemin.\[%bquote(&fichier).xls]&onglet!&plage_label" notab;
		data _null_;
			file t lrecl=500;
			set &tab;
			sp="09"X;
			put "%bquote(&label)"
			;		
		run;
	%end;
	filename t dde "EXCEL|&chemin.\[%bquote(&fichier).xls]&onglet!&plage" notab;
	data _null_;
		file t lrecl=500;
		set &tab;
		sp="09"X;
		put &var1 &format 
			%do i=2 %to &nbvar; sp &&var&i &format %end; ;
		;		
	run;
	options missing=".";

%mend;


