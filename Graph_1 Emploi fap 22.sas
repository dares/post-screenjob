%include "C:\Users\guillaume.toure\Desktop\DAMETH_TOURE\PSM\preparation_donnees\PSM 2021\Programmes\macros portraits fap22.sas";

%let deb=1982;
%let fin=2020;/*2014*/
libname eec "C:\Users\guillaume.toure\Desktop\DAMETH_TOURE\PSM\preparation_donnees\Series longues 2021\Bases SAS\sÚries_longues";

%let nom=g1;
%let nom_rep=Graphique_1 Emploi fap22;
%let chemin=C:\Users\guillaume.toure\Desktop\DAMETH_TOURE\PSM\preparation_donnees\PSM 2021\Sorties\&nom_rep;
libname libout "&chemin";
/* libname libold "d:\documents-utilisateurs\charline.babet\Donnees\PSM\PSM 2012\Sorties\&nom_rep"; */

%calculs_fap22(chemin_res=&chemin,nom=&nom,an_deb=&deb,an_fin=&fin,
	nom_table_prep=sl_dmq,lib_donnees=eec,lib_sortie=libout,var=,var_ventil=,
	var_pond=poids_final,seuil_diffusion=100);

/* %compare_annee_prec(chemin_res=&chemin,nom=&nom,an_deb=&deb,an_fin=&fin,
	nom_table_prep=sl_dmq,lib_donnees=eec,lib_sortie=libout,var=,var_ventil=,
	var_pond=poids_final,tab_compare=libold.res&nom._1982_2011) */

/*data libout.res&nom._&deb._&fin;
	set libout.res&nom._&deb._&fin;
	if fap_5 in("T2A60","T2B60") then do;
		eff1992_1994=.;
		eff1993_1995=.;
	end;
run;*/

%prep_fichier_fap22(an_deb=&deb,an_fin=&fin,chemin=&chemin,
	nom_tab=libout.res&nom._&deb._&fin,
	nom_tab_diff=libout.res&nom._&deb._&fin,nom_graph=&nom_rep,var= ,var_ventil=);

%prep_graph_taille_ech_fap22(an_deb=&deb,an_fin=&fin,chemin=&chemin,
	nom_tab=libout.taille_ech&nom._&deb._&fin,
	nom_graph=Graphique taille ech ens,titre="Taille de l'Úchantillon (ensemble)",
	var_ventil= );


